<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bfm_learn');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/:(Cav1t@#6_6%SQL!BaPwm7MU4JEQEa{A9<&7< D4]kZla*.xL;g(0d8>COrG^j');
define('SECURE_AUTH_KEY',  'M+Q|gG{xDy[eP`pt:C3eW @y</yQo34jUS&Jn8o- r#o={jK/C-bMv{ frEl<zzv');
define('LOGGED_IN_KEY',    'hV?W~!bW{iTphV:T }LSkO]gX$,6FU[/<~3vW1Ls+A_;,qdCStvY9i6hv^L_psuy');
define('NONCE_KEY',        'S`+#TlSoSb/7V]JtzF[I96R>P!HtC4xT,]L5@{WDKJT:iV{kF0kK_w~&:X3A2OBq');
define('AUTH_SALT',        'bs&$ ACxB U2X90&8^%zXr&tr!&HZRW{PMwh9Z`3dD?rG.yL}O)h)>OhRN/n~S,h');
define('SECURE_AUTH_SALT', '>>0eTAP|g6@JoV&hQm^~&QB-ndR|D{5o6*Z|V}E#M-.cpi6x!O:C,Up=cJ E>_:{');
define('LOGGED_IN_SALT',   '&%r6C/An[ O{>0Qw4x.JU8DT7!eeI6t.;._M]/H5t+01|EVr(/~)@@Rdg?<.cf)a');
define('NONCE_SALT',       '2<IV@_Y$L@!ik[K7OQJ:?;H|kw2Xv8| DQ`SxFepc>x@+VXeqPa)kmV1<k^zC=[P');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', false);
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/** Sets up WordPress temp directory for saving temporary files. */
define('WP_TEMP_DIR', ABSPATH . 'wp-content/temp');

 // Enable WP_DEBUG mode
define( 'WP_DEBUG', true );

// Enable Debug logging to the /wp-content/debug.log file
define( 'WP_DEBUG_LOG', true );

// Disable display of errors and warnings 
define( 'WP_DEBUG_DISPLAY', false );
@ini_set( 'display_errors', 0 );