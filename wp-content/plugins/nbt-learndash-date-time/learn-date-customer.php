<?php
/**
 * The plugin add learn date meta box
 *
 *
 * @link              netbaseteam.com
 * @since             1.0.0
 * @package           learndash_date_time
 *
 * @wordpress-plugin
 * Plugin Name:       Netbase LearnDash Date Time
 * Plugin URI:        netbaseteam.com
 * Description:       Plugin made for LearnDash Date Time by Netbaseteam.com.
 * Version:           1.0.0
 * Author:            netbaseteam
 * Author URI:        netbaseteam.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       nbt-learndash-date-time
 * Domain Path:       /languages
 */
// If this file is called directly, abort.
defined('ABSPATH') or die('No script kiddies please!');

class NbtLearnDate {

    public function __construct() {
        add_action('add_meta_boxes', array($this, 'nbt_learn_add_meta_box'));
        add_action('save_post', array($this, 'nbt_learn_date_custom_save'));
        add_action('nbt_learndash_date_hook', array($this, 'nbt_learndash_date_content_html'));
        add_action('admin_init', array($this, 'nbt_learn_date_add_script'));
    }

    // create learn date meta box
    function nbt_learn_add_meta_box() {
        add_meta_box('learn-date-meta', 'LearnDash Date Custom', array($this, 'nbt_learn_date_custom_output'), 'sfwd-courses');
    }

    // call meta box
    function nbt_learn_date_custom_output($post) {

        // get data metabox
        $date_show = get_post_meta($post->ID, '_date_show', true);
        
        $time_start = explode(':',get_post_meta($post->ID, '_nbt_time_start', true));
        $time_end = explode(':',get_post_meta($post->ID, '_nbt_time_end', true));

        $courses_date_start = get_post_meta($post->ID, '_courses_date_start', true);
        $courses_date_end = get_post_meta($post->ID, '_courses_date_end', true);

        $date_start = date_create(str_replace(',', '', $courses_date_start));
        $course_datestart_formart = date_format($date_start, "Y/m/d");

        $date_end = date_create(str_replace(',', '', $courses_date_end));
        $course_dateend_formart = date_format($date_end, "Y/m/d");


        $courses_lang = get_post_meta($post->ID, '_courses_lang', true);

        $date_checked = "";
        if ($date_show) {
            $date_checked = "checked";
        }

        echo ('<div class="sfwd_input panel-checked-show" id="nbtcheckdate"');
        echo ('<span class="sfwd_option_label"><a class="sfwd_help_text_link"><label class="sfwd_label textinput">Enable display </label></a></span>');
        echo ('<span class="sfwd_option_input">');
        echo ('<input type="checkbox" name="_date_show" value="' . $date_show . '" ' . $date_checked . ' >');
        echo ('</span>');
        echo ('</div>');
        echo ('<p style="clear:left"></p>');

        echo ('<div class="sfwd sfwd_options nbt_sfwd_courses_settings">');


        echo ('<div class="sfwd_input panel-time-start"');
        echo ('<span class="sfwd_option_label"><a class="sfwd_help_text_link"><label class="sfwd_label textinput">Time Start</label></a></span>');
        echo ('<span class="sfwd_option_input">');
        echo ('Hrs: <input type="number" value="' . $time_start[0] . '" max="23" min="0" name="hour_time_start"> Mins: <input type="number" value="' . $time_start[1] . '" name="min_time_start" max="59" min="0">');
        echo ('</span>');
        echo ('</div>');

        echo ('<div class="sfwd_input panel-time-end"');
        echo ('<span class="sfwd_option_label"><a class="sfwd_help_text_link"><label class="sfwd_label textinput">Time End</label></a></span>');
        echo ('<span class="sfwd_option_input">');
        echo ('Hrs: <input type="number" value="' . $time_end[0] . '" max="23" min="0" name="hour_time_end"> Mins: <input type="number" value="' . $time_end[1] . '" name="min_time_end" max="59" min="0">');
        echo ('</span>');
        echo ('</div>');

        echo ('<div class="sfwd_input panel-date-start">');
        echo ('<span class="sfwd_option_label"><a class="sfwd_help_text_link"><label class="sfwd_label textinput">Date Start</label></a></span>');
        echo ('<span class="sfwd_option_input"><input type="text" name="_courses_date_start" value="' . $courses_date_start . '" id="datepicker_start"><input type="hidden" value="' . $course_datestart_formart . '" name="datepicker_date_start"></span>');
        echo ('</div>');

        echo ('<div class="sfwd_input panel-date-end">');
        echo ('<span class="sfwd_option_label"><a class="sfwd_help_text_link"><label class="sfwd_label textinput">Date End</label></a></span>');
        echo ('<span class="sfwd_option_input"><input type="text" id="datepicker_end" name="_courses_date_end" value="' . $courses_date_end . '"><input type="hidden" value="' . $course_dateend_formart . '" name="datepicker_date_end"></span>');
        echo ('</div>');

        echo ('<div class="sfwd_input panel-course-langugage">');
        echo ('<span class="sfwd_option_label"><a class="sfwd_help_text_link"><label class="sfwd_label textinput">Language</label></a></span>');
        echo ('<span class="sfwd_option_input"><input type="text" value="' . $courses_lang . '" name="_courses_lang"></span>');
        echo ('</div>');
        echo ('<p style="clear:left"></p>');
        echo ('</div>');
    }

    function nbt_learn_date_custom_save($post_id) {

        $date_show = sanitize_text_field($_POST['_date_show']);

        $hour_time_start = sanitize_text_field($_POST['hour_time_start']);
        $min_time_start = sanitize_text_field($_POST['min_time_start']);

        $hour_time_end = sanitize_text_field($_POST['hour_time_end']);
        $min_time_end = sanitize_text_field($_POST['min_time_end']);

        $courses_date_start = sanitize_text_field($_POST['_courses_date_start']);
        $courses_date_end = sanitize_text_field($_POST['_courses_date_end']);

        $courses_lang = sanitize_text_field($_POST['_courses_lang']);

        if(strlen($hour_time_start) == 1) $hour_time_start = '0'.$hour_time_start;
        if(strlen($min_time_start) == 1) $min_time_start = '0'.$min_time_start;
        if(strlen($hour_time_end) == 1) $hour_time_end = '0'.$hour_time_end;
        if(strlen($min_time_end) == 1) $min_time_end = '0'.$min_time_end;
        
        if($hour_time_start && !$min_time_start) $min_time_start = '00';
       
        if($hour_time_end && !$min_time_end) $min_time_end = '00';

        update_post_meta($post_id, '_date_show', $date_show);
        
        $time_start = $hour_time_start.':'.$min_time_start;
        $time_end = $hour_time_end.':'.$min_time_end;
        
        update_post_meta($post_id, '_nbt_time_start', $time_start);
        update_post_meta($post_id, '_nbt_time_end', $time_end);

        update_post_meta($post_id, '_courses_date_start', $courses_date_start);
        update_post_meta($post_id, '_courses_date_end', $courses_date_end);

        update_post_meta($post_id, '_courses_lang', $courses_lang);
    }

    function nbt_learn_date_add_script() {
        wp_enqueue_script('jquery');
        wp_register_script('learndatejs', plugin_dir_url(__FILE__) . 'assets/js/learndatejs.js');
        wp_enqueue_script('learndatejs');
        wp_register_script('jqueryui', plugin_dir_url(__FILE__) . 'assets/js/jquery-ui-v1.12.0.js');
        wp_enqueue_script('jqueryui');

        wp_register_style('jquery_ui_css', plugins_url('assets/css/jquery-ui.css', __FILE__));
        wp_enqueue_style('jquery_ui_css');
        wp_register_style('wickedpickercss', plugins_url('assets/css/wickedpicker.css', __FILE__));
        wp_enqueue_style('wickedpickercss');
    }

    function nbt_learndash_date_content_html($post = null, $size = '', $attr = '') {

        if ($post = get_post($post)) {
            if ($post->post_type == 'sfwd-courses' && get_post_meta($post->ID, '_date_show', true) == '1') {
                
                $time_start = get_post_meta($post->ID, '_nbt_time_start', true);
                $time_end = get_post_meta($post->ID, '_nbt_time_end', true);

                $courses_date_start = get_post_meta($post->ID, '_courses_date_start', true);
                $courses_date_end = get_post_meta($post->ID, '_courses_date_end', true);
                $courses_lang = get_post_meta($post->ID, '_courses_lang', true);

                $date_start = str_replace(',', '', $courses_date_start);
                $date_end = str_replace(',', '', $courses_date_end);
                $startTimeStamp = strtotime($date_start);
                $endTimeStamp = strtotime($date_end);
                $timeDiff = abs($endTimeStamp - $startTimeStamp);
                $numberDays = $timeDiff / 86400;  // 86400 seconds in one day
                // and you might want to convert to integer
                $numberDays = intval($numberDays + 1);
                
                $courses_time_start = ''; $courses_time_end = '';
                if($time_start) {
                    $time_start_arr = explode(':', $time_start);
                    $courses_time_start = $time_start_arr[0] * 60 + $time_start_arr[1];
                }
                if($time_end) {
                    $time_end_arr = explode(':', $time_end);
                    $courses_time_end = $time_end_arr[0] * 60 + $time_end_arr[1];
                }

                $course_duration = '';
                if ($courses_time_end > $courses_time_start) {
                    $course_duration = $courses_time_end - $courses_time_start;
                }

                $course_duration_hour = '';
                if ($numberDays && $course_duration) {
                    //$course_duration 
                    $course_duration_hour = floor(($course_duration * $numberDays) / 60);
                    $course_duration_minute = ($course_duration * $numberDays) % 60;
                }

                $cats = get_the_category($post->ID);
                $cats_name = array();
                foreach ($cats as $key => $value) {
                    $cats_name[$key] = $value->name;
                }

                $cats_name = implode(', ', $cats_name);

                $course_data = get_post_meta($post->ID, '_sfwd-courses', true);
                // output html
                if ($cats_name) {
                    echo ('<div class="nbt-category-name">');
                    echo (esc_html__('Category: ') . '<span>' . esc_html__($cats_name) . '</span>');
                    echo ('</div>');
                }
                if ($courses_date_start) {
                    echo ('<div class="nbt-date-start">');
                    echo (esc_html__('Start Date: ') . '<span>' . esc_html__(date(get_option('date_format'),strtotime($courses_date_start))) . '</span>');
                    echo ('</div>');
                }

                if ($time_start && $time_end) {
                    echo ('<div class="nbt-range-time">');
                    echo (esc_html__('Time: ') . '<span>' . esc_html__($time_start));
                    echo (' - ' . esc_html__($time_end) . '</span>');
                    echo ('</div>');
                }

                if ($course_duration_hour) {
                    echo ('<div class="nbt-date-duration">');
                    echo (esc_html__('Course Duration: ') . '<span>' . esc_html__($course_duration_hour . ' Hrs '));
                    if ($course_duration_minute) {
                        echo (esc_html__($course_duration_minute . ' Mins'));
                    }
                    echo '</span>';
                    echo ('</div>');
                }
                if ($courses_lang) {
                    echo ('<div class="nbt-date-lang">');
                    echo (esc_html__('Language: ') . '<span>' . esc_html__($courses_lang) . '</span>');
                    echo ('</div>');
                }
                if ($course_data['sfwd-courses_course_price_type']=='free' || !$course_data['sfwd-courses_course_price']){
                    echo ('<div class="nbt-date-price">');
                    echo (esc_html__('Price: ') . '<span>' . esc_html__('Free') . '</span>');
                    echo ("</div>");
                } else {
                    $options = get_option('sfwd_cpt_options');
                    $currency = null;
                    if(!is_null($options)){
                        if(isset($options['modules']) && isset($options['modules']['sfwd-courses_options']) && isset($options['modules']['sfwd-courses_options']['sfwd-courses_paypal_currency'])){
                            $currency = $options['modules']['sfwd-courses_options']['sfwd-courses_paypal_currency'];
                        }
                    }
                    if(is_null($currency)){
                        $currency = 'USD';
                    }
                    $price = $course_data['sfwd-courses_course_price'];
                    if(is_numeric($price)){
                        if($currency == "USD"){
                            $price = '$' . $price;
                        } else {
                            $price .= ' ' . $currency;
                        }
                    }
                    if ($course_data['sfwd-courses_course_price']) {
                        echo ('<div class="nbt-date-price">');
                        echo (esc_html__('Price: ') . '<span>' . esc_html__($price) . '</span>');
                        echo ("</div>");
                    }
                }
            }
        }
    }

}

$NbtLearnDate = new NbtLearnDate();

