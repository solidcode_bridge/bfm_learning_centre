jQuery(function(){
		jQuery( "#datepicker_start" ).datepicker();
		jQuery( "#datepicker_end" ).datepicker();

		if(jQuery('input[name=_date_show]').is(':checked')){
			jQuery('.nbt_sfwd_courses_settings').show();
		}else{
			jQuery('.nbt_sfwd_courses_settings').hide();
		}
		jQuery('#nbtcheckdate :checkbox').change(function(){
			if(jQuery(this).is(':checked')){
				jQuery('input[name=_date_show]').val('1');
				jQuery('input[name=_date_show]').attr('checked', true);

				jQuery('.nbt_sfwd_courses_settings').show();
			}else{
				jQuery('input[name=_date_show]').val('0');
				jQuery('input[name=_date_show]').removeAttr('checked');

				jQuery('.nbt_sfwd_courses_settings').hide();
			}
		});
});