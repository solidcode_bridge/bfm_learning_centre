=== WP-Learndash date customer ===

Display date,time, duration, language learndash
only user Learndash plugin
/**
 * The plugin add learn date meta box
 *
 *
 * @link              netbaseteam.com
 * @since             1.0.0
 * @package           learndash_date_time
 *
 * @wordpress-plugin
 * Plugin Name:       Netbase LearnDash Date Time
 * Plugin URI:        netbaseteam.com
 * Description:       Plugin made for LearnDash Date Time by Netbaseteam.com.
 * Version:           1.0.0
 * Author:            netbaseteam
 * Author URI:        netbaseteam.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       nbt-learndash-date-time
 * Domain Path:       /languages
