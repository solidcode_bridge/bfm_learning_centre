<?php

if (!defined('ABSPATH')) {
	exit;
}
if (!class_exists( 'NBTSOW_Setup' )) {
	class NBTSOW_Setup {
		public function __construct() {
			add_filter( 'siteorigin_widgets_widget_folders', array($this, 'add_widgets') );
			add_filter( 'siteorigin_panels_widget_dialog_tabs', array($this, 'add_widget_tabs'), 20 );
			add_filter( 'siteorigin_panels_widgets', array($this, 'add_bundle_groups'), 11 );
			add_filter( 'siteorigin_panels_row_style_fields', array($this, 'row_effect_option') );
			add_filter( 'siteorigin_panels_row_style_attributes', array($this, 'row_effect_attribute'), 10, 3 );
//			add_filter( 'siteorigin_widgets_default_active', array($this, 'filter_default_widgets') );
			add_filter('siteorigin_widgets_active_widgets', array($this, 'filter_active_widgets'));
			// add_filter( 'siteorigin_panels_row_style_fields', array($this, 'row_margin_option') );
			// add_filter( 'siteorigin_panels_row_style_attributes', array($this, 'row_margin_attributes'), 10, 4 );
			// add_filter('siteorigin_panels_css_object', array($this, 'add_attributes_to_css_object'), 10, 3);

			add_filter( 'siteorigin_widgets_icon_families', array($this, 'my_icon_families_filter' ));
			add_action( 'wp_enqueue_scripts', array($this, 'education_scripts') );
		}

		// Get all widget
		function add_widgets($folders) {
			$folders[] = NBTSOW_PLUGIN_DIR . 'includes/widgets/';
			return $folders;
		}

		//Create 'NetBaseTeam SiteOrigin Widget' tab
		function add_widget_tabs($tabs) {
			$tabs[] = array(
				'title' => esc_html__('NetBaseTeam SiteOrigin Widgets', 'nbtsow'),
				'filter' => array(
					'groups' => array('nbtsow-widgets')
				)
			);
			return $tabs;
		}

		// Get all NetBaseTeam Widget to put to tab
		function add_bundle_groups($widgets) {
			foreach ($widgets as $class => &$widget) {
				if (preg_match('/NBTSOW_(.*)_Widget/', $class, $matches)) {
					$widget['groups'] = array('nbtsow-widgets');
				}
			}
			return $widgets;
		}

		// Default active on all widget
//		function filter_default_widgets($widgets) {
//			$widgets = array(
//				'nbtsow-blog-posts-widget',
//				'nbtsow-cats-widget',
//				'nbtsow-collection-widget',
//				'nbtsow-cta-widget',
//				'nbtsow-custom-tabs-widget',
//				'nbtsow-headline-widget',
//				'nbtsow-help-topic-widget',
//				'nbtsow-icon-button-widget',
//				'nbtsow-icon-widget',
//				'nbtsow-image-widget',
//				'nbtsow-learndash-latest-posts-widget',
//				'nbtsow-learndash-popular-widget',
//				'nbtsow-learndash-related-widget',
//				'nbtsow-learndash-search-widget',
//				'nbtsow-out-teams-widget',
//				'nbtsow-post-carousel-widget',
//				'nbtsow-taxonomy-widget',
//				'nbtsow-testimonial-widget'
//			);
//
////			$widgets[] = 'nbtsow-blog-posts-widget';
//
//			return $widgets;
//		}

		function filter_active_widgets($active){
			$active['nbtsow-blog-posts-widget'] = true;
			$active['nbtsow-cats-widget'] = true;
			$active['nbtsow-collection-widget'] = true;
			$active['nbtsow-cta-widget'] = true;
			$active['nbtsow-custom-widget'] = true;
			$active['nbtsow-custom-collapse-widget'] = true;
			$active['nbtsow-custom-tabs-widget'] = true;
			$active['nbtsow-eventespresso-widget'] = true;
			$active['nbtsow-headline-widget'] = true;
			$active['nbtsow-help-topic-widget'] = true;
			$active['nbtsow-icon-button-widget'] = true;
			$active['nbtsow-icon-widget'] = true;
			$active['nbtsow-image-widget'] = true;
			$active['nbtsow-learndash-latest-posts-widget'] = true;
			$active['nbtsow-learndash-popular-widget'] = true;
			$active['nbtsow-learndash-related-widget'] = true;
			$active['nbtsow-learndash-search-widget'] = true;
			$active['nbtsow-out-teams-widget'] = true;
			$active['nbtsow-post-carousel-widget'] = true;
			$active['nbtsow-taxonomy-widget'] = true;
			$active['nbtsow-testimonial-widget'] = true;
			return $active;
		}

		// Add parallax option to row
		function row_effect_option($fields) {

			$fields['img-hover'] = array(
				'name' => esc_html__('Image Hover Effect', 'nbtsow'),
				'type' => 'checkbox',
				'group' => 'design',
				'description' => esc_html__('Hover effect for images in this row', 'nbtsow'),
				'priority' => 8,
			);

			$fields['parallax'] = array(
				'name' => esc_html__('Parallax Effect', 'nbtsow'),
				'type' => 'checkbox',
				'group' => 'design',
				'description' => esc_html__('Parallax effect for background image', 'nbtsow'),
				'priority' => 9,
			);

			return $fields;

		}		

		// Parallax attribute
		function row_effect_attribute($attributes, $args) {

			if( !empty($args['img-hover']) ) {
				array_push($attributes['class'], 'nbtsow-img-hover');
			}

			if( !empty($args['parallax']) ) {
				array_push($attributes['class'], 'nbtsow-parallax');
			}
			
			return $attributes;

		}

		// Add Margin bottom in different screen size
		// function row_margin_option($fields) {
		// 	$fields['mobile_margin'] = array(
		// 		'name' => esc_html__('Mobile Margin', 'nbtsow'),
        //         'type' => 'measurement',
        //         'group' => 'layout',
        //         'description' => esc_html__('Mobile bottom margin for the row below 768px.', 'nbtsow'),
        //         'priority' => 2,
        //         'multiple' => true
		// 	);
		//
		// 	$fields['tablet_margin'] = array(
		// 		'name' => esc_html__('Tablet margin', 'nbtsow'),
        //         'type' => 'measurement',
        //         'group' => 'layout',
        //         'description' => esc_html__('Tablet bottom margin for the rowon screen 768px to 991px.', 'nbtsow'),
        //         'priority' => 3,
        //         'multiple' => true
		// 	);
		//
		// 	$fields['laptop_margin'] = array(
		// 		'name' => esc_html__('Laptop margin', 'nbtsow'),
        //         'type' => 'measurement',
        //         'group' => 'layout',
        //         'description' => esc_html__('Bottom margin for the row on screen 992px to 1024px.', 'nbtsow'),
        //         'priority' => 4,
        //         'multiple' => true
		// 	);
		//
		// 	return $fields;
		// }
		//
		// // Margin bottom attributes
		// function row_margin_attributes($attributes, $args) {
		// 	if( !empty($args['mobile_margin']) || !empty($args['tablet_margin']) || !empty($args['laptop_margin']) ) {
		// 		array_push($attributes['class'], 'nbtsow-row');
		// 	}
		//
		// 	return $attributes;
		// }

		// Add custom attributes to css object
		// function add_attributes_to_css_object($css, $panels_data, $post_id) {
		// 	foreach( $panels_data['grids'] as $gi => $grid ) {
		// 		$grid_id = !empty($grid['style']['id']) ? (string)sanitize_html_class($grid['style']['id']) : intval($gi);
		//
		// 		$margin = (isset($grid['style']['laptop_margin']) ? $grid['style']['laptop_margin'] : null);
		// 		if($margin) {
		// 			$css->add_row_css($post_id, $grid_id, '.nbtsow-row', array('margin-bottom' => $margin), 1024);
		// 		}
		//
		// 		$margin = (isset($grid['style']['tablet_margin']) ? $grid['style']['tablet_margin'] : null);
		// 		if($margin) {
		// 			$css->add_row_css($post_id, $grid_id, '.nbtsow-row', array('margin-bottom' => $margin), 992);
		// 		}
		//
		// 		$margin = (isset($grid['style']['mobile_margin']) ? $grid['style']['mobile_margin'] : null);
		// 		if($margin) {
		// 			$css->add_row_css($post_id, $grid_id, '.nbtsow-row', array('margin-bottom' => $margin), 768);
		// 		}
		// 	}
		//
		// 	return $css;
		// }

		function my_icon_families_filter( $icon_families ) {
	    	$icon_families['nbticon'] = array(
		        'name' => __( 'nbticon', 'example-text-domain' ),
		        // 'style_uri' => plugin_dir_url( __FILE__ ) . 'assets/css/nbtico.css',
		        'style_uri' => NBTSOW_PLUGIN_URL . 'assets/css/nbtico.css',
		        'icons' => array(
		            'nbticon-book-open' => '&#xe95f;',
		            'nbticon-suitcase-1' => '&#xe97e;',
		            'nbticon-ticket-1' => '&#xe98d;',
		            'nbticon-pencil-alt' => '&#xeb0c;',
		            'nbticon-share-2' => '&#xeb2a;',
		            'nbticon-award-2' => '&#xebbd;',
		            'nbticon-art-gallery' => '&#xec2c;',
		            'nbticon-college' => '&#xec38;',
		            'nbticon-laptop-2' => '&#xed9a;',
		            'nbticon-location-2' => '&#xee0f;',
		            'nbticon-cog' => '&#xee14;',
					'nbticon-multiple-users-silhouette' =>'&#xe900;',
					'nbticon-books-stack2' =>'&#xe901;',
					'nbticon-books-stack' =>'&#xe902;',
					'nbticon-college-studying2' =>'&#xe903;',
					'nbticon-college-studying' =>'&#xe904;',
					'nbticon-old-fashion-briefcase' =>'&#xe905;',
					'nbticon-draw-a-picture2' =>'&#xe906;',
					'nbticon-draw-a-picture' =>'&#xe907;',
					'nbticon-laptop' =>'&#xe908;',
					'nbticon-html-5-logotype' =>'&#xe909;',
					'nbticon-website-design-symbol2' =>'&#xe90a;',
					'nbticon-website-design-symbol' =>'&#xe90b;',
					'nbticon-black-graduation-cap2' =>'&#xe90c;',
					'nbticon-black-graduation-cap' =>'&#xe90d;',
		            'nbticon-chevron-with-circle-right' => '&#xe90e;',
					'nbticon-up-arrow2' =>'&#xe90f;',
					'nbticon-up-arrow3' =>'&#xe910;',
					'nbticon-up-arrow4' =>'&#xe911;',
					'nbticon-up-arrow' =>'&#xe912;',
					'nbticon-bus' =>'&#xe913;',
					'nbticon-help' =>'&#xe914;',
					'nbticon-help-button' =>'&#xe915;',
					'nbticon-padlock2' =>'&#xe916;',
					'nbticon-padlock' =>'&#xe917;',
					'nbticon-open-padlock2' =>'&#xe918;',
					'nbticon-open-padlock' =>'&#xe919;',
					'nbticon-note' =>'&#xe91a;',
					'nbticon-note-outlined-symbol' =>'&#xe91b;',
					'nbticon-font-selection-editor' =>'&#xe91c;',
					'nbticon-phone-call' =>'&#xe91d;',
					'nbticon-placeholder' =>'&#xe91e;',
					'nbticon-big-speech-balloon' =>'&#xe91f;',
					'nbticon-speech-bubble' =>'&#xe920;',
					'nbticon-black-user-shape' =>'&#xe921;',
					'nbticon-fullscreen' =>'&#xe922;',
					'nbticon-icon1' =>'&#xe923;',
					'nbticon-icon-(1)' =>'&#xe924;',
					'nbticon-quotations2' =>'&#xe925;',
					'nbticon-quotations' =>'&#xe926;',
					'nbticon-linkedin-logo' =>'&#xe927;',
					'nbticon-google-plus-logo' =>'&#xe928;',
					'nbticon-twitter' =>'&#xe929;',
					'nbticon-facebook' =>'&#xe92a;',
					'nbticon-skype-logo' =>'&#xe92b;',
					'nbticon-close-envelope' =>'&#xe92c;',
					'nbticon-icon' =>'&#xe92d;',
					'nbticon-location-pin' =>'&#xe92e;',
					'nbticon-play-button' =>'&#xe92f;',
					'nbticon-pdf' =>'&#xe930;',
					'nbticon-envelope' =>'&#xe931;',
					'nbticon-profile' =>'&#xe932;',
					'nbticon-left-quote2' =>'&#xe933;',
					'nbticon-left-quote' =>'&#xe934;',
					'nbticon-calendar-(2)' =>'&#xe935;',
					'nbticon-schedule-button' =>'&#xe936;',
					'nbticon-favourite-symbol' =>'&#xe937;',
					'nbticon-coin-icon' =>'&#xe938;',
					'nbticon-characters-interface-sign' =>'&#xe939;',
					'nbticon-teacher-writing-on-whiteboard' =>'&#xe93a;',
					'nbticon-teacher-reading' =>'&#xe93b;',
					'nbticon-plus-zoom-or-search-symbol-of-interface2' =>'&#xe93c;',
					'nbticon-plus-zoom-or-search-symbol-of-interface' =>'&#xe93d;',
					'nbticon-search2' =>'&#xe93e;',
					'nbticon-search' =>'&#xe93f;',
					'nbticon-left-arrow2' =>'&#xe940;',
					'nbticon-left-arrow' =>'&#xe941;',
					'nbticon-book' =>'&#xe942;',
					'nbticon-quotes-left' =>'&#xe977;',
					'nbticon-quotes-right' =>'&#xe978;',
					'nbticon-businesswoman' =>'&#xe957;',
					'nbticon-book2' =>'&#xe960;',
					'nbticon-lesson' =>'&#xe961;',
					'nbticon-student' =>'&#xe963;',
					'nbticon-managerprofile' =>'&#xe962;'

				),
		    );
	    return $icon_families;
		}
		function education_scripts() {
			// wp_enqueue_style( 'nbticon-style', get_template_directory_uri() . '/css/nbtico.css');
			if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
				wp_enqueue_script( 'comment-reply' );
			}
		}
	}
}

new NBTSOW_Setup();
