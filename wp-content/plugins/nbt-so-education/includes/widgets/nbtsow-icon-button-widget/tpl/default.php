<?php if (!empty($title)):?>
    <div class="nbtsow-iconbt-title">
    <?php echo $title; ?>
    </div>
<?php endif ?>
<ul class="nbtsow-iconbt-list-wrap unstyled">
    <?php foreach($icon_list as $icon_section):
        $icon_styles = array();
        if(!empty($icon_section['select']['icon_size'])) 
            $icon_styles[] = 'font-size: ' . intval($icon_section['select']['icon_size']).'px';
        if(!empty($icon_section['select']['icon_color'])) 
            $icon_styles[] = 'color: ' . $icon_section['select']['icon_color'];
    ?>
        <li class="nbt-iconbt-section">
            <div>
                <?php if($icon_section['select']['icon']):?>
                    <span class="icon-wrap">
                    <?php  echo siteorigin_widget_get_icon( $icon_section['select']['icon'], $icon_styles );?>
                    </span>
                <?php endif;?>
                <div class="nbt-iconbt-content">
                    <?php if($icon_section['headline']['text']): ?>
                        <<?php echo $icon_section['headline']['tag']; ?> class="nbtsow-iconbt-headline">
                            <?php echo esc_attr($icon_section['headline']['text']); ?>
                        </<?php echo $icon_section['headline']['tag']; ?>>
                    <?php endif ;?>
                    <?php if($icon_section['first_sub']['text']): ?>
                        <<?php echo $icon_section['first_sub']['tag']; ?> class="nbtsow-iconbt-firstsub">
                            <?php echo esc_attr($icon_section['first_sub']['text']); ?>
                        </<?php echo $icon_section['first_sub']['tag']; ?>>
                    <?php endif ;?>
                    <?php if($icon_section['second_sub']['text']): ?>
                        <<?php echo $icon_section['second_sub']['tag']; ?> class="nbtsow-iconbt-second-sub">
                            <?php echo esc_attr($icon_section['second_sub']['text']); ?>
                        </<?php echo $icon_section['second_sub']['tag']; ?>>
                    <?php endif ;?>
                    <?php if($icon_section['button']['text']): ?>
                    <a class="nbt-iconbt-readmore" href="<?php if(!empty ($icon_section['button']['href'])){
                       echo esc_url($icon_section['button']['href']); 
                    } ?>">
                        <?php echo $icon_section['button']['text']; ?>
                    </a>
                <?php endif;?>
                </div>
            </div>
        </li>
    <?php endforeach; ?>

</ul>
