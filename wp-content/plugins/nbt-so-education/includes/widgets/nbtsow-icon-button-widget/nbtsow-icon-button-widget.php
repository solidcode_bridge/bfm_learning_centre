<?php

/*
Widget Name: NetBaseTeam Icon Button Widget
Description: Widget to choose icon, text and button
Author: NetBaseTeam
Author URI: http://netbaseteam.com
*/

class NBTSOW_Icon_Button_Widget extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'nbtsow-icon-button-widget',
			esc_html__('NetBaseTeam Icon Button Widget', 'nbtsow'),
			array(
				'description' => esc_html__(' Widget to choose icon, text and button', 'nbtsow'),
			),
			array(),
			array(
				'title' => array(
                    'type' => 'text',
                    'label' => __('Title', 'nbtsow'),
                ),
				'icon_list' => array(
                    'type' => 'repeater',
                    'label' => __('Icon List', 'nbtsow'),
                    'item_name' => __('Icon', 'nbtsow'),
                    'item_label' => array(
                        'selector' => "[id*='icon_list-title']",
                        'update_event' => 'change',
                        'value_method' => 'val'
                    ),
                    'fields' =>array(
	                    'select' => array(
							'type' => 'section',
							'label' => esc_html__( 'Select icon', 'nbtsow' ),
							'hide' => false,
		                    'fields' => array(
		                        'icon' => array(
		                            'type' => 'icon',
		                            'label' => __('Icon.', 'nbtsow')
		                        ),
								'icon_color' => array(
									'type' => 'color',
									'label' => __('Icon Color', 'nbtsow')
								),
								'icon_size' => array(
									'type' => 'slider',
									'label' => __('Icon Size', 'nbtsow'),
									'min' => 1,
									'max' => 64,
									'integer' => true,
									'default' => 24
								),	
		                    ),
		                ),
	                    'headline' => array(
							'type' => 'section',
							'label' => esc_html__( 'Headline', 'nbtsow' ),
							'hide' => false,
							'fields' => array(
								'text' => array(
									'type' => 'text',
									'label' => esc_html__( 'Headline Text', 'nbtsow' ),
								),
								'tag' => array(
									'type' => 'select',
									'label' => esc_html__( 'HTML Tag', 'nbtsow' ),
									'default' => 'h2',
									'options' => array(
										'h1' => esc_html__( 'H1', 'nbtsow' ),
										'h2' => esc_html__( 'H2', 'nbtsow' ),
										'h3' => esc_html__( 'H3', 'nbtsow' ),
										'h4' => esc_html__( 'H4', 'nbtsow' ),
										'h5' => esc_html__( 'H5', 'nbtsow' ),
										'h6' => esc_html__( 'H6', 'nbtsow' ),
										'p' => esc_html__( 'Paragraph', 'nbtsow' ),
									)
								),
							),
						),
						'first_sub' => array(
							'type' => 'section',
							'label' => esc_html__( 'Sub Headline', 'nbtsow' ),
							'hide' => true,
							'fields' => array(
								'text' => array(
									'type' => 'text',
									'label' => esc_html__( 'Text', 'nbtsow' ),
								),
								'tag' => array(
									'type' => 'select',
									'label' => esc_html__( 'HTML Tag', 'nbtsow' ),
									'default' => 'h3',
									'options' => array(
										'h1' => esc_html__( 'H1', 'nbtsow' ),
										'h2' => esc_html__( 'H2', 'nbtsow' ),
										'h3' => esc_html__( 'H3', 'nbtsow' ),
										'h4' => esc_html__( 'H4', 'nbtsow' ),
										'h5' => esc_html__( 'H5', 'nbtsow' ),
										'h6' => esc_html__( 'H6', 'nbtsow' ),
										'p' => esc_html__( 'Paragraph', 'nbtsow' ),
									)
								),
							),
						),
						'second_sub' => array(
							'type' => 'section',
							'label' => esc_html__( 'Another Sub Headline', 'nbtsow' ),
							'hide' => true,
							'fields' => array(
								'text' => array(
									'type' => 'text',
									'label' => esc_html__( 'Text', 'nbtsow' ),
								),
								'tag' => array(
									'type' => 'select',
									'label' => esc_html__( 'HTML Tag', 'nbtsow' ),
									'default' => 'p',
									'options' => array(
										'h1' => esc_html__( 'H1', 'nbtsow' ),
										'h2' => esc_html__( 'H2', 'nbtsow' ),
										'h3' => esc_html__( 'H3', 'nbtsow' ),
										'h4' => esc_html__( 'H4', 'nbtsow' ),
										'h5' => esc_html__( 'H5', 'nbtsow' ),
										'h6' => esc_html__( 'H6', 'nbtsow' ),
										'p' => esc_html__( 'Paragraph', 'nbtsow' ),
									)
								),
							),
						),
						'button' => array(
							'type' => 'section',
							'label' => esc_html__( 'Button', 'nbtsow' ),
							'hide' => true,
							'fields' => array(
								'text' => array(
									'type' => 'text',
									'label' => esc_html__('Button Text', 'nbtsow'),
								),
								'href' => array(
									'type' => 'text',
									'label' => esc_html__('Button Link', 'nbtsow'),
								),
							),
						),
						'order' => array(
							'type' => 'order',
							'label' => esc_html__( 'Element Order', 'nbtsow' ),
							'options' => array(
								'selecticon' => esc_html__( 'Select icon', 'nbtsow' ),
								'headline' => esc_html__( 'Headline', 'nbtsow' ),
								'first_sub' => esc_html__( 'First Sub Headline', 'nbtsow' ),
								'second_sub' => esc_html__( 'Second Sub Headline', 'nbtsow' ),
								'button' => esc_html__( 'Button', 'nbtsow' ),
							),
							'default' => array('selecticon','headline', 'first_sub', 'second_sub','button'),
						)
					)
				),
			)
		);
	}

	function get_template_variables($instance, $args) {
		return array(
			'title' => $instance['title'],
			'icon_list' => !empty($instance['icon_list']) ? $instance['icon_list'] : array(),
		);
	}

	function get_template_name($instance) {
		return 'default';
	}

	function get_style_name($instance) {
		return '';
	}
}

siteorigin_widget_register('nbtsow-icon-button-widget', __FILE__, 'NBTSOW_Icon_Button_Widget');
