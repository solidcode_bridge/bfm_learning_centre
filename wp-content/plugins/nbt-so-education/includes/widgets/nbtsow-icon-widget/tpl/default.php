<?php if (!empty($title)):?>
    <div class="nbtsow-icon-title">
    <?php echo $title; ?>
    </div>
 <?php endif ?>
<div class="nbtsow-icon-list-wrap">
    <?php foreach($icon_list as $icon_section):
		$icon_styles = array();
		if(!empty($icon_section['icon_size'])) 
			$icon_styles[] = 'font-size: ' . intval($icon_section['icon_size']).'px';
		if(!empty($icon_section['icon_color'])) 
			$icon_styles[] = 'color: ' . $icon_section['icon_color'];
		?>
		<div class="nbtsow-icon-section">
			<?php if($icon_section['icon']): ?>
				<?php if($icon_section['icon_url']): ?>
					<a href="<?php echo esc_url($icon_section['icon_url']); ?>">
				<?php endif; ?>
						<span class="icon-wrap">
							<?php  echo siteorigin_widget_get_icon( $icon_section['icon'], $icon_styles );?>
						</span>
				<?php if($icon_section['icon_url']): ?>
					</a>     
				<?php endif; ?>
			<?php endif;?>
			<div class="nbtsow-icon-content">
				<?php if( !empty($icon_section['icon_title_style']) ) :
					echo '<' . $title_style . ' class="nbtsow-icon-title-style">';
					if($icon_section['icon_url']): ?>
						<a href="<?php echo esc_url($icon_section['icon_url']); ?>">
					<?php endif;
							echo $icon_section['icon_title_style'];
					if($icon_section['icon_url']): ?>
						</a>     
					<?php endif;
					echo '</' . $title_style . '>';
				endif ?>
				<?php if($icon_section['icon_text']): ?>
					<p class="nbtsow-icon-text">
						<?php echo esc_attr($icon_section['icon_text']); ?>
					</p>
				<?php endif ;?>
				<?php if($icon_section['icon_second_text']): ?>
					<p class="nbtsow-icon-second-text">
						<?php echo esc_attr($icon_section['icon_second_text']); ?>
					</p>
				<?php endif ;?>
			</div>
		</div>
    <?php endforeach; ?>
</div>
