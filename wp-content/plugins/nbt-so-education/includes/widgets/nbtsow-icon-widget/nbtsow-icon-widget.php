<?php

/*
  Widget Name: NetBaseTeam Icon Widget
  Description: Widget to choose icon
  Author: NetBaseTeam
  Author URI: http://netbaseteam.com
 */

class NBTSOW_Icon_Widget extends SiteOrigin_Widget {

    function __construct() {
        parent::__construct(
            'nbtsow-icon-widget', esc_html__('NetBaseTeam Icon Widget', 'nbtsow'), array(
                'description' => esc_html__(' Widget to choose icon', 'nbtsow'),
            ), array(), array(
                'title' => array(
                    'type' => 'text',
                    'label' => __('Title', 'nbtsow'),
                ),
                'title_style' => array(
                    'type' => 'select',
                    'label' => esc_html__('HTML Tag of Title', 'nbtsow'),
                    'default' => 'h3',
                    'options' => array(
                        'h1' => esc_html__('H1', 'nbtsow'),
                        'h2' => esc_html__('H2', 'nbtsow'),
                        'h3' => esc_html__('H3', 'nbtsow'),
                        'h4' => esc_html__('H4', 'nbtsow'),
                        'h5' => esc_html__('H5', 'nbtsow'),
                        'h6' => esc_html__('H6', 'nbtsow'),
                    )
                ),
                'icon_list' => array(
                    'type' => 'repeater',
                    'label' => esc_html__('Icon List', 'nbtsow'),
                    'item_name' => esc_html__('Icon', 'nbtsow'),
                    'item_label' => array(
                        'selector' => "[id*='icon_list-title']",
                        'update_event' => 'change',
                        'value_method' => 'val'
                    ),
                    'fields' => array(
                        'icon' => array(
                            'type' => 'icon',
                            'label' => esc_html__('Icon.', 'nbtsow')
                        ),
                        'icon_color' => array(
                            'type' => 'color',
                            'label' => esc_html__('Icon Color', 'nbtsow')
                        ),
                        'icon_size' => array(
                            'type' => 'slider',
                            'label' => esc_html__('Icon Size', 'nbtsow'),
                            'min' => 1,
                            'max' => 64,
                            'integer' => true,
                            'default' => 24
                        ),
                        'icon_title_style' => array(
                            'type' => 'text',
                            'label' => esc_html__('Title for icon section', 'nbtsow'),
                        ),
                        'icon_text' => array(
                            'type' => 'text',
                            'label' => esc_html__('Text for icon section', 'nbtsow'),
                        ),
                        'icon_second_text' => array(
                            'type' => 'text',
                            'label' => esc_html__('Second text for icon section', 'nbtsow'),
                        ),
                        'icon_url' => array(
                            'type' => 'link',
                            'label' => esc_html__('Icon URL', 'nbtsow'),
                        ),
                    )
                ),
            )
        );
    }

    function get_template_variables($instance, $args) {
        return array(
            'title' => $instance['title'],
            'title_style' => $instance['title_style'],
            'icon_list' => !empty($instance['icon_list']) ? $instance['icon_list'] : array(),
        );
    }

    function get_template_name($instance) {
        return 'default';
    }

    function get_style_name($instance) {
        return '';
    }

}

siteorigin_widget_register('nbtsow-icon-widget', __FILE__, 'NBTSOW_Icon_Widget');
