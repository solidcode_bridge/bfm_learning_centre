<?php

/*
  Widget Name: NetBaseTeam Learndash Search
  Description: NetBaseTeam Learndash Course Search.
  Author: NetBaseTeam
  Author URI: https://netbaseteam.com
 */

class NBTSOW_Learndash_Search_Widget extends SiteOrigin_Widget {

    function __construct() {
        parent::__construct(
			'nbtsow-learndash-search-widget', esc_html__('NetBaseTeam Learndash Search Widget', 'nbtsow'), array(
				'description' => esc_html__('Display Learndash Search Form', 'nbtsow'),
			), array(), array(
				'searchname' => array(
					'type' => 'text',
					'label' => esc_html__('Enter search name: ', 'nbtsow'),
					'default' => '',
				),
				'description' => array(
					'type' => 'textarea',
					'label' => esc_html__('Enter search description:  ', 'nbtsow'),
					'default' => '',
				),
				'category' => array(
					'type' => 'select',
					'label' => esc_html__('Select category search'),
					'multiple' => true,
					'options' => $this->getCategories()
				)
            )
        );

        add_action('pre_get_posts', array($this, 'nbtsow_search_filter'));
    }
	
	function initialize() {
        $this->register_frontend_styles(
			array(
				array(
					'nbtsow-search',
					plugin_dir_url(__FILE__) . 'css/style.css',
					array(),
					''
				)
			)
        );
    }

    function get_template_variables($instance, $args) {
        return array(
            'title' => !empty($instance['title']) ? $instance['title'] : '',
            'searchname' => !empty($instance['searchname']) ? $instance['searchname'] : '',
            'description' => $instance['description'],
            'category' => $instance['category'],
        );
    }

    function get_template_name($instance) {
        return 'default';
    }

    function get_style_name($instance) {
        return '';
    }

    function getCategories() {
        $term_names = get_terms('category', 'orderby=name&fields=id=>name&hide_empty=0');
		$term_slugs = get_terms('category', 'orderby=name&fields=id=>slug&hide_empty=0');
		$categories = array();
		foreach ($term_slugs as $key => $term_slug){
			$categories[$term_slug] = $term_names[$key];
		}
		return $categories;
    }

    function nbtsow_search_filter($query) {
        if (!is_admin() && $query->is_main_query()) {
            if ($query->is_search) {
                $cat = empty($_GET['cat']) ? '' : $_GET['cat'];

                $catid = '';
                if ($cat) {
                    $idObj = get_category_by_slug($cat);
                    $catid = $idObj->term_id;
                }

                $query->set('cat', $catid);
                
                $query->set('post_type', 'sfwd-courses');
            }
        }
    }

}

siteorigin_widget_register('nbtsow-learndash-search-widget', __FILE__, 'NBTSOW_Learndash_Search_Widget');

