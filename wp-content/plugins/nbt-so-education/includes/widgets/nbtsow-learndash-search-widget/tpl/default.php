
<form class="form-horizontal" action="<?php echo get_bloginfo("url"); ?>" method="get">
    <?php if ($searchname) : ?>
        <div class="title-search"><?php echo $searchname;?></div>
    <?php endif; ?>
    <?php if ($description) : ?>
        <div class="desc-search"><?php echo $description;?></div>
    <?php endif; ?>
    <div class="row row-search">
        <!-- Text input-->
        <div class="col-search-text">
            <input class="education-search-text" type="search" placeholder="Courses Name" name="s" value="" />
        </div>
        <!-- Select Category -->
        <div class="col-search-cat">
            <select id="cats" name="cat" class="education-search-category">
                <option value=""><?php echo esc_html__('Select Category', 'education'); ?></option>
                <?php foreach($category as $key=>$value):?>
                    <option value="<?php echo $value?>"><?php echo get_term_by('slug', $value, 'category')->name;?></option>
                <?php endforeach;?> 
            </select>
        </div>
        <!-- Button -->
        <div class="col-search-submit education-search-button">
            <input type="submit" value="Search" class="education-search-submit">
            <input type="hidden" value="sfwd-courses" name="post_type">
        </div>
    </div>
</form>
