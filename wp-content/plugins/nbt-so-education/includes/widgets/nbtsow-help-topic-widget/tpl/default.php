
<?php
/**
 * @var $design
 * @var $settings
 * @var $help-topic
 */
$autoplay = ($settings['autoplay']) ? 'true' : 'false';
$number = $settings['numbershow'] ? $settings['numbershow'] : 3;
$nav = ($settings['showbutton']) ? 'true' : 'false';
$dot = ($settings['showdot']) ? 'true' : 'false';
$speed = $settings['timeplay'] ? $settings['timeplay'] : 0;
$category = implode(',', $instance['category']);
$help_args = array(
    'post_type' => 'post',
    'cat' => $category,
    'no_found_rows' => true,
    'post_status' => 'publish',
);
$help_loop = new WP_Query($help_args);
if ( $help_loop->have_posts() ) {
    ?>
    <?php if(!empty($title)) {
        echo '<h3 class="nbtsow-title">' . $title . '</h3>';
    }
    ?>
    <ul class="owl-carousel unstyled nbt-help-topic-carousel<?php echo $instance['_sow_form_id']; ?>">
    <?php
    while ($help_loop->have_posts()): $help_loop->the_post();
    ?>
        <li class="nbtsow-help-topic">
            <div class="nbtsow-help-details">
                <h4 class="nbtsow-help-title">
                    <a href="<?php the_permalink(); ?>">
                        <?php the_title(); ?>
                    </a>
                </h4>
                <p class="nbtsow-help-excerpt">
                    <?php echo esc_html(get_the_excerpt()); ?>
                </p>
                <div class="nbtsow-layout-style-readmore">
                <a href="<?php echo get_permalink();?>">Read More</a>
            </div>
            </div>
        </li>
    <?php
    endwhile;
    ?>
    </ul>
    <?php
    wp_reset_postdata();
}
?>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        var owl = $('.nbt-help-topic-carousel<?php echo $instance["_sow_form_id"]; ?>'); // save reference to 
        $('.nbt-help-topic-carousel<?php echo $instance["_sow_form_id"]; ?>').owlCarousel({
			rtl:<?php echo is_rtl()?'true':'false'; ?>,
            items: <?php echo $number; ?>,
            stagePadding: 0,
            loop: true,
            margin: 10,
            smartSpeed: 450,
            dotData: true,
            dots: <?php echo $dot;?>,
            nav: <?php echo $nav; ?>,
            autoplay:<?php echo $autoplay; ?>,
            slideSpeed: <?php echo $speed; ?>

        });
    });

</script>