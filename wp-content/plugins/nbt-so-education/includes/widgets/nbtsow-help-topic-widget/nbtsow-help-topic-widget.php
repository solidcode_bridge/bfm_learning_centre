<?php

/*
  Widget Name: Netbaseteam help topic
  Description: Display some help topic.
  Author: Netbaseteam
  Author URI: https://Netbaseteam.com
 */

class NBTsow_Help_Topic_Widget extends SiteOrigin_Widget {

    function initialize() {
        $this->register_frontend_styles(array(
            array(
                'nbtsow-help-topic',
                plugin_dir_url(__FILE__) . 'css/style.css'
            )
        ));
    }

    function __construct() {
        parent::__construct(
            'nbtsow-help-topic',
            esc_html__('NetbaseTeam help topic', 'nbtsow'),
            array(
                'description' => esc_html__('Display some carousel help topic ', 'nbtsow'),
				'panels_groups' => array('nbtsow-widgets')
            ),
            array(),
            array(
                'title' => array(
                    'type' => 'text',
                    'label' => esc_html__('Title', 'so-widgets-bundle'),
                ),
                'category' => array(
                    'type' => 'text',
                    'label' => esc_html__('ID category 1,2,3...', 'nbtsow'),
                ),
                'settings' => array(
                    'type' => 'section',
                    'label' => esc_html__('Settings', 'nbtsow'),
                    'fields' => array(
                        'numbershow' => array(
                            'type' => 'slider',
                            'label' => __('Number help show', 'nbtsow'),
                            'integer' => true,
                            'default' => 3,
                            'max' => 15,
                            'min' => 1,
                        ),
                        'showbutton' => array(
                            'type' => 'checkbox',
                            'label' => esc_html__('Show carousel buttons Next/Prev', 'nbtsow'),
                        ),
                        'showdot' => array(
                            'type' => 'checkbox',
                            'label' => __('Show carousel dot', 'nbtsow'),
                        ),
                        'autoplay' => array(
                            'type' => 'checkbox',
                            'label' => esc_html__('Testimonial auto play ', 'nbtsow'),
                        ),
                        'timeplay' => array(
                            'type' => 'text',
                            'label' => esc_html__('Testimonial time auto play ', 'nbtsow'),
                            'default' => 3000
                        ),
                    ),
                ),
            )
        );
    }

    
    function get_template_variables($instance, $args) {
        return array(
            'title' => !empty($instance['title']) ? $instance['title'] : '',
            'category' => $instance['category'],
            'settings' => $instance['settings']
        );
    }
    function get_template_name($instance) {
        return 'default';
    }

    function get_style_name($instance) {
        return '';
    }


}

siteorigin_widget_register('nbtsow-help-topic', __FILE__, 'NBTsow_Help_Topic_Widget');
