
<?php
global $post;

$newdataposts = array();
if (is_single($post)) {
    $relateds = array();

    switch ($related_type) {
        case 'category':
            $relateds = get_posts(array(
                'post_type' => $post_type,
                'orderby'          => $orderby,
                'order'            => $order,
                'category__in' => wp_get_post_categories($post->ID),
                'numberposts' => $post_number,
                'post__not_in' => array($post->ID))
            );
            break;
        case 'tag':
            $relateds = get_posts(array(
                'post_type' => $post_type,
                'orderby'          => $orderby,
                'order'            => $order,
                'category__in' => wp_get_post_categories($post->ID),
                'numberposts' => $post_number,
                'post__not_in' => array($post->ID))
            );
            break;    
        case 'category-service':
            $custom_taxterms = wp_get_object_terms( $post->ID, 'category-service', array('fields' => 'ids') );
            // arguments
            $nbtargs = array(
            'post_type' => 'service',
            'post_status' => 'publish',
            'orderby'          => $orderby,
            'order'            => $order,
            'posts_per_page' => $post_number, // you may edit this number
            'orderby' => 'rand',
            'tax_query' => array(
                array(
                    'taxonomy' => 'category-service',
                    'field' => 'id',
                    'terms' => $custom_taxterms
                )
            ),
            'post__not_in' => array ($post->ID),
            );
            $relateds = new WP_Query( $nbtargs );
            break; 
        
        default:
            $custom_taxterms = wp_get_object_terms( $post->ID, 'category-team', array('fields' => 'ids') );
            // arguments
            $nbtargs = array(
            'post_type' => 'team',
            'post_status' => 'publish',
            'orderby'          => $orderby,
            'order'            => $order,
            'posts_per_page' => $post_number, // you may edit this number
            'orderby' => 'rand',
            'tax_query' => array(
                array(
                    'taxonomy' => 'category-team',
                    'field' => 'id',
                    'terms' => $custom_taxterms
                )
            ),
            'post__not_in' => array ($post->ID),
            );
            $relateds = new WP_Query( $nbtargs );
            break;
    }

    
}
 

$image_file_src = wp_get_attachment_image_src($image_file, '');

$attr = array();
if( !empty($image_file_src) ) {
	$attr = array(
		'src' => $image_file_src[0],
	);
	if(!empty($image_file_src[1])) $attr['width'] = $image_file_src[1];
	if(!empty($image_file_src[2])) $attr['height'] = $image_file_src[2];
}
if( function_exists('wp_get_attachment_image_srcset') && function_exists('wp_get_attachment_image_sizes') && !empty(wp_get_attachment_image_srcset($image_file, $size)) ) {
    $attr['srcset'] = wp_get_attachment_image_srcset($image_file, $thumbsize);
    $attr['sizes'] = wp_get_attachment_image_sizes($image_file, $thumbsize);
}
if( !empty($alt) ) $attr['alt'] = $alt;

?>
<?php if($relateds):?>
<?php if($related_type == 'category'):?>
    <ul class="nbtsow-related-posts clear unstyled">
    <?php
    foreach ( $relateds as $post ) :
        setup_postdata( $post ); ?> 
        <li class="nbtsow-related-post">
            <?php if (has_post_thumbnail(get_the_ID())): ?>
                <div class="nbtsow-related-thumb">
                    <a href="<?php the_permalink(get_the_ID()); ?>">
                        <?php echo get_the_post_thumbnail(get_the_ID(), $thumbsize); ?>
                    </a>
                </div>
            <?php elseif ($image_file_show):?>
                    <div class="nbtsow-related-thumb">
                       <?php if($attr['src']):?>
                        <div class="image-thumbnail-post" data-img="<?php echo $attr['src'];?>" data-wh="<?php echo $thumbsize;?>"></div>
                        <?php endif;?>
                    </div>    
            <?php endif; ?>

            <div class="nbtsow-related-details">
                <h4 class="nbtsow-related-title">
                    <a href="<?php the_permalink(get_the_ID()); ?>">
                        <?php the_title(); ?>
                    </a>
                </h4>
                <?php if ($contentshow): ?>
                    <div class="nbtsow-related-content">
                    <?php if(get_the_excerpt()):
                        the_excerpt() ;
                    else:
                        echo esc_html__(wp_trim_words(get_the_content(), $contentcharlimit, '...'));
                    endif;
                    ?>
                    </div>
                <?php endif; ?>
            </div>
            <?php if ($dateshow): ?>
                    <div class="nbtsow-related-date">
                        <span><?php echo get_the_date(get_option('date_format'), get_the_ID()); ?></span>
                    </div>
            <?php endif; ?>
        </li>

<?php endforeach; ?>
        <?php wp_reset_postdata(); ?>
</ul>
<?php else: ?>
<ul class="nbtsow-related-posts clear unstyled">
    <?php
    if ( $relateds->have_posts() ) :
    while ( $relateds->have_posts() ): $relateds->the_post();
        ?>
        <li class="nbtsow-related-post">
            <?php if (has_post_thumbnail(get_the_ID())): ?>
                <div class="nbtsow-related-thumb">
                    <a href="<?php the_permalink(get_the_ID()); ?>">
                        <?php echo get_the_post_thumbnail(get_the_ID(), $thumbsize); ?>
                    </a>
                </div>
            <?php elseif ($image_file_show):?>
                    <div class="nbtsow-related-thumb">
                       <?php if($attr['src']):?>
                        <div class="image-thumbnail-post" data-img="<?php echo $attr['src'];?>" data-wh="<?php echo $thumbsize;?>"></div>
                        <?php endif;?>
                    </div>    
            <?php endif; ?>

            <div class="nbtsow-related-details">
                <h4 class="nbtsow-related-title">
                    <a href="<?php the_permalink(get_the_ID()); ?>">
                        <?php the_title(); ?>
                    </a>
                </h4>
                <div class="nbtsow-related-content">
                <?php if(get_the_excerpt()):
                    the_excerpt() ;
                else:
                    echo esc_html__(wp_trim_words(get_the_content(), $contentcharlimit, '...'));
                endif;
                ?>
                </div>
            </div>
            <?php if ($dateshow): ?>
                    <div class="nbtsow-related-date">
                        <span><?php echo get_the_date(get_option('date_format'), get_the_ID()); ?></span>
                    </div>
            <?php endif; ?>
        </li>

<?php endwhile; endif; ?>
        <?php wp_reset_postdata(); ?>
</ul>
<?php endif;endif;?>