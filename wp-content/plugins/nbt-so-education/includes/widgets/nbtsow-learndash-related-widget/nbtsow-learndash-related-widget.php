<?php

/*
  Widget Name: NetBaseTeam Learndash Related Widget
  Description: NetBaseTeam Learndash Related Coures Widget.
  Author: NetBaseTeam
  Author URI: https://netbaseteam.com
 */
defined('ABSPATH') or die('No script kiddies please!');

class NBTSOW_Learndash_Related_Widget extends SiteOrigin_Widget {

    function __construct() {

        parent::__construct(
                'nbtsow-learndash-related-widget', esc_html__('NetBaseTeam Learndash Related Posts Widget', 'nbtsow'), array(
            'description' => 'Display Related Posts Widget'
                ), array(), array(
            'title' => array(
                'type' => 'text',
                'label' => esc_html__('Title of Widget ', 'nbtsow'),
                'default' => '',
            ),
            'description' => array(
                'type' => 'text',
                'label' => esc_html__('Enter description widget:  ', 'nbtsow'),
                'default' => '',
            ),
            'related_type' => array(
                'type' => 'select',
                'label' => esc_html__('Select related type', 'nbtsow'),
                'options' => array(
                    '' => esc_html__('Select a choice...', 'nbtsow'),
                    'category' => esc_html__('Categoy', 'nbtsow'),
                    'tag' => esc_html__('Tag', 'nbtsow'),
                    'category-service' => esc_html__('Service', 'nbtsow'),
                    'category-team' => esc_html__('Team', 'nbtsow'),
                )
            ),
            'post_type' => array(
                'type' => 'select',
                'label' => esc_html__('Post type'),
                'options' => array(
                    '' => esc_html__('All', 'nbtsow'),
                    'post' => esc_html__('Post', 'nbtsow'),
                    'sfwd-courses' => esc_html__('Courses', 'nbtsow')
                )
            ),
            'post_number' => array(
                'type' => 'text',
                'label' => esc_html__('Number of post to show'),
                'default' => 5,
            ),
            
            'dateshow' => array(
                'type' => 'checkbox',
                'label' => esc_html__('Date publish of post to show'),
                'default' => false
            ),
            'contentshow' => array(
                'type' => 'checkbox',
                'label' => esc_html__('Content publish of post to show'),
                'default' => false
            ),
            'contentcharlimit' => array(
                'type' => 'text',
                'label' => esc_html__('Content character limit of post'),
                'default' => 25
            ),
            'order' => array(
                'type' => 'select',
                'label' => esc_html__('Order'),
                'options' => array(
                    '' => esc_html__('Select a choice...', 'nbtsow'),
                    'ASC' => esc_html__('Ascending', 'nbtsow'),
                    'DESC' => esc_html__('Descending', 'nbtsow')
                )
            ),
            'orderby' => array(
                'type' => 'select',
                'label' => esc_html__('Order by'),
                'options' => array(
                    'none' => esc_html__('No order', 'nbtsow'),
                    'ID' => esc_html__('Post ID', 'nbtsow'),
                    'author' => esc_html__('Author', 'nbtsow'),
                    'title' => esc_html__('Title', 'nbtsow'),
                    'date' => esc_html__('Published date', 'nbtsow'),
                    'modified' => esc_html__('Modified date', 'nbtsow'),
                    'parent' => esc_html__('By parent', 'nbtsow'),
                    'rand' => esc_html__('Random order', 'nbtsow'),
                    'comment_count' => __('Comment count', 'nbtsow'),
                    'menu_order' => esc_html__('Menu order', 'nbtsow'),
                    'meta_value' => esc_html__('By meta value', 'nbtsow'),
                    'meta_value_num' => esc_html__('By numeric meta value', 'nbtsow'),
                    'post__in' => esc_html__('By include order', 'nbtsow'),
                )
            ),
            'image_file' => array(
                'type' => 'media',
                'label' => esc_html__('Image file :Display image file if article no thumbnail image ', 'nbtsow'),
                'library' => 'image',
                'fallback' => true,
            ),
            'image_file_show' => array(
                'type' => 'checkbox',
                'label' => esc_html__('Show image file'),
                'default' => true
            ),
            'thumbsize' => array(
                'type' => 'image-size',
                'label' => esc_html__('Thumbnail size (Width x Height)'),
            ),
            'theme' => array(
                    'type' => 'radio',
                    'label' => esc_html__('Theme Type', 'nbtsow'),
                    'default' => 'default',
                    'options' => array(
                        'default' => esc_html__('Default template', 'nbtsow'),
                        'slide' => esc_html__('Slide template', 'nbtsow')
                    )
                ),
            )
        );
    }

    function get_template_variables($instance, $args) {
        return array(
            'title' => !empty($instance['title']) ? $instance['title'] : '',
            'description' => $instance['description'],
            'post_number' => $instance['post_number'],
            'related_type' => $instance['related_type'],
            'post_type' => $instance['post_type'],
            'dateshow' => $instance['dateshow'],
            'order' => $instance['order'],
            'orderby' => $instance['orderby'],
            'thumbsize' => $instance['thumbsize'],
            'contentcharlimit' => $instance['contentcharlimit'],
            'contentshow' => $instance['contentshow'],
            'image_file' => $instance['image_file'],
            'image_file_show' => $instance['image_file_show'],
            'theme' => $instance['theme'],
            
        );
    }

    function get_template_name($instance) {
        $template = '';
        if( $instance['theme'] == 'default' ) {
            $template = 'default';
        } else {
            $template = 'slide';
        }
        return $template;
    }

    function get_style_name($instance) {
        return '';
    }

}

siteorigin_widget_register('nbtsow-learndash-related-widget', __FILE__, 'NBTSOW_Learndash_Related_Widget');
