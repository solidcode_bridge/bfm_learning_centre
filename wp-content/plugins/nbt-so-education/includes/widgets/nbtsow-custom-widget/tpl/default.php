<?php foreach ($order as $item):
	switch ($item):
		case 'title':
			if (!empty($title)):?>
				<<?php echo ($tag ? $tag : 'h3'); ?> class="nbtsow-custom-title">
					<?php echo $title; ?>
				</<?php echo ($tag ? $tag : 'h3'); ?>>
			<?php endif;
			break;
		case 'icon':
			if (!empty($icon) && (!empty($icon['icon']) || !empty($icon['icon_media']))): ?>
				<div class="nbtsow-custom-image">
					<?php if( !empty($icon['icon_media']) ):
						$attachment = wp_get_attachment_image_src($icon['icon_media']);
						if(!empty($attachment)):
							$icon_styles[] = 'background-image: url(' . sow_esc_url($attachment[0]) . ')';
							$icon_styles[] = 'width: ' . intval($icon['icon_size']).'px';
							$icon_styles[] = 'height: ' . intval($icon['icon_size']).'px';
							?><span class="nbtsow-icon-image" style="<?php echo implode('; ', $icon_styles) ?>"></span><?php
						endif;
					elseif(!empty($icon['icon'])):
						$icon_styles = array();
						if(!empty($icon['icon_size'])) 
							$icon_styles[] = 'font-size: ' . intval($icon['icon_size']).'px';
						if(!empty($icon['icon_color'])) 
							$icon_styles[] = 'color: ' . $icon['icon_color'];
						echo siteorigin_widget_get_icon($icon['icon'], $icon_styles);
					endif; ?>
				</div>
			<?php endif;
			break;
		case 'desc':
			if (!empty($description)):?>
				<div class="nbtsow-custom-desc">
					<?php echo $description; ?>
				</div>
			<?php endif;
			break;
		case 'btn':
			if (!empty($button) && (!empty($button['text']) || (!empty($button['icon']) && (!empty($button['icon']['icon']) || !empty($button['icon']['icon_media']))))):?>
				<div class="nbtsow-custom-btn">
					<a class=""<?php echo ($button['href'] ? (' href="'.sow_esc_url($button['href']).'"') : ''); ?>>
						<?php foreach ($button['order'] as $btn):
							switch ($btn):
								case 'icon':
									if( !empty($button['icon']['icon_media']) ):
										$attachment = wp_get_attachment_image_src($button['icon']['icon_media']);
										if(!empty($attachment)):
											$btn_i_styles[] = 'background-image: url(' . sow_esc_url($attachment[0]) . ')';
											$btn_i_styles[] = 'width: ' . intval($button['icon']['icon_size']).'px';
											$btn_i_styles[] = 'height: ' . intval($button['icon']['icon_size']).'px';
											?><span class="nbtsow-icon-image" style="<?php echo implode('; ', $btn_i_styles) ?>"></span><?php
										endif;
									elseif(!empty($button['icon']['icon'])):
										$btn_i_styles = array();
										if(!empty($icon['icon_size'])) 
											$btn_i_styles[] = 'font-size: ' . intval($icon['icon_size']).'px';
										if(!empty($icon['icon_color'])) 
											$btn_i_styles[] = 'color: ' . $icon['icon_color'];
										echo siteorigin_widget_get_icon($button['icon']['icon'], $btn_i_styles);
									endif;
									break;
								case 'text':
									echo $button['text'];
									break;
							endswitch;
						endforeach; ?>
					</a>
				</div>
			<?php endif;
			break;
	endswitch;
endforeach; ?>