<?php

/*
Widget Name: NetBaseTeam Custom HTML Widget
Description: A rich-text, text editor, button
Author: NetBaseTeam
Author URI: http://netbaseteam.com
*/

class NBTSOW_Custom_Widget extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'nbtsow-custom-widget',
			esc_html__('NetBaseTeam Custom HTML Widget', 'nbtsow'),
			array(
				'description' => esc_html__('A rich-text, text editor, button', 'nbtsow'),
			), array(
			), false, plugin_dir_path(__FILE__) . '../'
		);
	}
	
	function initialize() {
        $this->register_frontend_styles(
			array(
				array(
					'nbtsow-custom',
					plugin_dir_url(__FILE__) . 'css/style.css',
					array(),
					''
				)
			)
        );
    }

    function initialize_form() {
        return array(
			'title' => array(
				'type' => 'text',
				'label' => __('Title', 'nbtsow'),
			),
			'tag' => array(
				'type' => 'select',
				'label' => esc_html__( 'HTML Tag', 'nbtsow' ),
				'default' => 'h3',
				'options' => array(
					'h1' => esc_html__( 'H1', 'nbtsow' ),
					'h2' => esc_html__( 'H2', 'nbtsow' ),
					'h3' => esc_html__( 'H3', 'nbtsow' ),
					'h4' => esc_html__( 'H4', 'nbtsow' ),
					'h5' => esc_html__( 'H5', 'nbtsow' ),
					'h6' => esc_html__( 'H6', 'nbtsow' ),
					'p' => esc_html__( 'Paragraph', 'nbtsow' ),
				)
			),
			'icon' => array(
				'type' => 'section',
				'label' => esc_html__( 'Select Icon or Image', 'nbtsow' ),
				'hide' => true,
				'fields' => array(
					'icon' => array(
						'type' => 'icon',
						'label' => __('Icon.', 'nbtsow')
					),
					'icon_color' => array(
						'type' => 'color',
						'label' => __('Icon Color', 'nbtsow')
					),
					'icon_size' => array(
						'type' => 'slider',
						'label' => __('Icon Size', 'nbtsow'),
						'min' => 1,
						'max' => 64,
						'integer' => true,
						'default' => 24
					),
					'icon_media' => array(
						'type' => 'media',
						'label' => __( 'Image icon', 'nbtsow' ),
						'description' => __('Replaces the icon with your own image icon.', 'so-widgets-bundle'),
					)
				),
			),
			'description' => array(
				'type' => 'tinymce',
				'label' => __( 'Description', 'nbtsow' ),
				'rows' => 10,
				'default_editor' => 'html',
				'button_filters' => array(
					'mce_buttons' => array( $this, 'filter_mce_buttons' ),
					'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
					'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
					'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
					'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
				),
			),
			'button' => array(
				'type' => 'section',
				'label' => esc_html__( 'Button Option', 'nbtsow' ),
				'hide' => false,
				'fields' => array(
					'text' => array(
						'type' => 'text',
						'label' => esc_html__('Button Text', 'nbtsow'),
					),
					'href' => array(
						'type' => 'link',
						'label' => esc_html__('Button Link', 'nbtsow'),
					),
					'icon' => array(
						'type' => 'section',
						'label' => esc_html__( 'Select icon', 'nbtsow' ),
						'hide' => true,
						'fields' => array(
							'icon' => array(
								'type' => 'icon',
								'label' => __('Icon.', 'nbtsow')
							),
							'icon_color' => array(
								'type' => 'color',
								'label' => __('Icon Color', 'nbtsow')
							),
							'icon_size' => array(
								'type' => 'slider',
								'label' => __('Icon Size', 'nbtsow'),
								'min' => 1,
								'max' => 64,
								'integer' => true,
								'default' => 24
							),
							'icon_media' => array(
								'type' => 'media',
								'label' => __( 'Image icon', 'nbtsow' ),
								'description' => __('Replaces the icon with your own image icon.', 'so-widgets-bundle'),
							)
						),
					),
					'order' => array(
						'type' => 'order',
						'label' => esc_html__( 'Element Order', 'nbtsow' ),
						'options' => array(
							'icon' => esc_html__( 'Icon', 'nbtsow' ),
							'text' => esc_html__( 'Text', 'nbtsow' ),
						),
						'default' => array('icon','text'),
					)
				),
			),
			'order' => array(
				'type' => 'order',
				'label' => esc_html__( 'Element Order', 'nbtsow' ),
				'options' => array(
					'title' => esc_html__( 'Title', 'nbtsow' ),
					'icon' => esc_html__( 'Icon / Image', 'nbtsow' ),
					'desc' => esc_html__( 'Description', 'nbtsow' ),
					'btn' => esc_html__( 'Button', 'nbtsow' ),
				),
				'default' => array('icon','title','desc','btn'),
			)
		);
    }

	function get_template_variables($instance, $args) {
		return array(
			'title' => $instance['title'] ? $instance['title'] : '',
			'tag' => $instance['tag'] ? $instance['tag'] : 'h3',
			'description' => $instance['description'] ? $instance['description'] : '',
			'icon' => !empty($instance['icon']) ? $instance['icon'] : array(),
			'button' => !empty($instance['button']) ? $instance['button'] : array(),
			'order' => $instance['order']?$instance['order']:array( 'icon','title','desc','btn' ),
		);
	}

	function get_template_name($instance) {
		return 'default';
	}
}

siteorigin_widget_register('nbtsow-custom-widget', __FILE__, 'NBTSOW_Custom_Widget');
