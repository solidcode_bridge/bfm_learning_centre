<?php

/*
  Widget Name: NetBaseTeam Learndash Latest Post
  Description: NetBaseTeam Learndash Latest Post
  Author: NetBaseTeam
  Author URI: https://netbaseteam.com
 */

class NBTSOW_Learndash_Posts_Widget extends SiteOrigin_Widget {

    function __construct() {
        parent::__construct(
            'nbtsow-learndash-posts-widget', esc_html__('NetBaseTeam Learndash Latest Posts Widget', 'nbtsow'), array(
				'description' => 'Display Latest Posts Widget'
			), array(), array(
				'title' => array(
					'type' => 'text',
					'label' => esc_html__('Title of Widget ', 'nbtsow'),
					'default' => 'Latest courses',
				),
				'title_url' => array(
					'type' => 'text',
					'label' => esc_html__('Enter title url Widget:  ', 'nbtsow'),
					'default' => '',
				),
				'post_type' => array(
					'type' => 'select',
					'label' => esc_html__('Post type'),
					'options' => array(
						'' => esc_html__('Select a choice...', 'nbtsow'),
						'post' => esc_html__('Post', 'nbtsow'),
						'sfwd-courses' => esc_html__('Courses', 'nbtsow')
					)
				),
				'category' => array(
					'type' => 'select',
					'label' => esc_html__('Select category'),
					'multiple' => true,
					'options' => $this->getCategories()
				),
				'post_number' => array(
					'type' => 'text',
					'label' => esc_html__('Number of post to show'),
					'default' => 5,
				),
				'titleshow' => array(
					'type' => 'checkbox',
					'label' => esc_html__('Show/hide title post'),
					'default' => true
				),
				'reviews' => array(
					'type' => 'checkbox',
					'label' => esc_html__('Reviews of post to show'),
					'default' => true
				),
				'dateshow' => array(
					'type' => 'checkbox',
					'label' => esc_html__('Date publish of post to show'),
					'default' => false
				),
				'author' => array(
					'type' => 'checkbox',
					'label' => esc_html__('Author post to show'),
					'default' => false
				),
				'comments' => array(
					'type' => 'checkbox',
					'label' => esc_html__('Comments of post to show'),
					'default' => false
				),
				'contentshow' => array(
					'type' => 'checkbox',
					'label' => esc_html__('Content publish of post to show'),
					'default' => false
				),
				'contentcharlimit' => array(
					'type' => 'text',
					'label' => esc_html__('Content character limit of post'),
					'default' => ''
				),
				'readmore' => array(
					'type' => 'text',
					'label' => esc_html__('Readmore text'),
					'default' => ''
				),
				'order' => array(
					'type' => 'select',
					'label' => esc_html__('Order'),
					'options' => array(
						'' => esc_html__('Select a choice...', 'nbtsow'),
						'ASC' => esc_html__('Ascending', 'nbtsow'),
						'DESC' => esc_html__('Descending', 'nbtsow')
					)
				),
				'orderby' => array(
					'type' => 'select',
					'label' => esc_html__('Order by'),
					'options' => array(
						'none' => esc_html__('No order', 'nbtsow'),
						'ID' => esc_html__('Post ID', 'nbtsow'),
						'author' => esc_html__('Author', 'nbtsow'),
						'title' => esc_html__('Title', 'nbtsow'),
						'date' => esc_html__('Published date', 'nbtsow'),
						'modified' => esc_html__('Modified date', 'nbtsow'),
						'parent' => esc_html__('By parent', 'nbtsow'),
						'rand' => esc_html__('Random order', 'nbtsow'),
						'comment_count' => __('Comment count', 'nbtsow'),
						'menu_order' => esc_html__('Menu order', 'nbtsow'),
						'meta_value' => esc_html__('By meta value', 'nbtsow'),
						'meta_value_num' => esc_html__('By numeric meta value', 'nbtsow'),
						'post__in' => esc_html__('By include order', 'nbtsow'),
					)
				),
				'thumbshow' => array(
					'type' => 'checkbox',
					'label' => esc_html__('Thumbnail publish of post to show'),
					'default' => false
				),
				'thumbsize' => array(
					'type' => 'image-size',
					'label' => esc_html__('Thumbnail size (Width x Height)'),
				),
				'position' => array(
					'type' => 'select',
					'label' => __('Position Information', 'nbtsow'),
					'options' => array(
						'0' => __('Before Title', 'nbtsow'),
						'1' => __('After Title', 'nbtsow'),
						'2' => __('After Description', 'nbtsow'),
						'3' => __('Split', 'nbtsow'),
					),
					'default' => '1'
				),
				'theme' => array(
					'type' => 'select',
					'label' => __('Type theme', 'nbtsow'),
					'options' => array(
						'default' => __('Default', 'nbtsow'),
						'default1' => __('Default 1', 'nbtsow'),
						'default2' => __('Default 2', 'nbtsow'),
					),
				),
			)
        );
    }

    function get_template_variables($instance, $args) {
        return array(
            'title' => !empty($instance['title']) ? $instance['title'] : '',
            'title_url' => !empty($instance['title_url']) ? $instance['title_url'] : '',
            'post_number' => $instance['post_number'],
            'reviews' => $instance['reviews'],
            'dateshow' => $instance['dateshow'],
            'order' => $instance['order'],
            'orderby' => $instance['orderby'],
            'thumbsize' => !empty($instance['thumbsize']) ? $instance['thumbsize'] : '50*50',
            'post_type' => $instance['post_type'],
            'contentcharlimit' => $instance['contentcharlimit'],
            'contentshow' => $instance['contentshow'],
            'category' => $instance['category'],
            'thumbshow' => $instance['thumbshow'],
            'author' => $instance['author'],
            'comments' => $instance['comments'],
            'readmore' => $instance['readmore'],
            'titleshow' => $instance['titleshow'],
            'position' => $instance['position'],
            'theme' => $instance['theme'],
        );
    }

    function get_template_name($instance) {
        return $instance['theme'] ? $instance['theme'] : 'default';
    }

    function get_style_name($instance) {
        return false;
    }

    function getCategories() {
        $term_names = get_terms('category', 'orderby=name&fields=id=>name&hide_empty=0');
        $term_slugs = get_terms('category', 'orderby=name&fields=id=>slug&hide_empty=0');
        $categories = array();
        foreach ($term_slugs as $key => $term_slug) {
            $categories[$term_slug] = $term_names[$key];
        }
        return $categories;
    }

    function get_dataposts($instance) {

        $categories = array();
        if (count($instance['category']) > 1) {
            foreach ($instance['category'] as $category) {
                $categories[] = get_term_by('slug', $category, 'category')->term_id;
            }
            $category = implode(',', $categories);
        } else {
            $category = get_term_by('slug', $instance['category'], 'category')->term_id;
        }

        $args_courses = array(
            'numberposts' => $instance['post_number'],
            'offset' => 0,
            'category' => $category,
            'orderby' => $instance['orderby'],
            'order' => $instance['order'],
            'post_type' => $instance['post_type'],
            'theme' => $instance['theme'],
            'post_status' => 'publish',
            'suppress_filters' => true
        );

        $dataposts = wp_get_recent_posts($args_courses);

        if ($dataposts) {
            foreach ($dataposts as $key => $value) {
				// $dataposts[$key]['cats'] = get_the_category($value['ID']);
				$cats = get_the_category($value['ID']);
				$terms = array();
				foreach ($cats as $cat) {
					$terms[] = '<a class="nbtsow-cat" href="' . get_category_link($cat->term_id) . '">' . $cat->name . '</a>';
				}
				// $dataposts[$key]['cats'] = implode(', ', $terms);
				$dataposts[$key]['cats'] = $terms[array_rand($terms, 1)];
                $aggregate = $this->get_aggregate_reviews($value['ID']);
                $dataposts[$key]['avg'] = $aggregate->aggregate_rating;
            }

            return $dataposts;
        }

        return '';
    }

    function get_aggregate_reviews($postid) {

        global $wpdb;

        $query = $wpdb->prepare("
                            SELECT 
                            COUNT(*) AS aggregate_count, AVG(tmp1.rating) AS aggregate_rating
                            FROM 
                (
                    SELECT pm4.meta_value AS rating
                    FROM {$wpdb->prefix}posts p1
                    INNER JOIN {$wpdb->prefix}postmeta pm1 ON pm1.meta_key = 'wpcr3_enable' AND pm1.meta_value = '1' AND pm1.post_id = p1.id
                    INNER JOIN {$wpdb->prefix}postmeta pm2 ON pm2.meta_key = 'wpcr3_review_post' AND pm2.meta_value = p1.id 
                    INNER JOIN {$wpdb->prefix}posts p2 ON p2.id = pm2.post_id AND p2.post_status = 'publish' AND p2.post_type = 'wpcr3_review'
                    INNER JOIN {$wpdb->prefix}postmeta pm4 ON pm4.post_id = p2.id AND pm4.meta_key = 'wpcr3_review_rating' AND pm4.meta_value IS NOT NULL AND pm4.meta_value != '0'
                    WHERE
                    p1.id = %d
                    GROUP BY p2.id
                ) tmp1
                    ", intval($postid));

        $results = $wpdb->get_results($query);

        $rtn = new stdClass();

        if (count($results)) {

            $rtn->aggregate_count = $results[0]->aggregate_count;
            $rtn->aggregate_rating = ($results[0]->aggregate_rating) / 5 * 100;
            if ($rtn->aggregate_count == 0) {
                $rtn->aggregate_rating = 0;
            }
        }

        return $rtn;
    }

}

siteorigin_widget_register('nbtsow-learndash-posts-widget', __FILE__, 'NBTSOW_Learndash_Posts_Widget');
