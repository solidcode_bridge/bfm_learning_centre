<?php
$dataposts = $this->get_dataposts($instance);//var_dump($dataposts[0]);
$contentcharlimit = !empty($contentcharlimit) ? $contentcharlimit : '15';
?>
<?php if(!empty($title)) :
    echo '<h3 class="nbtsow-widget-title">' . $title . '</h3>';
endif;?>
<ul class="nbtsow-blog-posts clear nbtsow-layout-style2 unstyled">
<?php
    foreach ($dataposts as $key => $value):

?>
		<li class="nbtsow-post nbtsow-<?php echo ($post_type ? $post_type : ''); ?>-post">
            <div class="nbtsow-post-round">
				<?php if (has_post_thumbnail( $value['ID'] ) && $thumbshow ):?>
					<div class="nbtsow-layout-style-thumb">
						<a href="<?php echo get_permalink($value['ID']);?>">
							<?php echo get_the_post_thumbnail($value['ID'],$thumbsize );?>
						</a>
					</div>
				<?php endif;?>
				<div class="nbtsow-post-detail">
					<?php if ($dateshow || $comments) : ?>
						<div class="nbtsow-info">
							<?php if ($dateshow) : ?>
								<div class="nbtsow-date">
									<div class="nbtsow-day"><?php echo esc_html__(get_the_date('d',$value['ID'] )); ?></div>
									<div class="nbtsow-month"><?php echo esc_html__(get_the_date('M',$value['ID'] )); ?></div>
								</div>
							<?php endif; ?>
							<?php if ($comments) : ?>
								<div class="nbtsow-coments">
									<i class="nbticon-big-speech-balloon"></i>
									<span><?php echo get_comments_number(); ?></span>
								</div>
							<?php endif; ?>
						</div>
					<?php endif; ?>
					<div class="nbtsow-layout-style-details">
						<?php echo get_avatar( get_the_author_meta( 'ID', $value['post_author'] ), 60 ); ?>
						<?php if($position=='0' && ($author || $reviews)):?>
							<div class="nbtsow-info-top">
								<?php echo $author ? '<span class="nbtsow-auth">' . esc_html__('Post by ') . '<span>' . get_the_author_meta('user_nicename', $value['post_author']) . '</span></span>' : ''; ?>
								<?php echo ($value['cats']); ?>
								<?php if($reviews):?>   
									<div class="wpcr3_rating_style1_base ">
										<div class="wpcr3_rating_style1_average" style="width:<?php echo $value['avg'] ? $value['avg'] : '0';?>%;"></div>
									</div>  
								<?php endif;?>
							</div>
						<?php endif;?>
						<?php if ($titleshow): ?>
							<h4 class="nbtsow-layout-style-title">
								<a href="<?php echo get_permalink($value['ID']);?>">
									<?php echo esc_html__($value['post_title']); ?>
								</a>
							</h4>
						<?php endif; ?>
						<?php if(($position=='1' && ($author || $reviews)) || ($position=='3' && ($author))):?>
							<div class="nbtsow-info-middle">
								<?php echo $author ? '<span class="nbtsow-auth">' . esc_html__('Post by ') . '<span>' . get_the_author_meta('user_nicename', $value['post_author']) . '</span></span>' : ''; ?>
								<?php echo ($value['cats']); ?>
								<?php if($reviews && $position=='1'):?>   
									<div class="wpcr3_rating_style1_average" style="width:<?php echo $value['avg'] ? $value['avg'] : '0';?>%;"></div>
								<?php endif;?>
							</div>
						<?php endif;?>
						<?php if($contentshow): ?>
							<div class="nbtsow-layout-style-content">
								<?php echo esc_html__(wp_trim_words( $value['post_content'], $contentcharlimit, '...' )); ?>
							</div>
						<?php endif;?>
						<?php if(($position=='2' && ($author || $reviews)) || ($position=='3' && $reviews)):?>
							<div class="nbtsow-info-bottom">
								<?php if($reviews):?>   
									<div class="wpcr3_rating_style1_average" style="width:<?php echo $value['avg'] ? $value['avg'] : '0';?>%;"></div>
								<?php endif;
								if ($position=='2'):
									echo $author ? '<span class="nbtsow-auth">' . esc_html__('Post by ') . '<span>' . get_the_author_meta('user_nicename', $value['post_author']) . '</span></span>' : ''; ?>
									<?php echo ($value['cats']); ?>
									<?php 
								endif; ?>
							</div>
						<?php endif;?>
						<?php if($readmore): ?>
							<div class="readmore">
								<a href="<?php echo get_permalink($value['ID']);?>"><?php echo esc_html__($readmore); ?></a>
							</div>
						<?php endif; ?>
					</div>
				</div>
            </div>
        </li>
    <?php endforeach; ?>
</ul>
<?php wp_reset_postdata();?>