<?php
$newdataposts = $this->get_dataposts($instance);
$contentcharlimit = !empty($contentcharlimit) ? $contentcharlimit : '15';
?>
<?php
if (!empty($title)) :
    echo '<h3 class="nbtsow-widget-title">' . $title . '</h3>';
endif;
?>
<?php if ($newdataposts != ''): ?>
    <ul class="nbtsow-blog-posts clear nbtsow-layout-default unstyled">
        <?php foreach ($newdataposts as $key => $value): ?>
            <li class="nbtsow-course-post nbtsow-<?php echo ($post_type ? $post_type : ''); ?>-post">
                <?php if (has_post_thumbnail($value['ID']) && $thumbshow): ?>
                    <div class="nbtsow-course-thumb">
                        <a href="<?php echo get_permalink($value['ID']); ?>">
                            <?php echo get_the_post_thumbnail($value['ID'], $thumbsize); ?>
                        </a>
                    </div>
                <?php endif; ?>

                <div class="nbtsow-course-details">
					<?php if($position=='0' && ($author || $dateshow || $comments || $reviews)):?>
						<div class="nbtsow-info-top">
							<?php echo $author ? '<span>' . esc_html__('By: ') . get_the_author() . '</span>' : '';
							echo $dateshow ? '<span>' . esc_html__(get_the_date(get_option('date_format'),$value['ID'] )) . '</span>' : '';
							echo $comments ? '<span><i class="nbticon-big-speech-balloon"></i>' . get_comments_number() . ' ' . esc_html__('Comments') . '</span>' : '';
							if($reviews):?>   
								<div class="wpcr3_rating_style1_average" style="width:<?php echo $value['avg'] ? $value['avg'] : '0';?>%;"></div>
							<?php endif;?>
						</div>
					<?php endif;?>
                    <?php if ($titleshow): ?>
                        <h4 class="nbtsow-course-title">
                            <a href="<?php echo get_permalink($value['ID']); ?>">
                                <?php echo esc_html__($value['post_title']); ?>
                            </a>
                        </h4>
                    <?php endif; ?>
					<?php if(($position=='1' && ($author || $dateshow || $comments || $reviews)) || ($position=='3' && ($author || $dateshow || $comments))):?>
						<div class="nbtsow-info-middle">
							<?php echo $author ? '<span><i class="nbticon-black-user-shape"></i>' . esc_html__('By: ') . get_the_author() . '</span>' : '';
							echo $dateshow ? '<span><i class="nbticon-schedule-button"></i>' . esc_html__(get_the_date(get_option('date_format'),$value['ID'] )) . '</span>' : '';
							echo $comments ? '<span><i class="nbticon-big-speech-balloon"></i>' . get_comments_number() . ' ' . esc_html__('Comments') . '</span>' : '';
							if($reviews && $position=='1'):?>   
								<div class="wpcr3_rating_style1_base ">
									<div class="wpcr3_rating_style1_average" style="width:<?php echo $value['avg'] ? $value['avg'] : '0';?>%;"></div>
								</div>  
							<?php endif;?>
						</div>
					<?php endif;?>
					<?php if($contentshow): ?>
						<div class="nbtsow-course-content">
							<?php echo esc_html__(wp_trim_words( $value['post_content'], $contentcharlimit, '...' )); ?>
						</div>
					<?php endif;?>
					<?php if(($position=='2' && ($author || $dateshow || $comments || $reviews)) || ($position=='3' && $reviews)):?>
						<div class="nbtsow-info-bottom">
							<?php if($reviews):?>   
								<div class="wpcr3_rating_style1_base ">
									<div class="wpcr3_rating_style1_average" style="width:<?php echo $value['avg'] ? $value['avg'] : '0';?>%;"></div>
								</div>  
							<?php endif;
							if ($position=='2'):
								echo $author ? '<span>' . esc_html__('By: ') . get_the_author() . '</span>' : '';
								echo $dateshow ? '<span>' . esc_html__(get_the_date(get_option('date_format'),$value['ID'] )) . '</span>' : '';
								echo $comments ? '<span>' . get_comments_number() . ' ' . esc_html__('Comments') . '</span>' : '';
							endif; ?>
						</div>
					<?php endif;?>
					<?php if($readmore): ?>
						<div class="readmore">
							<a href="<?php echo get_permalink($value['ID']);?>"><?php echo esc_html__($readmore); ?></a>
						</div>
					<?php endif; ?>
                </div>
            </li>
        <?php endforeach; ?>
        <?php wp_reset_postdata(); ?>
    </ul>
<?php endif; ?>
