<?php
/*
  Widget Name: NetBaseTeam Event Espresso Widget
  Description: Event Espresso Upcoming Events Widget.
  Author: NetBaseTeam
  Author URI: https://netbaseteam.com
 */

class NBTsow_Eventespresso_Widget extends SiteOrigin_Widget {
    function __construct() {
        parent::__construct(
			'nbtsow-eventespresso-widget', __('NetBaseTeam Event Espresso Widget', 'nbtsow'), array(
            'description' => __('NetBaseTeam Event Espresso Upcoming Events Widget', 'nbtsow'),
            'panels_groups' => array('nbtsow-widgets')
                ), array(
                ), false, plugin_dir_path(__FILE__) . '../'
        );
    }

    function initialize_form() {
        return array(
            'title' => array(
                'type' => 'text',
                'label' => __('Title', 'nbtsow'),
            ),
			'title_style' => array(
				'type' => 'select',
				'label' => esc_html__('HTML Tag of Title', 'nbtsow'),
				'default' => 'h3',
				'options' => array(
					'h1' => esc_html__('H1', 'nbtsow'),
					'h2' => esc_html__('H2', 'nbtsow'),
					'h3' => esc_html__('H3', 'nbtsow'),
					'h4' => esc_html__('H4', 'nbtsow'),
					'h5' => esc_html__('H5', 'nbtsow'),
					'h6' => esc_html__('H6', 'nbtsow'),
				)
			),
            'sub_title' => array(
                'type' => 'textarea',
                'label' => __('Sub Title', 'nbtsow'),
            ),
			'sub_title_style' => array(
				'type' => 'select',
				'label' => esc_html__('HTML Tag of Title', 'nbtsow'),
				'default' => 'h3',
				'options' => array(
					'h1' => esc_html__('H1', 'nbtsow'),
					'h2' => esc_html__('H2', 'nbtsow'),
					'h3' => esc_html__('H3', 'nbtsow'),
					'h4' => esc_html__('H4', 'nbtsow'),
					'h5' => esc_html__('H5', 'nbtsow'),
					'h6' => esc_html__('H6', 'nbtsow'),
					'p' => esc_html__('P', 'nbtsow'),
					'div' => esc_html__('DIV', 'nbtsow'),
				)
			),
			'category' => array(
				'type' => 'select',
				'label' => esc_html__('Event Category', 'nbtsow'),
				'default' => '',
				'options' => $this->get_eventcats()
			),
			'limit' => array(
				'type' => 'number',
				'label' => __( 'Number of Events to Display', 'nbtsow' ),
				'default' => '3'
			),
			'show_expired' => array(
				'type' => 'select',
				'label' => __( 'Show Expired Events', 'nbtsow' ),
				'default' => '0',
				'options' => array(
					'0' => __( 'No', 'nbtsow' ),
					'1' => __( 'Yes', 'nbtsow' )
				)
			),
			'ititle_style' => array(
				'type' => 'select',
				'label' => esc_html__('HTML Tag of Event Title', 'nbtsow'),
				'default' => 'h3',
				'options' => array(
					'h1' => esc_html__('H1', 'nbtsow'),
					'h2' => esc_html__('H2', 'nbtsow'),
					'h3' => esc_html__('H3', 'nbtsow'),
					'h4' => esc_html__('H4', 'nbtsow'),
					'h5' => esc_html__('H5', 'nbtsow'),
					'h6' => esc_html__('H6', 'nbtsow'),
				)
			),
			'image_size' => array(
				'type' => 'image-size',
				'label' => esc_html__('Image Size', 'nbtsow')
			),
			'show_desc' => array(
				'type' => 'select',
				'label' => __( 'Show Description', 'nbtsow' ),
				'default' => '0',
				'options' => array(
					'0' => __( 'No', 'nbtsow' ),
					'1' => __( 'Yes', 'nbtsow' )
				)
			),
			'show_date' => array(
				'type' => 'select',
				'label' => __( 'Show Dates', 'nbtsow' ),
				'default' => '0',
				'options' => array(
					'0' => __( 'No', 'nbtsow' ),
					'1' => __( 'Yes', 'nbtsow' )
				)
			),
			'show_allpage' => array(
				'type' => 'select',
				'label' => __( 'Show on all Pages', 'nbtsow' ),
				'default' => '0',
				'options' => array(
					'0' => __( 'No', 'nbtsow' ),
					'1' => __( 'Yes', 'nbtsow' )
				)
			),
			'date_range' => array(
				'type' => 'select',
				'label' => __( 'Show Date Range', 'nbtsow' ),
				'default' => '0',
				'options' => array(
					'0' => __( 'No', 'nbtsow' ),
					'1' => __( 'Yes', 'nbtsow' )
				)
			),
			'order' => array(
				'type' => 'order',
				'label' => esc_html__( 'Element Order', 'nbtsow' ),
				'options' => array(
					'i_title' => esc_html__( 'Title', 'nbtsow' ),
					'i_img' => esc_html__( 'Image', 'nbtsow' ),
					'i_date' => esc_html__( 'Date Time', 'nbtsow' ),
					'i_desc' => esc_html__( 'Short Description', 'nbtsow' ),
				),
				'default' => array( 'i_title', 'i_img', 'i_date', 'i_desc' ),
			),
			'theme' => array(
				'type' => 'select',
				'label' => __('Type theme', 'nbtsow'),
				'options' => array(
					'default' => __('Default', 'nbtsow')
				),
			),
        );
    }
	
	function get_eventcats(){
		$event_categories = array();
		$event_categories[''] = __( 'Display All', 'nbtsow' );
		/** @type EEM_Term $EEM_Term */
		$EEM_Term = EE_Registry::instance()->load_model( 'Term' );
		$categories = $EEM_Term->get_all_ee_categories( TRUE );
		if ( $categories ) {
			foreach ( $categories as $category ) {
				if ( $category instanceof EE_Term ) {
					$event_categories[$category->get( 'slug' )] = $category->get( 'name' );
				}
			}
		}
		return $event_categories;
	}
    
    public function get_template_variables( $instance, $args ) {
        return array(
			'title' => $instance['title'],
			'title_style' => $instance['title_style'],
			'sub_title' => $instance['sub_title'],
			'sub_title_style' => $instance['sub_title_style'],
			'category' => $instance['category'],
			'limit' => $instance['limit'],
			'show_expired' => $instance['show_expired'],
			'ititle_style' => $instance['ititle_style'],
			'image_size' => $instance['image_size'],
			'show_desc' => $instance['show_desc'],
			'show_date' => $instance['show_date'],
			'show_allpage' => $instance['show_allpage'],
			'date_range' => $instance['date_range'],
			'order' => $instance['order']?$instance['order']:array( 'i_title', 'i_img', 'i_date', 'i_desc' ),
			'theme' => $instance['theme'],
        );
    }
    function get_template_name($instance) {
        return $instance['theme'] ? $instance['theme'] : 'default';
    }
}

siteorigin_widget_register('nbtsow-eventespresso-widget', __FILE__, 'NBTsow_Eventespresso_Widget');
