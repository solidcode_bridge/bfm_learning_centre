<?php
$autoplayTimeout = '';
if($autoplay == 'true') $autoplayTimeout = 'autoplayTimeout: '.$timeplay;
$teams = $instance['teams'];
?>
<?php if ($title): ?>
<?php echo $args['before_title'] . esc_html($title) . $args['after_title']; ?>
<?php endif; ?>
<?php if($description): ?>
<span><?php echo esc_html($description); ?></span>
<?php endif; ?>
        <?php if ($teams): ?>
<div class="nbtloop<?php echo $instance['_sow_form_id']; ?> owl-carousel">
     <?php foreach ($teams as $team):
            $url = $team['url'];
            $new_window = $team['new_window'];
            $src = wp_get_attachment_image_src($team['image'], $image_size, false);
            $attr = array();
            if (!empty($src)) {
                $attr = array(
                    'src' => $src[0],
                );
                if (!empty($src[1]))
                    $attr['width'] = $src[1];
                if (!empty($src[2]))
                    $attr['height'] = $src[2];
            }

            // Backward compability with WordPress before 4.4
            if (function_exists('wp_get_attachment_image_srcset') && function_exists('wp_get_attachment_image_sizes') && !empty(wp_get_attachment_image_srcset($team['image'], $image_size))) {
                $attr['srcset'] = wp_get_attachment_image_srcset($team['image'], $image_size);
                $attr['sizes'] = wp_get_attachment_image_sizes($team['image'], $image_size);
            }
            if (!empty($alt)){
                $attr['alt'] = $alt;
			} else {
				$attr['alt'] = '';
			}
        ?>
        <div class="item">
			<?php if(!empty($src)): ?>
				<div class="nbtsow-team-image">
					<?php if (!empty($url)) : ?><a href="<?php echo sow_esc_url($url) ?>" <?php if ($new_window) echo 'target="_blank"' ?>><?php endif; ?>
							<img class="img-responsive" <?php foreach ($attr as $n => $v)
						echo $n . '="' . esc_attr($v) . '" ' ?> />
					<?php if (!empty($url)) : ?></a><?php endif; ?>
				</div>
			<?php endif ?>
			<?php if($team['name'] || $team['instructive']): ?>
				<div class="nbtsow-team-data">
					<?php if($team['name']): ?>
						<div class="team-name">
							<?php if (!empty($url)) : ?><a href="<?php echo sow_esc_url($url) ?>" <?php if ($new_window) echo 'target="_blank"' ?>><?php endif; ?>
							<?php echo $team['name']; ?>
							<?php if (!empty($url)) : ?></a><?php endif; ?>
						</div>
					<?php endif ?>
					<?php if($team['instructive']): ?>
						<div class="team-instructive"><?php echo $team['instructive']; ?></div>
					<?php endif ?>
				</div>
			<?php endif ?>
        </div>
    <?php endforeach; ?>
</div>
<script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.nbtloop<?php echo $instance['_sow_form_id']; ?>').owlCarousel({
				rtl:<?php echo is_rtl()?'true':'false'; ?>,
                loop: true,
                responsive:{
                    0: {
                        items: 1,
                        center: true,
                        nav: true
                    },
                    600: {
                        items: <?php echo $number;?>,
                        center: true,
                        nav: <?php echo $nav;?>
                    }
                },
                margin: <?php echo $margin;?>,
                dots: <?php echo $showdot;?>,
                lazyLoad: <?php echo $lazyLoad;?>,
                padding: <?php echo $padding;?>,
                autoplay: <?php echo $autoplay;?>,
                <?php echo $autoplayTimeout;?>
              });
          }); 
    </script>
<?php endif; ?>