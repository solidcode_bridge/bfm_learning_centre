<?php
/*
  Widget Name: NetBaseTeam Out Teams Widget
  Description: Out Teams Widget.
  Author: NetBaseTeam
  Author URI: https://netbaseteam.com
 */

class NBTsow_Out_Teams_Widget extends SiteOrigin_Widget {
	

    function __construct() {
        parent::__construct(
			'nbtsow-out-teams-widget', __('NetBaseTeam Out Teams Widget', 'so-widgets-bundle'), array(
				'description' => __('NetBaseTeam Out Teams Widget', 'so-widgets-bundle'),
				'panels_groups' => array('nbtsow-widgets')
			), array(
			), false, plugin_dir_path(__FILE__) . '../'
        );
    }

    function initialize_form() {
        return array(
            'title' => array(
                'type' => 'text',
                'label' => __('Title', 'so-widgets-bundle'),
            ),
            'description' => array(
                'type' => 'textarea',
                'label' => __('Description', 'so-widgets-bundle'),
            ),
            
            'teams' => array(
                'type' => 'repeater',
                'label' => __('Teams', 'so-widgets-bundle'),
                'item_name' => __('Testimonial', 'so-widgets-bundle'),
                'item_label' => array(
                    'selector' => "[id*='teams-name']",
                    'update_event' => 'change',
                    'value_method' => 'val'
                ),
                'fields' => array(
                    'name' => array(
                        'type' => 'text',
                        'label' => __('Name', 'so-widgets-bundle'),
                        'description' => __('The author of the testimonial', 'so-widgets-bundle'),
                    ),
                    'link_name' => array(
                        'type' => 'checkbox',
                        'label' => __('Link name', 'so-widgets-bundle'),
                    ),
                    'instructive' => array(
                        'type' => 'text',
                        'label' => __('Instructive', 'so-widgets-bundle'),
                        'description' => __('Instructive', 'so-widgets-bundle'),
                    ),
                    'image' => array(
                        'type' => 'media',
                        'label' => __('Image', 'so-widgets-bundle'),
                    ),
                    'link_image' => array(
                        'type' => 'checkbox',
                        'label' => __('Link image', 'so-widgets-bundle'),
                    ),
                    'url' => array(
                            'type' => 'link',
                            'label' => __('Destination URL', 'so-widgets-bundle'),
                    ),
                    'new_window' => array(
                        'type' => 'checkbox',
                        'label' => __('Open In New Window', 'so-widgets-bundle'),
                    ),
                )
            ),
            'settings' => array(
                'type' => 'section',
                'label' => __('Settings', 'so-widgets-bundle'),
                'fields' => array(
                    'numbershow' => array(
                        'type' => 'slider',
                        'label' => __('Number team show', 'so-widgets-bundle'),
                        'integer' => true,
                        'default' => 3,
                        'max' => 15,
                        'min' => 1,
                    ),
                    'showdot' => array(
                        'type' => 'checkbox',
                        'label' => __('Show carousel dot', 'so-widgets-bundle'),
                    ),
                    'showbutton' => array(
                        'type' => 'checkbox',
                        'label' => __('Show carousel buttons Next/Prev', 'so-widgets-bundle'),
                    ),
                    'autoplay' => array(
                        'type' => 'checkbox',
                        'label' => __('Testimonial auto play ', 'so-widgets-bundle'),
                    ),
                    'lazyLoad' => array(
                        'type' => 'checkbox',
                        'label' => __('Lazy load images', 'so-widgets-bundle'),
                    ),
                    'timeplay' => array(
                        'type' => 'text',
                        'label' => __('Testimonial time auto play ', 'so-widgets-bundle'),
                        'default' => 3000
                    ),
                    'margin' => array(
                        'type' => 'slider',
                        'label' => __('Margin-right(px) on item', 'so-widgets-bundle'),
                        'integer' => true,
                        'default' => 0,
                        'max' => 150,
                        'min' => 0,
                    ),
                    'padding' => array(
                        'type' => 'slider',
                        'label' => __('Padding left and right on stage', 'so-widgets-bundle'),
                        'integer' => true,
                        'default' => 0,
                        'max' => 150,
                        'min' => 0,
                    ),
                )
            ),
            'design' => array(
                'type' => 'section',
                'label' => __('Design', 'so-widgets-bundle'),
                'fields' => array(
                    'image' => array(
                        'type' => 'section',
                        'label' => __('Image', 'so-widgets-bundle'),
                        'fields' => array(
                            'image_size' => array(
                                    'type' => 'image-size',
                                    'label' => __('Icon image size', 'so-widgets-bundle'),
                            ),
                            
                        ),
                    ),
                    'colors' => array(
                        'type' => 'section',
                        'label' => __('Colors', 'so-widgets-bundle'),
                        'fields' => array(
                            'name' => array(
                                'type' => 'color',
                                'label' => __('Name color', 'so-widgets-bundle'),
                                'default' => ''
                            ),
                            'name_hover' => array(
                                'type' => 'color',
                                'label' => __('Name hover color', 'so-widgets-bundle'),
                                'default' => ''
                            ),
                            'instructive' => array(
                                'type' => 'color',
                                'label' => __('Location color', 'so-widgets-bundle'),
                                'default' => ''
                            ),
                            'button_color' => array(
                                'type' => 'color',
                                'label' => __('Next/Prev button color'),
                                'default' => '#1c81c5'
                            ),
                            'teams_background' => array(
                                'type' => 'color',
                                'label' => __('Widget Background', 'so-widgets-bundle'),
                            ),
                        ),
                    ),
                ),
            )
        );
    }
    
    public function get_template_variables( $instance, $args ) {
        return array(
                'title' => $instance['title'],
                'description' => $instance['description'],
                'number' => $instance['settings']['numbershow'],
                'showdot' => !empty($instance['settings']['showdot']) ? 'true' : 'false',
                'nav' => !empty($instance['settings']['showbutton']) ? 'true' : 'false',
                'lazyLoad' => !empty($instance['settings']['lazyLoad']) ? 'true' : 'false',
                'margin' => $instance['settings']['margin'],
                'padding' => $instance['settings']['padding'],
                'autoplay' => !empty($instance['settings']['autoplay']) ? 'true' : 'false',
                'timeplay' => !empty($instance['settings']['timeplay']) ? $instance['settings']['timeplay'] : 0,
                'image_size' => $instance['design']['image']['image_size'],
        );
    }

    function get_template_name($instance) {
        return 'default';
    }
    

    /**
     * Get the LESS variables for the price table widget.
     *
     * @param $instance
     *
     * @return array
     */
    function get_less_variables($instance) {
        return array(
            'name_color' => $instance['design']['colors']['name'],
            'name_color_hover' => $instance['design']['colors']['name_hover'],
            'instructive' => $instance['design']['colors']['instructive'],
            'button_color' => $instance['design']['colors']['button_color'],
            'teams_background' => $instance['design']['colors']['teams_background'],
        );
    }

}

siteorigin_widget_register('nbtsow-out-teams-widget', __FILE__, 'NBTsow_Out_Teams_Widget');
