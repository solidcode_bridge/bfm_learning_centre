<?php
$query = siteorigin_widget_post_selector_process_query($instance['posts']);
$posts = new WP_Query($query);

$target = '';
if ($new_window)
    $target = 'target="_blank"';
?>
<?php if ($posts->have_posts()) : ?>
    <div class="nbtsow-post-carousel-widget">
        <?php if (!empty($title)){
			echo $args['before_title'] . esc_html($title) . $args['after_title'];
		} ?>
        <span><?php echo esc_html($description); ?></span>
        <div class="nbtsow-post-carousel<?php echo $instance['_sow_form_id']; ?> owl-carousel">
            <?php while ($posts->have_posts()) : $posts->the_post(); ?>
                <div class="item">
                    <div class="nbtsow-post-carousel-thumb">
                        <?php if ($link): ?><a href="<?php the_permalink(); ?>" <?php echo $target; ?>><?php endif; ?>
                            <?php
                            if (has_post_thumbnail()) :
                                echo '<span>';
                                    the_post_thumbnail($thumb_size);
                                echo '</span>';
                            endif;
                            ?>
                        <?php if ($link): ?></a><?php endif; ?>
                    </div>
                    <?php if ($title_show): ?>
                        <div class="nbtsow-post-carousel-title">
                            <?php if ($link): ?><a href="<?php the_permalink(); ?>" <?php echo $target; ?>><?php endif; ?>
                                <?php echo esc_html__(wp_trim_words(get_the_title(), $limitchar, $more)); ?>
                            <?php if ($link): ?></a><?php endif; ?>
                        </div>
                <?php endif; ?>
                </div>
        <?php endwhile; ?>  
        </div> 
    <?php if ($viewall): ?>
            <div class="nbtsow-post-carousel-viewall">
                <a href="<?php echo $linkall; ?>" target="_blank"><?php echo $viewall; ?></a>
            </div>
    <?php endif; ?>   
    </div>    
<?php endif; ?>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('.nbtsow-post-carousel<?php echo $instance['_sow_form_id']; ?>').owlCarousel({
			rtl:<?php echo is_rtl()?'true':'false'; ?>,
            loop: true,
            margin: 10,
            dots: false,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: <?php echo $item; ?>,
                    nav: true
                }
            }
        });
    });
</script>    