<?php
/*
  Widget Name: NetBaseTeam Post Carousel Widget
  Description: NetBaseTeam Post Carousel widget.
  Author: NetBaseTeam
  Author URI: https://netbaseteam.com
 */

class NBTsow_Post_Carousel_Widget extends SiteOrigin_Widget {

    function __construct() {
        parent::__construct(
                'nbtsow-post-carousel-widget', esc_html__('NetBaseTeam Post Carousel Widget', 'nbtsow'), array(
            'description' => __('Display Post Carousel Widget', 'nbtsow'),
            'panels_groups' => array('nbtsow-widgets')
                ), array(
                ), false, plugin_dir_path(__FILE__)
        );
    }

    function get_widget_form() {

        return array(
            'title' => array(
                'type' => 'text',
                'label' => esc_html__('Title', 'nbtsow'),
            ),
            'description' => array(
                'type' => 'textarea',
                'label' => esc_html__('Description', 'nbtsow'),
            ),
            'posts' => array(
                'type' => 'posts',
                'label' => __('Posts query', 'nbtsow'),
            ),
            'settings' => array(
                'type' => 'section',
                'label' => esc_html__('Settings', 'nbtsow'),
                'fields' => array(
                    'title_show' => array(
                        'type' => 'checkbox',
                        'label' => esc_html__('Display title post', 'nbtsow'),
                        'default' => ''
                    ),

                    'item' => array(
                        'type' => 'text',
                        'label' => esc_html__('Show number item display ', 'nbtsow'),
                        'default' => 5,
                    ),
                    'limitchar' => array(
                        'type' => 'slider',
                        'label' => esc_html__('Limit charactor title post', 'nbtsow'),
                        'default' => '',
                        'min' => 1,
                        'max' => 150,
                    ),
                    'more' => array(
                        'type' => 'text',
                        'label' => esc_html__('What to append if text needs to be trimmed', 'nbtsow'),
                        'default' => '...',
                    ),
                    'link' => array(
                        'type' => 'checkbox',
                        'label' => esc_html__('Link post', 'nbtsow'),
                        'default' => ''
                    ),
                    'new_window' => array(
                        'type' => 'checkbox',
                        'label' => __('Open In New Window', 'nbtsow'),
                    ),
                    'thumb_size' => array(
                        'type' => 'image-size',
                        'label' => esc_html__('Image size', 'nbtsow'),
                    ),
                    'viewall' => array(
                        'type' => 'text',
                        'label' => esc_html__('View all text'),
                        'default' => ''
                    ),
                    'linkall' => array(
                        'type' => 'text',
                        'label' => esc_html__('Link view all', 'nbtsow'),
                        'default' => ''
                    ),
                )
            ),
        );
    }

    function get_template_variables($instance, $args) {
        return array(
            'title' => $instance['title'],
            'description' => $instance['description'],
            'item' => $instance['settings']['item'],
            'thumb_size' => $instance['settings']['thumb_size'],
            'linkall' => $instance['settings']['linkall'],
            'viewall' => $instance['settings']['viewall'],
            'link' => $instance['settings']['link'],
            'new_window' => $instance['settings']['new_window'],
            'title_show' => $instance['settings']['title_show'],
            'limitchar' => $instance['settings']['limitchar'],
            'more' => $instance['settings']['more'],
            'posts' => $instance['posts'],
        );
    }

    function get_template_name($instance) {
        return 'default';
    }

}

siteorigin_widget_register('nbtsow-post-carousel-widget', __FILE__, 'NBTsow_Post_Carousel_Widget');
