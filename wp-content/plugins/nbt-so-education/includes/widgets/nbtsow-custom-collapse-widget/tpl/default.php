<?php if (!empty($title)): ?>
	<<?php echo ($tag ? $tag : 'h3'); ?> class="nbtsow-custom-collapse-title">
		<?php echo $title; ?>
	</<?php echo ($tag ? $tag : 'h3'); ?>>
<?php endif;
if (!empty($description)): ?>
	<div class="nbtsow-custom-collapse-desc">
		<?php echo $description; ?>
	</div>
<?php endif;
if (!empty($items)): ?>
	<div class="panel-group nbtsow-custom-collapses" id="custom-collapse" role="tablist" aria-multiselectable="true">
		<?php foreach ($items as $key=>$item): ?>
			<div class="panel panel-style<?php echo ($key==0?' active':''); ?>">
				<a role="button" data-toggle="collapse" data-parent="#custom-collapse" href="#collapse<?php echo esc_attr($key); ?>" aria-expanded="true" aria-controls="collapse<?php echo esc_attr($key); ?>">
					<i class="fa fa-angle-right"></i>
				</a>
				<<?php echo ($tag_i ? $tag_i : 'h4'); ?> class="panel-title style">
					<a role="button" data-toggle="collapse" data-parent="#custom-collapse" href="#collapse<?php echo esc_attr($key); ?>" aria-expanded="true" aria-controls="collapse<?php echo esc_attr($key); ?>">
						<?php echo $item['title']; ?>
					</a>
				</<?php echo ($tag_i ? $tag_i : 'h4'); ?>>
				<div id="collapse<?php echo esc_attr($key); ?>" class="panel-collapse collapse<?php echo ($key==0?' in':''); ?>" role="tabpanel">
					<div class="panel-body">
						<?php echo $item['description']; ?>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif;
?>