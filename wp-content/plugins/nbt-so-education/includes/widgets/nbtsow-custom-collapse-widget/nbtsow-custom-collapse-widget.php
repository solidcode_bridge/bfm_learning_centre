<?php

/*
Widget Name: NetBaseTeam Custom Collapse Widget
Description: Collapsible content panels
Author: NetBaseTeam
Author URI: http://netbaseteam.com
*/

class NBTSOW_Custom_Collapse_Widget extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'nbtsow-custom-collapse-widget',
			esc_html__('Collapsible content panels', 'nbtsow'),
			array(
				'description' => esc_html__('NetBaseTeam Custom Collapse Widget, button', 'nbtsow'),
			), array(
			), false, plugin_dir_path(__FILE__) . '../'
		);
	}
	
	function initialize() {
        $this->register_frontend_styles(
			array(
				array(
					'nbtsow-custom',
					plugin_dir_url(__FILE__) . 'css/style.css',
					array(),
					''
				)
			)
        );
    }

    function initialize_form() {
        return array(
			'title' => array(
				'type' => 'text',
				'label' => __('Title', 'nbtsow'),
			),
			'tag' => array(
				'type' => 'select',
				'label' => esc_html__( 'HTML Tag', 'nbtsow' ),
				'default' => 'h3',
				'options' => array(
					'h1' => esc_html__( 'H1', 'nbtsow' ),
					'h2' => esc_html__( 'H2', 'nbtsow' ),
					'h3' => esc_html__( 'H3', 'nbtsow' ),
					'h4' => esc_html__( 'H4', 'nbtsow' ),
					'h5' => esc_html__( 'H5', 'nbtsow' ),
					'h6' => esc_html__( 'H6', 'nbtsow' ),
					'p' => esc_html__( 'Paragraph', 'nbtsow' ),
				)
			),
			'description' => array(
				'type' => 'tinymce',
				'label' => __( 'Description', 'nbtsow' ),
				'rows' => 10,
				'default_editor' => 'html',
				'button_filters' => array(
					'mce_buttons' => array( $this, 'filter_mce_buttons' ),
					'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
					'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
					'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
					'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
				),
			),
			'tag_i' => array(
				'type' => 'select',
				'label' => esc_html__( 'HTML Tag (Title Collapse)', 'nbtsow' ),
				'default' => 'h3',
				'options' => array(
					'h1' => esc_html__( 'H1', 'nbtsow' ),
					'h2' => esc_html__( 'H2', 'nbtsow' ),
					'h3' => esc_html__( 'H3', 'nbtsow' ),
					'h4' => esc_html__( 'H4', 'nbtsow' ),
					'h5' => esc_html__( 'H5', 'nbtsow' ),
					'h6' => esc_html__( 'H6', 'nbtsow' ),
					'p' => esc_html__( 'Paragraph', 'nbtsow' ),
				)
			),
			'items' => array(
				'type' => 'repeater',
				'label' => esc_html__('Section List', 'nbtsow'),
				'item_name' => esc_html__('Collapse', 'nbtsow'),
				'item_label' => array(
					'selector' => "[id*='collapse_item']",
					'update_event' => 'change',
					'value_method' => 'val'
				),
				'fields' => array(
					'title' => array(
						'type' => 'text',
						'label' => esc_html__('Title for section', 'nbtsow'),
					),
					'description' => array(
						'type' => 'tinymce',
						'label' => __( 'Description', 'nbtsow' ),
						'rows' => 10,
						'default_editor' => 'html',
						'button_filters' => array(
							'mce_buttons' => array( $this, 'filter_mce_buttons' ),
							'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
							'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
							'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
							'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
						),
					),
				)
			),
		);
    }

	function get_template_variables($instance, $args) {
		return array(
			'title' => $instance['title'] ? $instance['title'] : '',
			'tag' => $instance['tag'] ? $instance['tag'] : 'h3',
			'description' => $instance['description'] ? $instance['description'] : '',
			'tag_i' => $instance['tag_i'] ? $instance['tag_i'] : 'h3',
			'items' => !empty($instance['items']) ? $instance['items'] : array(),
		);
	}

	function get_template_name($instance) {
		return 'default';
	}
}

siteorigin_widget_register('nbtsow-custom-collapse-widget', __FILE__, 'NBTSOW_Custom_Collapse_Widget');
