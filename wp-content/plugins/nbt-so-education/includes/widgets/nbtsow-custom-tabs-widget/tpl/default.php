<?php
$data_posts = $this->getPostCats($instance);
?>
<div class="nbtsow-custom-tabs">
    <?php if( !empty($title) ) {
         echo $args['before_title'] . esc_html($title) . $args['after_title'];
    } ?>
    <?php if (!empty($description)){
        echo '<span class="widget-description">' . esc_html($description) . '</span>';
    }?>
    <ul class="nav nav-justified unstyled">
        <?php foreach($data_posts as $key => $categorytab):
         $active = ($key == 0) ? 'class="active"' : '';
        ?>
            <li <?php echo $active;?>>
                <a data-toggle="tab" href="#nbtsow_custom_tab<?php echo $instance['panels_info']['id'];?><?php echo $key;?>"><?php echo $categorytab['cat_name'];?></a>
            </li>
        <?php endforeach;?>
    </ul>
    <div class="tab-content">
        <?php foreach($data_posts as $key => $posts):
        $class = ($key == 0) ? 'class="tab-pane fade in active"' : 'class="tab-pane fade"';?>  
            <div id="nbtsow_custom_tab<?php echo $instance['panels_info']['id'];?><?php echo $key?>" <?php echo $class;?>>
                <?php if($posts['postdatas']):
                $the_query = new WP_Query( $posts['postdatas'] );
                if ( $the_query->have_posts() ) :
                    while ( $the_query->have_posts() ) : $the_query->the_post(); ?>  
                        <div class="nbtsow-custom-tab nbtsow-item<?php if( is_rtl() ) echo ' rtl' ?>">
                            <div class="nbtsow-tabcontent-thumbnail">
                                <?php if (has_post_thumbnail()) {
                                        the_post_thumbnail($thumb_size);
                                } ?>
                            </div> 
                            <div class="nbtsow-detail">
                                <h3><?php the_title() ?></h3>
                                <div class="nbtsow-expand"><i class="nbticon-up-arrow4" aria-hidden="true"></i></div>
                                <div class="nbtsow-contentshort">
                                    <?php if(get_the_excerpt()):?><span><?php echo esc_html(get_the_excerpt()); ?></span><?php endif;?>
                                </div>
                                <div class="nbtsow-content">
                                    
                                    <?php
                                    if ( get_post_gallery() ) :
                                        $gallery = get_post_gallery( get_the_ID(), false );
                                        foreach( $gallery['src'] as $src ) : ?>
                                            <img src="<?php echo $src; ?>" class="my-custom-class" alt="Gallery image" />
                                            <?php
                                        endforeach;
                                    else: ?>
                                        <span>
                                        <?php echo esc_html(get_the_content());?>
                                         </span>
                                    <?php endif; ?>
                                </div>
                            </div>                                                                                
                        </div> 
                    <?php endwhile; wp_reset_postdata();?>
                <?php endif;?>
            </div>
        <?php endif;?>
        <?php endforeach;?>
    </div>    
</div>
