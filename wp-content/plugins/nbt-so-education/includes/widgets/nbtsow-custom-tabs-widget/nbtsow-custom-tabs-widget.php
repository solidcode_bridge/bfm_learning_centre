<?php
/*
  Widget Name: NetBaseTeam Custom Tabs Widget
  Description: NetBaseTeam Custom Tabs widget.
  Author: NetBaseTeam
  Author URI: https://netbaseteam.com
 */

class NBTsow_Custom_Tabs_Widget extends SiteOrigin_Widget {

    function __construct() {
        parent::__construct(
                'nbtsow-custom-tabs-widget', esc_html__('NetBaseTeam Custom Tabs Widget', 'nbtsow'), array(
            'description' => __('Display Custom Tabs Widget', 'nbtsow'),
            'panels_groups' => array('nbtsow-widgets')
                ), array(
                ), false, plugin_dir_path(__FILE__)
        );
    }

    function get_widget_form() {

        return array(
            'title' => array(
                'type' => 'text',
                'label' => esc_html__('Title', 'nbtsow'),
            ),
            'description' => array(
                'type' => 'textarea',
                'label' => esc_html__('Description', 'nbtsow'),
            ),
            'categories' => array(
                'type' => 'repeater',
                'label' => esc_html__('Category Tabs', 'nbtsow'),
                'item_name' => esc_html__('Category Tab', 'nbtsow'),
                'item_label' => array(
                    'selector' => "[id*='custom-tabs-name']",
                    'update_event' => 'change',
                    'value_method' => 'val'
                ),
                'fields' => array(
                    'cat_name' => array(
                        'type' => 'text',
                        'label' => esc_html__('Tab name', 'nbtsow'),
                    ),
                    'get_cats' => array(
                        'type' => 'select',
                        'label' => esc_html__('Select category', 'nbtsow'),
                        'multiple' => true,
                        'options' => $this->getCategories()
                    ),
                    'post_types' => array(
                        'type' => 'select',
                        'label' => esc_html__('Post type', 'nbtsow'),
                        'options' => $this->getPostType()
                    ),
                    'orderby' => array(
                        'type' => 'select',
                        'label' => esc_html__('Order by', 'nbtsow'),
                        'options' => array(
                            'none' => esc_html__('No order', 'nbtsow'),
                            'ID' => esc_html__('Post ID', 'nbtsow'),
                            'author' => esc_html__('Author', 'nbtsow'),
                            'title' => esc_html__('Title', 'nbtsow'),
                            'date' => esc_html__('Published date', 'nbtsow'),
                            'modified' => esc_html__('Modified date', 'nbtsow'),
                            'parent' => esc_html__('By parent', 'nbtsow'),
                            'rand' => esc_html__('Random order', 'nbtsow'),
                            'comment_count' => __('Comment count', 'nbtsow'),
                            'menu_order' => esc_html__('Menu order', 'nbtsow'),
                            'meta_value' => esc_html__('By meta value', 'nbtsow'),
                            'meta_value_num' => esc_html__('By numeric meta value', 'nbtsow'),
                            'post__in' => esc_html__('By include order', 'nbtsow'),
                        )
                    ),
                    'order' => array(
                        'type' => 'select',
                        'label' => esc_html__('Order', 'nbtsow'),
                        'options' => array(
                            'none' => esc_html__('No order', 'nbtsow'),
                            'ASC' => esc_html__('ASC', 'nbtsow'),
                            'DESC' => esc_html__('DESC', 'nbtsow'),
                        )
                    )
                )
            ),
            'settings' => array(
                'type' => 'section',
                'label' => esc_html__('Settings', 'nbtsow'),
                'fields' => array(
                    'limitpost' => array(
                        'type' => 'slider',
                        'label' => esc_html__('Posts limit display', 'nbtsow'),
                        'default' => 5,
                        'min' => 1,
                        'max' => 20
                    ),
                    'thumb_size' => array(
                        'type' => 'image-size',
                        'label' => esc_html__('Image size', 'nbtsow'),
                    ),
                    'widget_title_color' => array(
                        'type' => 'color',
                        'label' => esc_html__('Widget title color', 'nbtsow'),
                        'default' => ''
                    ),
                    'widget_title_desc' => array(
                        'type' => 'color',
                        'label' => esc_html__('Widget description color', 'nbtsow'),
                        'default' => ''
                    ),
                    'title_color' => array(
                        'type' => 'color',
                        'label' => esc_html__('Post title color', 'nbtsow'),
                        'default' => ''
                    ),
                    'content_color' => array(
                        'type' => 'color',
                        'label' => esc_html__('Post content color', 'nbtsow'),
                        'default' => ''
                    ),
                    'icon_color' => array(
                        'type' => 'color',
                        'label' => esc_html__('Icon down/up color', 'nbtsow'),
                        'default' => ''
                    ),
                    'background' => array(
                        'type' => 'color',
                        'label' => esc_html__('Tabs background color', 'nbtsow'),
                        'default' => ''
                    ),
                )
            ),
        );
    }

    function initialize() {
        $this->register_frontend_styles(
                array(
                    array(
                        'nbtsow-custom-tabs',
                        plugin_dir_url(__FILE__) . 'css/style.css',
                        array(),
                        ''
                    )
                )
        );
        $this->register_frontend_scripts(
                array(
                    array(
                        'nbtsow-custom-tabs',
                        plugin_dir_url(__FILE__) . 'js/customtabs.js',
                        array('jquery'),
                        ''
                    )
                )
        );
    }

    function getCategories() {
        $term_names = get_terms('category', 'orderby=name&fields=id=>name&hide_empty=0');
		$term_slugs = get_terms('category', 'orderby=name&fields=id=>slug&hide_empty=0');
		$categories = array();
		foreach ($term_slugs as $key => $term_slug){
			$categories[$term_slug] = $term_names[$key];
		}
		return $categories;
    }

    function getPostType() {
        $post_types = array();
        foreach (get_post_types(array('public' => true), 'objects') as $id => $type) {
            if (!empty($type->labels->name)) {
                $post_types['_all'] = esc_html__('All', 'nbtsow');
                $post_types[$id] = esc_html__($type->labels->name, 'nbtsow');
            }
        }
        return $post_types;
    }

    function getPostCats($instance) {
        $posts_datas = array();
        foreach ($instance['categories'] as $key => $categorie) {
            $posts_datas[$key]['cat_name'] = $categorie['cat_name'];

            // if (count($categorie['get_cats']) > 1) {
                // $categorie['get_cats'] = implode(',', $categorie['get_cats']);
            // }
			$categories = array();
			if (count($categorie['get_cats']) > 1){
				foreach ($categorie['get_cats'] as $category){
					$categories[] = get_term_by('slug', $category, 'category')->term_id;
				}
				$categorie['get_cats'] = implode(',', $categories);
			} else {
				$categorie['get_cats'] = get_term_by('slug', $categorie['get_cats'], 'category')->term_id;
			}
            $post_types = $categorie['post_types'];

            if ($post_types == '_all') {
                $all_type = $this->getPostType();
                unset($all_type['_all']);
                $post_types = implode(',', $all_type);
            }
			
            $args_post = array(
                'posts_per_page' => $instance['settings']['limitpost'],
                'offset' => 0,
                'cat' => $categorie['get_cats'],
                'orderby' => $categorie['orderby'],
                'order' => $categorie['order'],
                'post_type' => $post_types,
                'post_status' => 'publish',
            );

            $posts_datas[$key]['postdatas'] = $args_post;
        }

        return $posts_datas;
    }

    function get_template_variables($instance, $args) {
        return array(
            'title' => $instance['title'],
            'description' => $instance['description'],
            'thumb_size' => $instance['settings']['thumb_size'],
            'categories' => $instance['categories'],
        );
    }

    function get_less_variables($instance) {
        return array(
            'widget_title_color' => $instance['settings']['widget_title_color'],
            'widget_title_desc' => $instance['settings']['widget_title_desc'],
            'title_color' => $instance['settings']['title_color'],
            'content_color' => $instance['settings']['content_color'],
            'icon_color' => $instance['settings']['icon_color'],
            'background' => $instance['settings']['background'],
        );
    }

    function get_template_name($instance) {
        return 'default';
    }

}

siteorigin_widget_register('nbtsow-custom-tabs-widget', __FILE__, 'NBTsow_Custom_Tabs_Widget');
