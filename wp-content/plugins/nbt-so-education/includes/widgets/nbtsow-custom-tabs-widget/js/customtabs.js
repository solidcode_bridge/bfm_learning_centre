jQuery(document).ready(function ($) {
    var nbtclick = $(".nbtsow-expand").click(function (e) {
        e.preventDefault();
        // if (nbtclick) {
        //     $(this).parents('.nbtsow-custom-tab').eq(0).toggleClass('tab-active');
        // }
        // else{
        //     nbtclick.not(this).parents('.tab-pane');
        //     nbtclick.not(this).find('.tab-active');
        //     nbtclick.not(this).removeClass('.tab-active');
        //     nbtclick.not(this).addClass('.tab-active');
        // } 

        if($(this).parents('.nbtsow-custom-tab').eq(0).hasClass('tab-active')){
            $(this).parents('.nbtsow-custom-tab').eq(0).removeClass('tab-active');
        } else {
            $(this).parents('.tab-pane').eq(0).find('.tab-active').removeClass('tab-active');
             $(this).parents('.nbtsow-custom-tab').eq(0).addClass('tab-active');
        }

        if ($(this).attr('class') == 'active') {
            $(this).addClass('nbtsow-expand');
            $(this).removeClass('active');
            $(this).next().removeClass('hide');
            $(this).next().next().slideUp();
            $(this).html('<i class="nbticon-up-arrow4" aria-hidden="true"></i>');
        } else {
            $(this).removeClass('nbtsow-expand');
            $(this).addClass('active');
            nbtclick.not(this).next().removeClass('hide');
            nbtclick.not(this).removeClass('active');
            nbtclick.not(this).html('<i class="nbticon-up-arrow4" aria-hidden="true"></i>');
            $('div.nbtsow-content:visible').slideUp();
            $(this).next().next().slideDown();
            $(this).next().addClass('hide');
            $(this).html('<i class="nbticon-up-arrow" aria-hidden="true"></i>');
        }
    });
});