<?php
$testimonials = $instance['testimonials'];
$autoplayTimeout = '';
if ($autoplay == 'true')
    $autoplayTimeout = 'autoplayTimeout: ' . $timeplay . ',';
?>

<?php if (!empty($instance['title'])) echo $args['before_title'] . esc_html($instance['title']) . $args['after_title'] ?>
   <?php echo ($description); ?>
<?php if ($testimonials): ?>
    <div class="nbtsow-testimonial-widget layout2">
        <div class="owl-carousel owl-theme" id="nbtsow-testimonials-desc<?php echo $instance['_sow_form_id']; ?>">
            <?php
            foreach ($testimonials as $testimonial):
                $testimonial_desc = $testimonial['text'];
				$image = $this->testimonial_user_image($testimonial['image'], $design, $testimonial['name']);
                ?>
                <div class="testimonial-item">
                    <?php if (!empty($testimonial_desc)) : ?>
                        <div class="testimonial_desc"><span><?php echo esc_html($testimonial_desc); ?></span></div>
                    <?php endif; ?>
					<div class="testimonial-thumb">
						<?php echo $image; ?>
						<?php if ($testimonial['name'] || $testimonial['location']): ?>
							<div class="testimonial-info">
								<?php if ($testimonial['name']): ?>
									<div class="testimonial-name">
										<?php echo $testimonial['name']; ?>
									</div>
								<?php endif; ?>
								<?php if ($testimonial['location']): ?>
									<div class="testimonial-location">
										<?php echo $testimonial['location']; ?>
									</div>
								<?php endif; ?>
							</div>
						<?php endif; ?>
					</div>
                </div>
            <?php endforeach; ?>    
        </div>
    </div>

    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $("#nbtsow-testimonials-desc<?php echo $instance['_sow_form_id']; ?>").owlCarousel({
				rtl:<?php echo is_rtl()?'true':'false'; ?>,
                items: 1,
                slideSpeed: 2000,
                nav: false,
                autoplay: <?php echo $autoplay; ?>,
				<?php echo $autoplayTimeout; ?>
                autoplayHoverPause: true,
                dots: <?php echo $showdot; ?>,
                loop: true,
            });
        });
    </script>

<?php endif; ?>