<?php
$testimonials = $instance['testimonials'];
$autoplayTimeout = '';
if ($autoplay == 'true')
    $autoplayTimeout = 'autoplayTimeout: ' . $timeplay . ',';
?>

<?php if (!empty($instance['title'])) echo $args['before_title'] . esc_html($instance['title']) . $args['after_title'] ?>
   <?php echo ($description); ?>
<?php if ($testimonials): ?>
    <div class="nbtsow-testimonial-widget">
        <div class="owl-carousel owl-theme" id="nbtsow-testimonials-desc<?php echo $instance['_sow_form_id']; ?>">
            <?php
            foreach ($testimonials as $testimonial):
                $testimonial_desc = $testimonial['text'];
                ?>
                <div class="testimonial-item">
                    <?php if (!empty($testimonial_desc)) : ?>
                        <div class="testimonial_desc"><span><?php echo esc_html($testimonial_desc); ?></span></div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>    
        </div>

        <div class="owl-carousel owl-theme nbtsow-testimonial-thumb" id="nbtsow-testimonial-data<?php echo $instance['_sow_form_id']; ?>">
            <?php
            foreach ($testimonials as $testimonial):
                $image = $this->testimonial_user_image($testimonial['image'], $design, $testimonial['name']);
                ?>
                <div class="testimonial-item">
                    <?php echo $image; ?>
                    <?php if ($testimonial['name'] || $testimonial['location']): ?>
                        <div class="testimonial-info">
                            <?php if ($testimonial['name']): ?>
                                <div class="testimonial-name">
                                    <?php echo $testimonial['name']; ?>
                                </div>
                            <?php endif; ?>
                            <?php if ($testimonial['location']): ?>
                                <div class="testimonial-location">
                                    <?php echo $testimonial['location']; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>    
        </div>
    </div>

    <script type="text/javascript">
        jQuery(document).ready(function ($) {

            var sync1 = $("#nbtsow-testimonials-desc<?php echo $instance['_sow_form_id']; ?>");
            var sync2 = $("#nbtsow-testimonial-data<?php echo $instance['_sow_form_id']; ?>");
            var slidesPerPage = <?php echo count($testimonials); ?>; //globaly define number of elements per page
            var syncedSecondary = true;

            sync1.owlCarousel({
				rtl:<?php echo is_rtl()?'true':'false'; ?>,
                items: 1,
                slideSpeed: 2000,
                nav: false,
                autoplay: <?php echo $autoplay; ?>,
    <?php echo $autoplayTimeout; ?>
                autoplayHoverPause: true,
                dots: <?php echo $showdot; ?>,
                loop: true,
            }).on('changed.owl.carousel', syncPosition);

            sync2
                    .on('initialized.owl.carousel', function () {
                        sync2.find(".owl-item").eq(0).addClass("current");
                    })
                    .owlCarousel({
						rtl:<?php echo is_rtl()?'true':'false'; ?>,
                        items: slidesPerPage,
                        smartSpeed: 200,
                        slideSpeed: 500,
                        responsiveRefreshRate: 100
                    }).on('changed.owl.carousel', syncPosition2);

            function syncPosition(el) {
                //if you set loop to false, you have to restore this next line
                //var current = el.item.index;

                //if you disable loop you have to comment this block
                var count = el.item.count - 1;
                var current = Math.round(el.item.index - (el.item.count / 2) - .5);

                if (current < 0) {
                    current = count;
                }
                if (current > count) {
                    current = 0;
                }

                //end block

                sync2
                        .find(".owl-item")
                        .removeClass("current")
                        .eq(current)
                        .addClass("current");
                var onscreen = sync2.find('.owl-item.active').length - 1;
                var start = sync2.find('.owl-item.active').first().index();
                var end = sync2.find('.owl-item.active').last().index();

                if (current > end) {
                    sync2.data('owl.carousel').to(current, 100, true);
                }
                if (current < start) {
                    sync2.data('owl.carousel').to(current - onscreen, 100, true);
                }
            }

            function syncPosition2(el) {
                if (syncedSecondary) {
                    var number = el.item.index;
                    sync1.data('owl.carousel').to(number, 100, true);
                }
            }

            sync2.on("click", ".owl-item", function (e) {
                e.preventDefault();
                var number = $(this).index();
                sync1.data('owl.carousel').to(number, 300, true);
            });
        });
    </script>

<?php endif; ?>