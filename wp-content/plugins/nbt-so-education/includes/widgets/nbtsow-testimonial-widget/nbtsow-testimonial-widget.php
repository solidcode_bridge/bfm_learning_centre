<?php
/*
  Widget Name: Netbaseteam Testimonials
  Description: Display some testimonials.
  Author: Netbaseteam
  Author URI: https://Netbaseteam.com
 */

class NBTsow_Testimonial_Widget extends SiteOrigin_Widget {

    function __construct() {
        parent::__construct(
            'nbtsow-testimonial-widget', __('NetbaseTeam Testimonials', 'ntbsow-widget'), array(
            'description' => __('Display some testimonials', 'ntbsow-widget'),
            'panels_groups' => array('nbtsow-widgets'),
            'help' => 'https://Netbaseteam.com'
                ), array(
                ), false, plugin_dir_path(__FILE__)
        );
    }

    function initialize() {
        $this->register_frontend_styles(array(
            array(
                'nbtsow-testimonial-widget',
                plugin_dir_url(__FILE__) . 'css/style.css'
            )
        ));
    }

    function get_widget_form() {
        return array(
            'title' => array(
                'type' => 'text',
                'label' => __('Title', 'ntbsow-widget'),
            ),
            'description' => array(
                'type' => 'tinymce',
                'label' => __('Description', 'ntbsow-widget'),
            ),
			'temp' => array(
				'type' => 'select',
				'label' => esc_html__('Select template'),
				'options' => array(
					'' => esc_html__('Select a choice...', 'nbtsow'),
					'default' => esc_html__('Default'),
					'default2' => esc_html__('Layout 2')
				)
			),
            'testimonials' => array(
                'type' => 'repeater',
                'label' => __('Testimonials', 'ntbsow-widget'),
                'item_name' => __('Testimonial', 'ntbsow-widget'),
                'item_label' => array(
                    'selector' => "[id*='testimonials-name']",
                    'update_event' => 'change',
                    'value_method' => 'val'
                ),
                'fields' => array(
                    'name' => array(
                        'type' => 'text',
                        'label' => __('Name', 'ntbsow-widget'),
                        'description' => __('The author of the testimonial', 'ntbsow-widget'),
                    ),
                    'location' => array(
                        'type' => 'text',
                        'label' => __('Location', 'ntbsow-widget'),
                        'description' => __('Their location or company name', 'ntbsow-widget'),
                    ),
                    'image' => array(
                        'type' => 'media',
                        'label' => __('Image', 'ntbsow-widget'),
                    ),
                    'text' => array(
                        'type' => 'textarea',
                        'label' => __('Content', 'ntbsow-widget'),
                        'description' => __('What your customer had to say', 'ntbsow-widget'),
                    ),
                )
            ),
            'settings' => array(
                'type' => 'section',
                'label' => __('Settings', 'so-widgets-bundle'),
                'fields' => array(
                    'showdot' => array(
                        'type' => 'checkbox',
                        'label' => __('Creates and enables page dots', 'so-widgets-bundle'),
                    ),
                    'showbutton' => array(
                        'type' => 'checkbox',
                        'label' => __('Show carousel buttons Next/Prev', 'so-widgets-bundle'),
                    ),
                    'autoplay' => array(
                        'type' => 'checkbox',
                        'label' => __('Testimonial auto play ', 'so-widgets-bundle'),
                    ),
                    'timeplay' => array(
                        'type' => 'text',
                        'label' => __('Testimonial time auto play ', 'so-widgets-bundle'),
                        'default' => 3000
                    ),
                )
            ),
            'design' => array(
                'type' => 'section',
                'label' => __('Design', 'ntbsow-widget'),
                'fields' => array(
                    'image' => array(
                        'type' => 'section',
                        'label' => __('Image', 'ntbsow-widget'),
                        'fields' => array(
                            'image_shape' => array(
                                'type' => 'select',
                                'label' => __('Testimonial image shape', 'ntbsow-widget'),
                                'options' => array(
                                    'square' => __('Square', 'ntbsow-widget'),
                                    'round' => __('Round', 'ntbsow-widget'),
                                ),
                                'default' => 'square',
                            ),
                            'image_size' => array(
                                'type' => 'slider',
                                'label' => __('Image size', 'ntbsow-widget'),
                                'integer' => true,
                                'default' => 50,
                                'max' => 150,
                                'min' => 20,
                            ),
                        ),
                    ),
                    'colors' => array(
                        'type' => 'section',
                        'label' => __('Colors', 'ntbsow-widget'),
                        'fields' => array(
                            'testimonial_color' => array(
                                'type' => 'color',
                                'label' => __('Testimonial color', 'ntbsow-widget'),
                                'default' => '#000000'
                            ),
                            'name' => array(
                                'type' => 'color',
                                'label' => __('Name color', 'ntbsow-widget'),
                                'default' => '#000000'
                            ),
                            'name_hover' => array(
                                'type' => 'color',
                                'label' => __('Name hover color', 'ntbsow-widget'),
                                'default' => '#000000'
                            ),
                            'location' => array(
                                'type' => 'color',
                                'label' => __('Location color', 'ntbsow-widget'),
                                'default' => '#000000'
                            ),
                            'testimonial_background' => array(
                                'type' => 'color',
                                'label' => __('Widget Background', 'ntbsow-widget'),
                            ),
                        ),
                    ),
                ),
            ),
        );
    }


    function get_less_variables($instance) {
        return array(
            'image_size' => intval($instance['design']['image']['image_size']) . 'px',
            'testimonial_color' => $instance['design']['colors']['testimonial_color'],
            'name_color' => $instance['design']['colors']['name'],
            'name_color_hover' => $instance['design']['colors']['name_hover'],
            'location' => $instance['design']['colors']['location'],
            'testimonial_background' => $instance['design']['colors']['testimonial_background'],
        );
    }

    function get_template_variables($instance, $args) {
        return array(
            'testimonials' => !empty($instance['testimonials']) ? $instance['testimonials'] : array(),
            'settings' => $instance['settings'],
            'design' => $instance['design'],
            'title' => $instance['title'],
            'description' => $instance['description'],
            'showdot' => !empty($instance['settings']['showdot']) ? 'true' : 'false',
            'showbutton' => !empty($instance['settings']['showdot']) ? 'true' : 'false',
            'autoplay' => !empty($instance['settings']['autoplay']) ? 'true' : 'false',
            'timeplay' => !empty($instance['settings']['timeplay']) ? $instance['settings']['timeplay'] : 0,
            'image_size' => $instance['design']['image']['image_size'],
        );
    }
	
	function get_template_name($instance) {
        $template = '';
        if ($instance['temp'] == '') {
            $template = 'default';
        } else {
            $template = $instance['temp'];
        }
        return $template;
    }

    function testimonial_user_image($image_id, $design, $name) {
        if (!empty($image_id)) {
            $src = wp_get_attachment_image_src($image_id, array($design['image']['image_size'], $design['image']['image_size']));
            return  "<img class='nbtsow-image-shape-".$design['image']['image_shape']."' src='". esc_url($src[0])."' alt='".$name."'>";
        } else {
			return '';
		}
    }

    

}

siteorigin_widget_register('nbtsow-testimonial-widget', __FILE__, 'NBTsow_Testimonial_Widget');
