<?php

/*
  Widget Name: NetBaseTeam Learndash Popular Post
  Description: NetBaseTeam Learndash Popular Post
  Author: NetBaseTeam
  Author URI: https://netbaseteam.com
 */
defined('ABSPATH') or die('No script kiddies please!');

class NBTSOW_Learndash_Popular_Widget extends SiteOrigin_Widget {

    function __construct() {

        parent::__construct(
            'nbtsow-learndash-popular-widget', esc_html__('NetBaseTeam Learndash Popular Posts Widget', 'nbtsow'), array(
				'description' => 'Display Popular Posts Widget'
			), array(), array(
				'title' => array(
					'type' => 'text',
					'label' => esc_html__('Title of Widget ', 'nbtsow'),
					'default' => 'Popular courses',
				),
				'title_url' => array(
					'type' => 'link',
					'label' => esc_html__('Enter title url Widget:  ', 'nbtsow'),
					'default' => '',
				),
				'description' => array(
					'type' => 'textarea',
					'label' => esc_html__('Enter description Widget:  ', 'nbtsow'),
					'default' => '',
				),
				'post_type' => array(
					'type' => 'select',
					'label' => esc_html__('Post type'),
					'options' => array(
						'' => esc_html__('All', 'nbtsow'),
						'post' => esc_html__('Post', 'nbtsow'),
						'sfwd-courses' => esc_html__('Courses', 'nbtsow')
					)
				),
				'categories' => array(
					'type' => 'select',
					'label' => esc_html__('Select category'),
					'multiple' => true,
					'options' => $this->getCategories()
				),
				'layout' => array(
					'type' => 'select',
					'label' => esc_html__('Select layout'),
					'options' => array(
						'' => esc_html__('Select a choice...', 'nbtsow'),
						'default' => esc_html__('Default'),
						'grid' => esc_html__('Grid'),
						'tab' => esc_html__('Tab'),
						'tab-style' => esc_html__('Tab Style 1'),
						'tab-style2' => esc_html__('Tab Style 2'),
						'tab-style3' => esc_html__('Tab Style 3')
					)
				),
				'post_number' => array(
					'type' => 'number',
					'label' => esc_html__('Number of post to show'),
					'default' => 5
				),
				'col' => array(
					'type' => 'section',
					'label' => __( 'Grid options' , 'widget-form-fields-text-domain' ),
					'hide' => true,
					'fields' => array(
						'lg' => array(
							'type' => 'slider',
							'label' => __( 'Large devices Desktops (≥1200px)', 'nbtsow' ),
							'default' => 4,
							'min' => 1,
							'max' => 6,
							'integer' => true
						),
						'md' => array(
							'type' => 'slider',
							'label' => __( 'Medium devices Desktops (≥992px)', 'nbtsow' ),
							'default' => 3,
							'min' => 1,
							'max' => 6,
							'integer' => true
						),
						'sm' => array(
							'type' => 'slider',
							'label' => __( 'Small devices Tablets (≥768px)', 'nbtsow' ),
							'default' => 2,
							'min' => 1,
							'max' => 6,
							'integer' => true
						),
						'xs' => array(
							'type' => 'slider',
							'label' => __( 'Extra small devices Phones (<768px)', 'nbtsow' ),
							'default' => 1,
							'min' => 1,
							'max' => 6,
							'integer' => true
						)
					)
				),
				'titleshow' => array(
					'type' => 'checkbox',
					'label' => esc_html__('Title of post to show'),
					'default' => true
				),
				'reviews' => array(
					'type' => 'checkbox',
					'label' => esc_html__('Reviews of post to show'),
					'default' => true
				),
				'dateshow' => array(
					'type' => 'checkbox',
					'label' => esc_html__('Date publish of post to show'),
					'default' => false
				),
				'priceshow' => array(
					'type' => 'checkbox',
					'label' => esc_html__('Price of course to show'),
					'default' => false
				),
				'contentshow' => array(
					'type' => 'checkbox',
					'label' => esc_html__('Content publish of post to show'),
					'default' => false
				),
				'contentcharlimit' => array(
					'type' => 'text',
					'label' => esc_html__('Content character limit of post'),
					'default' => 25
				),
				'order' => array(
					'type' => 'select',
					'label' => esc_html__('Order'),
					'options' => array(
						'' => esc_html__('Select a choice...', 'nbtsow'),
						'ASC' => esc_html__('Ascending', 'nbtsow'),
						'DESC' => esc_html__('Descending', 'nbtsow')
					)
				),
				'orderby' => array(
					'type' => 'select',
					'label' => esc_html__('Order by'),
					'options' => array(
						'' => esc_html__('Select a choice...', 'nbtsow'),
						'post_date' => esc_html__('Date', 'nbtsow'),
						'post_title' => esc_html__('Title', 'nbtsow'),
						'meta_value' => esc_html__('Views', 'nbtsow'),
						'rand()' => esc_html__('Random', 'nbtsow'),
					)
				),
				'image_file' => array(
					'type' => 'media',
					'label' => esc_html__('Image file', 'nbtsow'),
					'description' => esc_html__('Display image file if article no thumbnail image ', 'nbtsow'),
					'library' => 'image',
					'fallback' => true,
				),
				'image_file_show' => array(
					'type' => 'checkbox',
					'label' => esc_html__('Show image file'),
					'default' => true
				),
				'thumbsize' => array(
					'type' => 'image-size',
					'label' => esc_html__('Thumbnail size (Width x Height)'),
				),
				'button' => array(
					'type' => 'section',
					'label' => esc_html__( 'Button Option', 'nbtsow' ),
					'hide' => false,
					'fields' => array(
						'text' => array(
							'type' => 'text',
							'label' => esc_html__('Button Text', 'nbtsow'),
						),
						'href' => array(
							'type' => 'link',
							'label' => esc_html__('Button Link', 'nbtsow'),
						),
						'icon' => array(
							'type' => 'section',
							'label' => esc_html__( 'Select icon', 'nbtsow' ),
							'hide' => true,
							'fields' => array(
								'icon' => array(
									'type' => 'icon',
									'label' => __('Icon.', 'nbtsow')
								),
								'icon_color' => array(
									'type' => 'color',
									'label' => __('Icon Color', 'nbtsow')
								),
								'icon_size' => array(
									'type' => 'slider',
									'label' => __('Icon Size', 'nbtsow'),
									'min' => 1,
									'max' => 64,
									'integer' => true,
									'default' => 24
								),
								'icon_media' => array(
									'type' => 'media',
									'label' => __( 'Image icon', 'nbtsow' ),
									'description' => __('Replaces the icon with your own image icon.', 'so-widgets-bundle'),
								)
							),
						),
						'order' => array(
							'type' => 'order',
							'label' => esc_html__( 'Element Order', 'nbtsow' ),
							'options' => array(
								'icon' => esc_html__( 'Icon', 'nbtsow' ),
								'text' => esc_html__( 'Text', 'nbtsow' ),
							),
							'default' => array('icon','text'),
						)
					),
				),
            )
        );
    }
    
    function initialize() {
        $this->register_frontend_styles(
            array(
                array(
                    'nbtsow-learndash-popular',
                    plugin_dir_url(__FILE__) . 'css/style.css',
                    array(),
                    ''
                )
            )
        );
        
    }

    function getCategories() {
		$term_names = get_terms('category', 'orderby=name&fields=id=>name&hide_empty=0');
		$term_slugs = get_terms('category', 'orderby=name&fields=id=>slug&hide_empty=0');
		$categories = array();
		foreach ($term_slugs as $key => $term_slug){
			$categories[$term_slug] = $term_names[$key];
		}
		return $categories;
    }

    function get_template_variables($instance, $args) {

        return array(
            'title' => !empty($instance['title']) ? $instance['title'] : '',
            'title_url' => !empty($instance['title_url']) ? $instance['title_url'] : '',
            'description' => $instance['description'],
            'post_number' => $instance['post_number'],
            'reviews' => $instance['reviews'],
            'dateshow' => $instance['dateshow'],
            'priceshow' => $instance['priceshow'],
            'order' => $instance['order'],
            'orderby' => $instance['orderby'],
            'thumbsize' => $instance['thumbsize'],
            'post_type' => $instance['post_type'],
            'col' => $instance['col'],
            'contentcharlimit' => $instance['contentcharlimit'],
            'contentshow' => $instance['contentshow'],
            'titleshow' => $instance['titleshow'],
            'categories' => $instance['categories'],
            'image_file' => $instance['image_file'],
            'image_file_show' => $instance['image_file_show'],
			'button' => !empty($instance['button']) ? $instance['button'] : array(),
        );
    }

    function get_template_name($instance) {
        $template = '';
        if ($instance['layout'] == '') {
            $template = 'default';
        } else {
            $template = $instance['layout'];
        }
        return $template;
    }

    function get_style_name($instance) {
        return '';
    }

    function get_popularPost($instance) {
        global $wpdb;
        $postmeta_tbl = $wpdb->prefix . 'postmeta';
		$categories = array();
		if (count($instance['categories'])){
			if (is_array($instance['categories'])){
				foreach ($instance['categories'] as $category){
					$categories[] = get_term_by('slug', $category, 'category')->term_id;
				}
				$category = implode(',', $categories);
			} else {
				$categories[] = get_term_by('slug', $instance['categories'], 'category')->term_id;
				$category = $categories[0];
			}
		}
		
        if ($instance['orderby'])
            $instance['orderby'] = 'ORDER BY ' . $instance['orderby'] . ' ' . $instance['order'];
            $categories = !empty($categories) ? 'and tr.term_taxonomy_id IN (' . $category . ')' : '';
        $post_type = !empty($instance['post_type']) ? 'p.post_type IN ("' . $instance['post_type'] . '") AND ' : '';

        $popular = "
                SELECT DISTINCT p.ID,p.post_title,p.post_content,p.guid FROM {$wpdb->prefix}posts p
                INNER JOIN {$postmeta_tbl} pm ON p.ID = pm.post_id
                INNER JOIN {$wpdb->prefix}term_relationships tr ON p.ID = tr.object_id
                WHERE 
                    {$post_type} p.post_status = 'publish' {$categories}
				{$instance['orderby']}
                LIMIT {$instance['post_number']}
        ";
        $results = $wpdb->get_results($popular);
        
        if ($results) {
            foreach ($results as $key => $value) {
                $aggregate = $this->get_aggregate_reviews($value->ID);
                $results[$key]->avg =  $aggregate->aggregate_rating;$results[$key]->date_start = '';
                $results[$key]->times = '';
                
                if(get_post_meta($value->ID, '_date_show',true)){ 
					$results[$key]->date_start = $this->get_dates($value->ID);
					$results[$key]->times = $this->get_times($value->ID);
                }
                $results[$key]->price = $this->get_course_price($value->ID);
            }
        }

        if (!$results)
            return array();
        return $results;
    }

    function get_aggregate_reviews($postid) {

        global $wpdb;

        $query = $wpdb->prepare("
			SELECT 
			COUNT(*) AS aggregate_count, AVG(tmp1.rating) AS aggregate_rating
			FROM 
            (
            	SELECT pm4.meta_value AS rating
                FROM {$wpdb->prefix}posts p1
                INNER JOIN {$wpdb->prefix}postmeta pm1 ON pm1.meta_key = 'wpcr3_enable' AND pm1.meta_value = '1' AND pm1.post_id = p1.id
                INNER JOIN {$wpdb->prefix}postmeta pm2 ON pm2.meta_key = 'wpcr3_review_post' AND pm2.meta_value = p1.id 
                INNER JOIN {$wpdb->prefix}posts p2 ON p2.id = pm2.post_id AND p2.post_status = 'publish' AND p2.post_type = 'wpcr3_review'
                INNER JOIN {$wpdb->prefix}postmeta pm4 ON pm4.post_id = p2.id AND pm4.meta_key = 'wpcr3_review_rating' AND pm4.meta_value IS NOT NULL AND pm4.meta_value != '0'
                WHERE
                p1.id = %d
                GROUP BY p2.id
            ) tmp1
		", intval($postid));

        $results = $wpdb->get_results($query);

        $rtn = new stdClass();

        if (count($results)) {

            $rtn->aggregate_count = $results[0]->aggregate_count;
            $rtn->aggregate_rating = ($results[0]->aggregate_rating)/5 * 100;
            if ($rtn->aggregate_count == 0) {
                $rtn->aggregate_rating = 0;
            }
        }

        return $rtn;
    }

    function getDataTabByCatId($instance) {
        $data = array();
		if (is_array($instance['categories'])){
			foreach ($instance['categories'] as $value) {
				$data[$value] = $this->getDataTabPopular($instance, $value);
			}
		} else {
			$data[$instance['categories']] = $this->getDataTabPopular($instance, $instance['categories']);
		}

        return $data;
    }

    function getDataTabPopular($instance, $category) {
        global $wpdb;
        $cats = array();
		if (count($instance['categories'])){
			if (is_array($instance['categories'])){
				foreach ($instance['categories'] as $cat){
					$cats[] = get_term_by('slug', $cat, 'category')->term_id;
				}
			} else {
				$cats[] = get_term_by('slug', $instance['categories'], 'category')->term_id;
			}
		}
		
        $categories = !empty($cats) ? 'and tr.term_taxonomy_id IN (' . get_term_by('slug', $category, 'category')->term_id . ')' : '';

        $post_number = !empty($instance['post_number']) ? 'LIMIT ' . $instance['post_number'] : '';
        $order = !empty($instance['orderby']) ? 'ORDER BY ' . $instance['orderby'] . ' ' . $instance['order'] : '';
        $post_type = !empty($instance['post_type']) ? 'p.post_type IN ("' . $instance['post_type'] . '") AND ' : '';
        $postmeta_tbl = $wpdb->prefix . 'postmeta';

        $popular = "
            SELECT DISTINCT p.ID,p.post_title,p.post_content,p.guid FROM {$wpdb->prefix}posts p
            INNER JOIN {$postmeta_tbl} pm ON p.ID = pm.post_id
            INNER JOIN {$wpdb->prefix}term_relationships tr ON p.ID = tr.object_id
            WHERE 
                    {$post_type} p.post_status = 'publish' {$categories}
            {$order}
            {$post_number}
        ";
        $results = $wpdb->get_results($popular);

        if ($results) {
            foreach ($results as $key => $value) {
                $aggregate = $this->get_aggregate_reviews($value->ID);
                $results[$key]->avg =  $aggregate->aggregate_rating;
                
                $results[$key]->date_start = '';
                $results[$key]->times = '';
                
                if(get_post_meta($value->ID, '_date_show',true)){ 
					$results[$key]->date_start = $this->get_dates($value->ID);
					$results[$key]->times = $this->get_times($value->ID);
                }
                if(get_post_meta($value->ID, 'video_type',true) && get_post_meta($value->ID, 'video_type',true) != 'none'){
					$ratio = get_post_meta($value->ID, 'aspect_ratio',true) ? get_post_meta($value->ID, 'aspect_ratio',true) : '16by9';
					$results[$key]->video = '<div class="videotrailer-course embed-responsive embed-responsive-' . $ratio . '">' . $this->get_video($value->ID) . '</div>';
                }
                $results[$key]->price = $this->get_course_price($value->ID);
            }
        }
        return $results;
    }
    
    function get_times($post_id){
	    $time_start = date_i18n(get_option('time_format'),strtotime(get_post_meta($post_id, '_nbt_time_start',true)));
        $time_end = date_i18n(get_option('time_format'),strtotime(get_post_meta($post_id, '_nbt_time_end',true)));
        if($time_start && $time_end){
			return $time_start. ' - ' .$time_end;
		}
        return '';
	}
    
    function get_dates($post_id){
        $date_start = get_post_meta($post_id, '_courses_date_start',true);
        if($date_start)
			return date_i18n(get_option('date_format'),strtotime($date_start));
        return '';
    }
    
    function get_video($post_id){
        $v_course = '';
		if ($v_type = get_post_meta($post_id,'video_type', true)):
			switch ($v_type):
				case 'external':
					if ($v_url = get_post_meta($post_id,'external_video_url', true)):
						$v_course = wp_oembed_get( $v_url );
					endif;
					break;
				case 'self':
					if ($v_url = get_post_meta($post_id,'self_hosted_video', true)):
						$att = explode('/', $v_url['mime_type']);
						if ($att[0]=='video'):
							$v_course = wp_video_shortcode(array('src'=>$v_url['url']));
						elseif ($att[0]=='audio'):
							$v_course = wp_audio_shortcode(array('src'=>$v_url['url']));
						endif;
					endif;
					break;
				case 'remote':
					if ($v_url = get_post_meta($post_id,'external_video_url', true)):
						$v_course = wp_oembed_get( $v_url );
					endif;
					break;
				case 'custom':
					if (get_post_meta($post_id,'custom_embed_code', true)):
						$v_course = get_post_meta($post_id,'custom_embed_code', true);
					endif;
					break;
			endswitch;
		endif;
        return $v_course;
    }
    
    function get_course_price($post_id){
        $course_data = get_post_meta($post_id, '_sfwd-courses', true);
        $price = $course_data['sfwd-courses_course_price'];
        if($price) 
            return $price;
        return '';
    }

}

siteorigin_widget_register('nbtsow-learndash-popular-widget', __FILE__, 'NBTSOW_Learndash_Popular_Widget');
