<?php
$popularpost = $this->getDataTabByCatId($instance);

$src = wp_get_attachment_image_src($image_file, $thumbsize);
$attr = array();
if( !empty($src) ) {
    $attr = array(
        'src' => $src[0],
    );
    if(!empty($src[1])) $attr['width'] = $src[1];
    if(!empty($src[2])) $attr['height'] = $src[2];
}

// Backward compability with WordPress before 4.4
if( function_exists('wp_get_attachment_image_srcset') && function_exists('wp_get_attachment_image_sizes') && !empty(wp_get_attachment_image_srcset($image_file, $size)) ) {
    $attr['srcset'] = wp_get_attachment_image_srcset($image_file, $thumbsize);
    $attr['sizes'] = wp_get_attachment_image_sizes($image_file, $thumbsize);
}
if( !empty($alt) ) $attr['alt'] = $alt;

if($popularpost){
    foreach ($popularpost as $key => $value){
        if(empty($value)){
            unset($popularpost[$key]);  
        }
    }
}

if ($priceshow){
    $options = get_option('sfwd_cpt_options');
    $currency = null;
    if(!is_null($options)){
        if(isset($options['modules']) && isset($options['modules']['sfwd-courses_options']) && isset($options['modules']['sfwd-courses_options']['sfwd-courses_paypal_currency'])){
            $currency = $options['modules']['sfwd-courses_options']['sfwd-courses_paypal_currency'];
        }
    }
    if(is_null($currency)){
        $currency = 'USD';
    }
}
?>
<div class="tab-style2">
<?php if($title || $description): ?>
    <div class="tabs-meta">
		<?php if($title): ?>
			<?php if($title_url):?>
				<a href="<?php echo $title_url;?>" class="title"><?php echo $title;?></a>
			<?php else:?>
				<span class="title"><?php echo $title;?></span>
			<?php endif; ?>
        <?php endif; ?> 
        <?php if($description):?>
            <p><?php echo $description;?></p>    
        <?php endif; ?>
    </div>
<?php endif; ?>
<ul class="nav nav-tabs unstyled">
    <?php 
    $i=0;
    foreach ($popularpost as $key => $value):
        $i++;
        $active = ($i==1)?"active":'';?>
        <li class="<?php echo $active;?>"><a data-toggle="tab" href="#nbt<?php echo $instance['_sow_form_id'] . $key; ?>"><?php echo get_term_by('slug', $key, 'category')->name; ?></a></li>
    <?php endforeach; ?>
</ul>

<div class="tab-content">
    <?php $i=0; foreach ($popularpost as $keys => $values): 
        $i++;
        $active = ($i==1)? "in active":'';?>
        <div id="nbt<?php echo $instance['_sow_form_id'] . $keys ?>" class="tab-pane fade <?php echo $active;?>">
            <div class="data-lg-<?php echo esc_attr($col['lg']);?> data-md-<?php echo esc_attr($col['md']);?> data-sm-<?php echo esc_attr($col['sm']);?> data-xs-<?php echo esc_attr($col['xs']);?>">
                <?php foreach ($values as $key=>$value):?>
                    <div class="nbtsow-course<?php echo ($key==0?' first':''); echo ($key==(count($values)-1)?' last':''); ?>">
                        <div class="nbtsow-course-thumb">
                            <div class="image-thumbnail-post">
								<?php if ($key==0 && $value->video):
									echo sprintf($value->video);
								else:
									if (has_post_thumbnail($value->ID)): ?>
										<a href="<?php echo get_permalink($value->ID);?>" title="<?php echo $value->post_title; ?>">
											<?php   echo get_the_post_thumbnail($value->ID,$thumbsize); ?>
										</a>
									<?php elseif ($image_file_show):?>
										<?php if($attr['src']):?>
											<a href="<?php echo get_permalink($value->ID);?>">
												<img <?php foreach($attr as $n => $v) echo $n.'="' . esc_attr($v) . '" ' ?> <?php if($full_width) echo 'class="nbtsow-image-full"';?> />
											</a>
										<?php endif;?>
									<?php endif;
								endif; ?>
                            </div>
                        </div>
                        <div class="nbtsow-course-details">
                            <?php if ($titleshow): ?>
                                <h4 class="nbtsow-course-title">
                                    <a href="<?php echo get_permalink($value->ID); ?>">
                                        <?php echo esc_html__($value->post_title); ?>
                                    </a>
                                </h4>
                            <?php endif; ?>
                            <?php if ($dateshow): ?>
                                <div class="nbtsow-course-date-time">
                                    <div class="nbtsow-course-date">
                                        <?php if($value->date_start):?>
                                            <span class="nbticon-calendar"><?php echo ($key==0 ? esc_html__('Start date: ') : '') . $value->date_start; ?></span>
                                        <?php else:?>
                                            <span class="nbticon-calendar"><?php echo esc_html__(get_the_date(get_option('date_format'), $value->ID)); ?></span>
                                        <?php endif;?>
                                    </div>
                                    <?php if ($value->times): ?>
                                        <div class="nbtsow-course-time">
                                            <span class="nbticon-schedule-button"><?php echo esc_html__($value->times) ?></span>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                            <?php if ($reviews): ?> 
                                <div class="wpcr3_rating_style1_base ">
                                    <div class="wpcr3_rating_style1_average" style="width:<?php if ($value->avg) {echo $value->avg;} else {echo '0'; } ?>%;">
                                    </div>
                                </div>
                            <?php endif;?>
                            <?php if ($priceshow):
                                $price = $value->price ? $value->price : esc_html__('Free');
                                if($price==''){
                                    $price .= esc_html__('Free');
                                }
                                if(is_numeric($price)){
                                    if($currency == "USD"){
                                        $price = '$' . $price;
                                    } else {
                                        $price .= ' ' . $currency;
                                    }
                                }
                                ?>
                                <div class="nbtsow-course-price <?php echo !empty($value->price) ? 'price_'.$currency : esc_attr__('Free')?>">
                                    <span><?php echo esc_html__($price) ?></span>
                                </div>
                            <?php endif; ?>
							<?php if($key==0 && $instance['post_type']=='sfwd-courses'): ?>
								<a class="nbtsow-course-join" href="<?php echo get_permalink($value->ID);?>" title="<?php echo $value->post_title; ?>"><?php echo esc_html__('Join now'); ?></a>
                            <?php endif; ?>
                            <?php if ($contentshow): ?>
                                <div class="nbtsow-course-content">
                                    <?php echo esc_html__(wp_trim_words($value->post_content, $contentcharlimit, '...')); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>        
                <?php endforeach;?>
            </div>
        </div>
<?php endforeach; ?>
</div>
</div>