<?php
$popularpost = $this->get_popularPost($instance);
?>
<?php if(!empty($title)) {
    echo '<h3 class="nbtsow-widget-title">' . $title . '</h3>';
}?>
<ul class="nbtsow-blog-posts clear nbtsow-blog-posts-default unstyled">
<?php
    foreach ($popularpost as $key => $value):
?>
    <li class="nbtsow-course-post">
            <?php if (has_post_thumbnail( $value->ID ) ):?>
                <div class="nbtsow-course-thumb">
                    <div class="nbtsow-blog-thumb">
                        <a href="<?php echo get_permalink($value->ID);?>">
                                <?php echo get_the_post_thumbnail($value->ID, $thumbsize );?>
                        </a>
                    </div>
                </div>
            <?php endif;?>
            <div class="nbtsow-course-details">
                <?php if ($titleshow): ?>
                    <h4 class="nbtsow-course-title">
                        <a href="<?php echo get_permalink($value->ID);?>">
                            <?php echo $value->post_title; ?>
                        </a>
                    </h4>
                <?php endif; ?>
                <?php if ($contentshow): ?>
                    <div class="nbtsow-course-content">
                        <?php echo esc_html__(wp_trim_words( $value->post_content, $contentcharlimit, '...' )); ?>
                    </div>
                <?php endif; ?>
                <?php if($dateshow):?>
                    <div class="nbtsow-course-date">
                        <span><?php echo get_the_date(date_format(),$value->ID );?></span>
                    </div>
                <?php endif;?>
                <?php if($reviews):?>   
                    <div class="wpcr3_rating_style1_base ">
                        <div class="wpcr3_rating_style1_average" style="width:<?php if($value->avg){ echo $value->avg;}else{ echo '0'; }?>%;"></div>
                    </div>  
                <?php endif;?>
            </div>
        </li>
    <?php endforeach; ?>
</ul>
<?php wp_reset_postdata();?>