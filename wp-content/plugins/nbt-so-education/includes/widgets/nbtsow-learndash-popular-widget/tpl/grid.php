<?php
$popularpost = $this->get_popularPost($instance);
?>
<?php if(!empty($title)) {
    echo '<h3 class="nbtsow-widget-title">' . $title . '</h3>';
}?>
<div class="nbtsow-blog-posts clear nbtsow-posts-grid">
    <div class="data-lg-<?php echo $col['lg'];?> data-md-<?php echo $col['md'];?> data-sm-<?php echo $col['sm'];?> data-xs-<?php echo $col['xs'];?>">
    <?php
    foreach ($popularpost as $key => $value):
    ?>
        <div class="nbtsow-course">
                <?php if (has_post_thumbnail( $value->ID ) ):?>
                    <div class="nbtsow-course-thumb">
                        <div class="nbtsow-blog-thumb">
                            <a href="<?php echo get_permalink($value->ID);?>">
                                <?php echo get_the_post_thumbnail($value->ID, $thumbsize );?>
                            </a>
                        </div>
                    </div>
                <?php endif;?>
				<?php if ($titleshow || $contentshow || $dateshow || $reviews): ?>
					<div class="nbtsow-course-details">
						<?php if ($titleshow): ?>
							<h4 class="nbtsow-course-title">
								<a href="<?php echo get_permalink($value->ID);?>">
									<?php echo $value->post_title; ?>
								</a>
							</h4>
						<?php endif; ?>
						<?php if ($contentshow): ?>
							<div class="nbtsow-course-content">
								<?php echo esc_html__(wp_trim_words( $value->post_content, $contentcharlimit, '...' )); ?>
							</div>
						<?php endif; ?>
						<?php if($dateshow):?>
							<div class="nbtsow-course-date-time">
								<div class="nbtsow-course-date">
									<?php if($value->date_start):?>
										<span class="nbticon-calendar"><?php echo esc_html__($value->date_start); ?></span>
									<?php else:?>
										<span class="nbticon-calendar"><?php echo esc_html__(get_the_date(get_option('date_format'), $value->ID)); ?></span>
									<?php endif;?>
								</div>
								<?php if ($value->times): ?>
									<div class="nbtsow-course-time">
										<span class="nbticon-schedule-button"><?php echo esc_html__($value->times) ?></span>
									</div>
								<?php endif; ?>
							</div>
						<?php endif;?>
						<?php if($reviews):?>   
							<div class="wpcr3_rating_style1_base ">
								<div class="wpcr3_rating_style1_average" style="width:<?php if($value->avg){ echo $value->avg;}else{ echo '0'; }?>%;"></div>
							</div>
						<?php endif;?>
					</div>
				<?php endif;?>
            </div>
        <?php endforeach; ?>
    </div>
	<div class="clearfix"></div>
	<?php if (!empty($button) && (!empty($button['text']) || (!empty($button['icon']) && (!empty($button['icon']['icon']) || !empty($button['icon']['icon_media']))))):?>
		<div class="nbtsow-course-btn">
			<a <?php echo ($button['href'] ? (' href="'.sow_esc_url($button['href']).'"') : ''); ?>>
				<?php foreach ($button['order'] as $btn):
					switch ($btn):
						case 'icon':
							if( !empty($button['icon']['icon_media']) ):
								$attachment = wp_get_attachment_image_src($button['icon']['icon_media']);
								if(!empty($attachment)):
									$btn_i_styles[] = 'background-image: url(' . sow_esc_url($attachment[0]) . ')';
									$btn_i_styles[] = 'width: ' . intval($button['icon']['icon_size']).'px';
									$btn_i_styles[] = 'height: ' . intval($button['icon']['icon_size']).'px';
									?><span class="nbtsow-icon-image" style="<?php echo implode('; ', $btn_i_styles) ?>"></span><?php
								endif;
							elseif(!empty($button['icon']['icon'])):
								$btn_i_styles = array();
								if(!empty($icon['icon_size'])) 
									$btn_i_styles[] = 'font-size: ' . intval($icon['icon_size']).'px';
								if(!empty($icon['icon_color'])) 
									$btn_i_styles[] = 'color: ' . $icon['icon_color'];
								echo siteorigin_widget_get_icon($button['icon']['icon'], $btn_i_styles);
							endif;
							break;
						case 'text':
							echo $button['text'];
							break;
					endswitch;
				endforeach; ?>
			</a>
		</div>
	<?php endif; ?>
</div>
<?php wp_reset_postdata(); ?>