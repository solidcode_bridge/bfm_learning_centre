
<?php if (!empty($instance['title'])) echo $args['before_title'] . esc_html($instance['title']) . $args['after_title'] ?>
    <span><?php echo esc_html($description); ?></span>
<ul class="unstyled">
    <?php 
    $depth = 1;
    if($show_child) $depth = 0;
    wp_list_categories( array(
        'orderby' => 'ID',
        'include' => $categories,
        'taxonomy' => 'category',
        'title_li'            => '',
        'orderby'   => $orderby,
        'order' => $order,
        'depth' => $depth
    ) ); ?> 
</ul>
