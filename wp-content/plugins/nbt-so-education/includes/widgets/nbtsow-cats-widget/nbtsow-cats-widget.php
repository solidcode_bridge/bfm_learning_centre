<?php
/*
  Widget Name: NetBaseTeam Category Widget
  Description: NetBaseTeam Category Widget.
  Author: NetBaseTeam
  Author URI: https://netbaseteam.com
 */

class NBTSOW_Cats_Widget extends SiteOrigin_Widget {

    function __construct() {
        parent::__construct(
                'nbtsow-cats-widget', esc_html__('NetBaseTeam Category Widget', 'nbtsow'), array(
            'description' => esc_html__('NetBaseTeam Category Widget.', 'nbtsow'),
                ), array(), array(
                ), false, plugin_dir_path(__FILE__)
        );
    }

    function get_widget_form() {
		$term_names = get_terms('category', 'orderby=name&fields=id=>name&hide_empty=0');
		$term_slugs = get_terms('category', 'orderby=name&fields=id=>slug&hide_empty=0');
		$categories = array();
		foreach ($term_slugs as $key => $term_slug){
			$categories[$term_slug] = $term_names[$key];
		}
        $taxonomies = wp_list_pluck(get_taxonomies(array(), 'objects'), 'label');

        return array(
            'title' => array(
                'type' => 'text',
                'label' => esc_html__('Widget title', 'nbtsow'),
            ),
            'description' => array(
                'type' => 'textarea',
                'label' => esc_html__('Description', 'nbtsow'),
            ),
            'categories' => array(
                'type' => 'select',
                'label' => esc_html__('Select category', 'nbtsow'),
                'multiple' => true,
                'options' => $categories
            ),
            'show_child' => array(
                'type' => 'checkbox',
                'label' => esc_html__('Show category child', 'nbtsow'),
                'default' => true,
            ),
            'taxonomy' => array(
                'type' => 'select',
                'label' => esc_html__('Taxonomy type', 'nbtsow'),
                'options' => $taxonomies,
            ),
            'orderby' => array(
                'type' => 'select',
                'label' => esc_html__('Order by', 'nbtsow'),
                'options' => array(
                    'none' => esc_html__('No order', 'nbtsow'),
                    'ID' => esc_html__('ID', 'nbtsow'),
                    'author' => esc_html__('Author', 'nbtsow'),
                    'title' => esc_html__('Title', 'nbtsow'),
                    'name' => esc_html__('Name', 'nbtsow'),
                    'date' => esc_html__('Published date', 'nbtsow'),
                    'modified' => esc_html__('Modified date', 'nbtsow'),
                    'parent' => esc_html__('By parent', 'nbtsow'),
                    'rand' => esc_html__('Random order', 'nbtsow'),
                    'comment_count' => __('Comment count', 'nbtsow'),
                    'menu_order' => esc_html__('Menu order', 'nbtsow'),
                    'meta_value' => esc_html__('By meta value', 'nbtsow'),
                    'meta_value_num' => esc_html__('By numeric meta value', 'nbtsow'),
                    'post__in' => esc_html__('By include order', 'nbtsow'),
                )
            ),
            'order' => array(
                'type' => 'select',
                'label' => esc_html__('Order', 'nbtsow'),
                'options' => array(
                    'none' => esc_html__('No order', 'nbtsow'),
                    'ASC' => esc_html__('ASC', 'nbtsow'),
                    'DESC' => esc_html__('DESC', 'nbtsow'),
                )
            )
        );
    }

    function get_template_variables($instance, $args) {
		$categories = array();
		if (count($instance['categories']) > 1){
			foreach ($instance['categories'] as $category){
				$categories[] = get_term_by('slug', $category, 'category')->term_id;
			}
		} else {
			$category = get_term_by('slug', $instance['categories'], 'category')->term_id;
		}
        return array(
            'title' => $instance['title'],
            'description' => $instance['description'],
            'categories' => count($categories) ? $categories : $category,
            'taxonomy' => $instance['taxonomy'],
            'orderby' => $instance['orderby'],
            'order' => $instance['order'],
            'show_child' => $instance['show_child']
        );
    }

}

siteorigin_widget_register('nbtsow-cats-widget', __FILE__, 'NBTSOW_Cats_Widget');
