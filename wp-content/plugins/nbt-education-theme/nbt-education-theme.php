<?php
/**
 * The plugin add Service & Team custom post type
 *
 *
 * @link              http://www.netbaseteam.com
 * @since             1.8.0
 * @package           nbt-education-theme
 *
 * @wordpress-plugin
 * Plugin Name:       Netbase Education Theme
 * Plugin URI:        netbaseteam.com
 * Description:       Plugin made for LearnDash Date Time by Netbaseteam.com.
 * Version:           1.8.0
 * Author:            NetbaseTeam
 * Author URI:        http://www.netbaseteam.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       nbt-education-theme
 * Domain Path:       /languages
 */
// If this file is called directly, abort.
defined('ABSPATH') or die('No script kiddies please!');

class NbtEducationTheme {

    public function __construct() {
        add_action('init', array($this, 'service_post_type'));
		add_action('init', array($this, 'service_taxonomy'));
		add_action('init', array($this, 'team_post_type'));
		add_action('init', array($this, 'team_taxonomy'));
		remove_shortcode('gallery');
		add_shortcode( 'gallery', array($this, 'education_gallery_shortcode'));
		add_action('wp_head', array( $this, 'nbtSetViews'));
		add_filter('megamenu_themes', array( $this, 'megamenu_add_theme_education_1492489384'));
    }
    
	// Register Service Post Type
	function service_post_type() {

		$labels = array(
			'name'                  => esc_html_x( 'Services', 'Post Type General Name', 'education' ),
			'singular_name'         => esc_html_x( 'Service', 'Post Type Singular Name', 'education' ),
			'menu_name'             => esc_html__( 'Services', 'education' ),
			'name_admin_bar'        => esc_html__( 'Service', 'education' ),
			'archives'              => esc_html__( 'Services', 'education' ),
			'parent_item_colon'     => esc_html__( 'Parent Service:', 'education' ),
			'all_items'             => esc_html__( 'All Services', 'education' ),
			'add_new_item'          => esc_html__( 'Add New Service', 'education' ),
			'add_new'               => esc_html__( 'Add Service', 'education' ),
			'new_item'              => esc_html__( 'New Service', 'education' ),
			'edit_item'             => esc_html__( 'Edit Service', 'education' ),
			'update_item'           => esc_html__( 'Update Service', 'education' ),
			'view_item'             => esc_html__( 'View Service', 'education' ),
			'search_items'          => esc_html__( 'Search Service', 'education' ),
			'not_found'             => esc_html__( 'Not found', 'education' ),
			'not_found_in_trash'    => esc_html__( 'Not found in Trash', 'education' ),
			'featured_image'        => esc_html__( 'Featured Image', 'education' ),
			'set_featured_image'    => esc_html__( 'Set featured image', 'education' ),
			'remove_featured_image' => esc_html__( 'Remove featured image', 'education' ),
			'use_featured_image'    => esc_html__( 'Use as featured image', 'education' ),
			'insert_into_item'      => esc_html__( 'Insert into service', 'education' ),
			'uploaded_to_this_item' => esc_html__( 'Uploaded to this service', 'education' ),
			'items_list'            => esc_html__( 'Services list', 'education' ),
			'items_list_navigation' => esc_html__( 'Services list navigation', 'education' ),
			'filter_items_list'     => esc_html__( 'Filter services list', 'education' ),
		);
		$args = array(
			'label'                 => esc_html__( 'Service', 'education' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', ),
			'taxonomies'            => array( 'category-service', 'post_tag' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-clock',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => true,		
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'capability_type'       => 'post',
		);
		register_post_type( 'service', $args );

	}

	// Register Service Taxonomy
	function service_taxonomy() {

		$labels = array(
			'name'                       => esc_html_x( 'Services', 'Taxonomy General Name', 'education' ),
			'singular_name'              => esc_html_x( 'Service', 'Taxonomy Singular Name', 'education' ),
			'menu_name'                  => esc_html__( 'Service Category', 'education' ),
			'all_items'                  => esc_html__( 'All Services Catogories', 'education' ),
			'parent_item'                => esc_html__( 'Parent Service Category', 'education' ),
			'parent_item_colon'          => esc_html__( 'Parent Service Category:', 'education' ),
			'new_item_name'              => esc_html__( 'New Service Category Name', 'education' ),
			'add_new_item'               => esc_html__( 'Add New Service Category', 'education' ),
			'edit_item'                  => esc_html__( 'Edit Service Category', 'education' ),
			'update_item'                => esc_html__( 'Update Service Category', 'education' ),
			'view_item'                  => esc_html__( 'View Service Category', 'education' ),
			'separate_items_with_commas' => esc_html__( 'Separate Service Categories with commas', 'education' ),
			'add_or_remove_items'        => esc_html__( 'Add or remove Service Categories', 'education' ),
			'choose_from_most_used'      => esc_html__( 'Choose from the most used', 'education' ),
			'popular_items'              => esc_html__( 'Popular Service Categories', 'education' ),
			'search_items'               => esc_html__( 'Search Service Categories', 'education' ),
			'not_found'                  => esc_html__( 'Not Found', 'education' ),
			'no_terms'                   => esc_html__( 'No Service Categories', 'education' ),
			'items_list'                 => esc_html__( 'Service Categories list', 'education' ),
			'items_list_navigation'      => esc_html__( 'Service Categories list navigation', 'education' ),
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
		);
		register_taxonomy( 'category-service', array( 'service' ), $args );
	}

	// Register Team Post Type
	function team_post_type() {

		$labels = array(
			'name'                  => esc_html_x( 'Teams', 'Post Type General Name', 'education' ),
			'singular_name'         => esc_html_x( 'Team', 'Post Type Singular Name', 'education' ),
			'menu_name'             => esc_html__( 'Teams', 'education' ),
			'name_admin_bar'        => esc_html__( 'Team', 'education' ),
			'archives'              => esc_html__( 'Teams', 'education' ),
			'parent_item_colon'     => esc_html__( 'Parent Team:', 'education' ),
			'all_items'             => esc_html__( 'All Teams', 'education' ),
			'add_new_item'          => esc_html__( 'Add New Team', 'education' ),
			'add_new'               => esc_html__( 'Add Team', 'education' ),
			'new_item'              => esc_html__( 'New Team', 'education' ),
			'edit_item'             => esc_html__( 'Edit Team', 'education' ),
			'update_item'           => esc_html__( 'Update Team', 'education' ),
			'view_item'             => esc_html__( 'View Team', 'education' ),
			'search_items'          => esc_html__( 'Search Team', 'education' ),
			'not_found'             => esc_html__( 'Not found', 'education' ),
			'not_found_in_trash'    => esc_html__( 'Not found in Trash', 'education' ),
			'featured_image'        => esc_html__( 'Featured Image', 'education' ),
			'set_featured_image'    => esc_html__( 'Set featured image', 'education' ),
			'remove_featured_image' => esc_html__( 'Remove featured image', 'education' ),
			'use_featured_image'    => esc_html__( 'Use as featured image', 'education' ),
			'insert_into_item'      => esc_html__( 'Insert into team', 'education' ),
			'uploaded_to_this_item' => esc_html__( 'Uploaded to this team', 'education' ),
			'items_list'            => esc_html__( 'Teams list', 'education' ),
			'items_list_navigation' => esc_html__( 'Teams list navigation', 'education' ),
			'filter_items_list'     => esc_html__( 'Filter teams list', 'education' ),
		);
		$args = array(
			'label'                 => esc_html__( 'Team', 'education' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', ),
			'taxonomies'            => array( 'category-team', 'post_tag' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-groups',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => true,		
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'capability_type'       => 'post',
		);
		register_post_type( 'team', $args );

	}

	// Register Team Taxonomy
	function team_taxonomy() {

		$labels = array(
			'name'                       => esc_html_x( 'Teams', 'Taxonomy General Name', 'education' ),
			'singular_name'              => esc_html_x( 'Team', 'Taxonomy Singular Name', 'education' ),
			'menu_name'                  => esc_html__( 'Team Category', 'education' ),
			'all_items'                  => esc_html__( 'All Teams Catogories', 'education' ),
			'parent_item'                => esc_html__( 'Parent Team Category', 'education' ),
			'parent_item_colon'          => esc_html__( 'Parent Team Category:', 'education' ),
			'new_item_name'              => esc_html__( 'New Team Category Name', 'education' ),
			'add_new_item'               => esc_html__( 'Add New Team Category', 'education' ),
			'edit_item'                  => esc_html__( 'Edit Team Category', 'education' ),
			'update_item'                => esc_html__( 'Update Team Category', 'education' ),
			'view_item'                  => esc_html__( 'View Team Category', 'education' ),
			'separate_items_with_commas' => esc_html__( 'Separate Team Categories with commas', 'education' ),
			'add_or_remove_items'        => esc_html__( 'Add or remove Team Categories', 'education' ),
			'choose_from_most_used'      => esc_html__( 'Choose from the most used', 'education' ),
			'popular_items'              => esc_html__( 'Popular Team Categories', 'education' ),
			'search_items'               => esc_html__( 'Search Team Categories', 'education' ),
			'not_found'                  => esc_html__( 'Not Found', 'education' ),
			'no_terms'                   => esc_html__( 'No Team Categories', 'education' ),
			'items_list'                 => esc_html__( 'Team Categories list', 'education' ),
			'items_list_navigation'      => esc_html__( 'Team Categories list navigation', 'education' ),
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
		);
		register_taxonomy( 'category-team', array( 'team' ), $args );
	}
	
	function education_gallery_shortcode( $attr ) {

		$post = get_post();

		static $instance = 0;
		$instance++;

		if ( ! empty( $attr['ids'] ) ) {
			// 'ids' is explicitly ordered, unless you specify otherwise.
			if ( empty( $attr['orderby'] ) ) {
				$attr['orderby'] = 'post__in';
			}
			$attr['include'] = $attr['ids'];
		}
		$output = apply_filters( 'post_gallery', '', $attr, $instance );
		if ( $output != '' ) {
			return $output;
		}

		$html5 = current_theme_supports( 'html5', 'gallery' );
		$atts = shortcode_atts( array(
			'order'      => 'ASC',
			'orderby'    => 'menu_order ID',
			'id'         => $post ? $post->ID : 0,
			'itemtag'    => $html5 ? 'figure'     : 'dl',
			'icontag'    => $html5 ? 'div'        : 'dt',
			'captiontag' => $html5 ? 'figcaption' : 'dd',
			'columns'    => 3,
			'size'       => 'thumbnail',
			'include'    => '',
			'exclude'    => '',
			'link'       => '',
			'class'		 =>''
		), $attr, 'gallery' );

		$id = intval( $atts['id'] );

		if ( ! empty( $atts['include'] ) ) {
			$_attachments = get_posts( array( 'include' => $atts['include'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );

			$attachments = array();
			foreach ( $_attachments as $key => $val ) {
				$attachments[$val->ID] = $_attachments[$key];
			}
		} elseif ( ! empty( $atts['exclude'] ) ) {
			$attachments = get_children( array( 'post_parent' => $id, 'exclude' => $atts['exclude'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
		} else {
			$attachments = get_children( array( 'post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
		}

		if ( empty( $attachments ) ) {
			return '';
		}

		if ( is_feed() ) {
			$output = "\n";
			foreach ( $attachments as $att_id => $attachment ) {
				$output .= wp_get_attachment_link( $att_id, $atts['size'], true ) . "\n";
			}
			return $output;
		}

		$itemtag = tag_escape( $atts['itemtag'] );
		$captiontag = tag_escape( $atts['captiontag'] );
		$icontag = tag_escape( $atts['icontag'] );
		$valid_tags = wp_kses_allowed_html( 'post' );
		if ( ! isset( $valid_tags[ $itemtag ] ) ) {
			$itemtag = 'dl';
		}
		if ( ! isset( $valid_tags[ $captiontag ] ) ) {
			$captiontag = 'dd';
		}
		if ( ! isset( $valid_tags[ $icontag ] ) ) {
			$icontag = 'dt';
		}

		$columns = intval( $atts['columns'] );
		$itemwidth = $columns > 0 ? floor(100/$columns) : 100;
		$float = is_rtl() ? 'right' : 'left';

		$selector = "gallery-{$instance}";

		$gallery_style = '';
		if ( apply_filters( 'use_default_gallery_style', ! $html5 ) ) {
			$gallery_style = "
			<style type='text/css'>
				#{$selector} {
					margin: auto;
				}
				#{$selector} .gallery-item {
					float: {$float};
					margin-top: 10px;
					text-align: center;
					width: {$itemwidth}%;
				}
				#{$selector} img {
					border: 2px solid #cfcfcf;
				}
				#{$selector} .gallery-caption {
					margin-left: 0;
				}
				/* see gallery_shortcode() in wp-includes/media.php */
			</style>\n\t\t";
		}

		$size_class = sanitize_html_class( $atts['size'] );
		$lnk_class = sanitize_html_class( $atts['link'] );
		$gallery_div = "<div id='$selector' class='gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class} gallery-link-{$lnk_class}'>";
		$output = apply_filters( 'gallery_style', $gallery_style . $gallery_div );

		$i = 0;
		foreach ( $attachments as $id => $attachment ) {

			$attr = ( trim( $attachment->post_excerpt ) ) ? array( 'aria-describedby' => "$selector-$id" ) : '';
			if ( ! empty( $atts['link'] ) && 'file' === $atts['link'] ) {
				$image_output = wp_get_attachment_link( $id, $atts['size'], false, false, false, $attr );
			} elseif ( ! empty( $atts['link'] ) && 'none' === $atts['link'] ) {
				$image_output = wp_get_attachment_image( $id, $atts['size'], false, $attr );
			} else {
				$image_output = wp_get_attachment_link( $id, $atts['size'], true, false, false, $attr );
			}
			$image_meta  = wp_get_attachment_metadata( $id );

			$orientation = '';
			if ( isset( $image_meta['height'], $image_meta['width'] ) ) {
				$orientation = ( $image_meta['height'] > $image_meta['width'] ) ? 'portrait' : 'landscape';
			}
			$output .= "<{$itemtag} class='gallery-item'>";
					$class =$atts['class'];
			$output .= "
				<{$icontag} class='gallery-icon {$orientation} {$class}'>
					$image_output
				</{$icontag}>";
			if ( $captiontag && trim($attachment->post_excerpt) ) {
				$output .= "
					<{$captiontag} class='wp-caption-text gallery-caption' id='$selector-$id'>
					" . wptexturize($attachment->post_excerpt) . "
					</{$captiontag}>";
			}
			$output .= "</{$itemtag}>";
			if ( ! $html5 && $columns > 0 && ++$i % $columns == 0 ) {
				$output .= '<br style="clear: both" />';
			}
		}

		if ( ! $html5 && $columns > 0 && $i % $columns !== 0 ) {
			$output .= "
				<br style='clear: both' />";
		}

		$output .= "
			</div>\n";

		return $output;
	}
	
	function education_get_reviews($postid) {
		$rtn = new stdClass();
		if (!in_array('wp-customer-reviews/wp-customer-reviews-3.php', apply_filters('active_plugins', get_option('active_plugins')))) {
			deactivate_plugins(basename(__FILE__)); // Deactivate our plugin
			
		} else {
			global $wpdb;
			$query = $wpdb->prepare("
				SELECT 
				COUNT(*) AS aggregate_count, AVG(tmp1.rating) AS aggregate_rating
				FROM 
					(
						SELECT pm4.meta_value AS rating
						FROM {$wpdb->prefix}posts p1
						INNER JOIN {$wpdb->prefix}postmeta pm1 ON pm1.meta_key = 'wpcr3_enable' AND pm1.meta_value = '1' AND pm1.post_id = p1.id
						INNER JOIN {$wpdb->prefix}postmeta pm2 ON pm2.meta_key = 'wpcr3_review_post' AND pm2.meta_value = p1.id 
						INNER JOIN {$wpdb->prefix}posts p2 ON p2.id = pm2.post_id AND p2.post_status = 'publish' AND p2.post_type = 'wpcr3_review'
						INNER JOIN {$wpdb->prefix}postmeta pm4 ON pm4.post_id = p2.id AND pm4.meta_key = 'wpcr3_review_rating' AND pm4.meta_value IS NOT NULL AND pm4.meta_value != '0'
						WHERE
						p1.id = %d
						GROUP BY p2.id
					) tmp1
						", intval($postid));

			$results = $wpdb->get_results($query);
			if (count($results)) {
				$rtn->aggregate_count = $results[0]->aggregate_count;
				$rtn->aggregate_rating = ($results[0]->aggregate_rating) / 5 * 100;
				if ($rtn->aggregate_count == 0) {
					$rtn->aggregate_rating = 0;
				}
			}
			return $rtn;
		}
	}
	function nbtSetViews(){
		// check is single set view
		if(is_single() && !is_page()){
			global $wpdb, $post;
			$postmeta_tbl = $wpdb->prefix . 'postmeta';

			$wpdb->flush();
			$data = $wpdb->get_row("SELECT * FROM {$postmeta_tbl} WHERE post_id = {$post->ID} AND meta_key = '_nbt_view'", ARRAY_A );
			if(!is_null($data)){
				$new_views = $data['meta_value'] + 1;
				$wpdb->query("UPDATE {$postmeta_tbl} SET meta_value={$new_views} WHERE post_id = {$post->ID} AND meta_key = '_nbt_view';");
				$wpdb->flush();
			}else{
				$wpdb->query("INSERT INTO {$postmeta_tbl} (post_id, meta_key, meta_value) VALUES ('$post->ID', '_nbt_view', 1);");
				$wpdb->flush();
			}
		}
	}
	function megamenu_add_theme_education_1492489384($themes) {
		$themes["education_1492489384"] = array(
			'title' => 'Education Blue',
			'container_background_from' => 'rgba(0, 0, 0, 0)',
			'container_background_to' => 'rgba(0, 0, 0, 0)',
			'arrow_up' => 'disabled',
			'arrow_down' => 'disabled',
			'arrow_left' => 'disabled',
			'arrow_right' => 'disabled',
			'menu_item_align' => 'right',
			'menu_item_background_hover_from' => 'rgba(0, 0, 0, 0)',
			'menu_item_background_hover_to' => 'rgba(0, 0, 0, 0)',
			'menu_item_link_font' => 'Roboto',
			'menu_item_link_height' => '35px',
			'menu_item_link_color' => 'rgb(0, 0, 0)',
			'menu_item_link_text_transform' => 'uppercase',
			'menu_item_link_text_align' => 'center',
			'menu_item_link_color_hover' => 'rgb(28, 129, 197)',
			'menu_item_link_padding_left' => '5px',
			'menu_item_link_padding_right' => '5px',
			'menu_item_border_color' => 'rgba(255, 255, 255, 0)',
			'menu_item_border_bottom' => '2px',
			'menu_item_border_color_hover' => 'rgb(28, 129, 197)',
			'menu_item_highlight_current' => 'on',
			'panel_background_from' => 'rgb(255, 255, 255)',
			'panel_background_to' => 'rgb(255, 255, 255)',
			'panel_border_color' => 'rgb(28, 129, 197)',
			'panel_border_top' => '2px',
			'panel_header_color' => 'rgb(0, 0, 0)',
			'panel_header_font' => 'Roboto',
			'panel_header_padding_bottom' => '10px',
			'panel_header_margin_bottom' => '10px',
			'panel_header_border_color' => 'rgb(232, 232, 232)',
			'panel_header_border_bottom' => '1px',
			'panel_padding_left' => '35px',
			'panel_padding_right' => '35px',
			'panel_padding_top' => '30px',
			'panel_padding_bottom' => '20px',
			'panel_font_size' => '16px',
			'panel_font_color' => 'rgb(0, 0, 0)',
			'panel_font_family' => 'Roboto',
			'panel_second_level_font_color' => 'rgb(0, 0, 0)',
			'panel_second_level_font_color_hover' => 'rgb(28, 129, 197)',
			'panel_second_level_text_transform' => 'uppercase',
			'panel_second_level_font' => 'Roboto',
			'panel_second_level_font_size' => '15px',
			'panel_second_level_font_weight' => 'bold',
			'panel_second_level_font_weight_hover' => 'bold',
			'panel_second_level_text_decoration' => 'none',
			'panel_second_level_text_decoration_hover' => 'none',
			'panel_second_level_padding_bottom' => '5px',
			'panel_second_level_margin_bottom' => '5px',
			'panel_second_level_border_color' => '#555',
			'panel_third_level_font_color' => 'rgb(0, 0, 0)',
			'panel_third_level_font_color_hover' => 'rgb(28, 129, 197)',
			'panel_third_level_font' => 'Roboto',
			'panel_third_level_font_size' => '13px',
			'panel_third_level_padding_top' => '5px',
			'panel_third_level_padding_bottom' => '5px',
			'flyout_width' => '250px',
			'flyout_menu_background_from' => 'rgb(255, 255, 255)',
			'flyout_menu_background_to' => 'rgb(255, 255, 255)',
			'flyout_border_color' => 'rgb(28, 129, 197)',
			'flyout_border_top' => '2px',
			'flyout_menu_item_divider' => 'on',
			'flyout_menu_item_divider_color' => 'rgba(104, 104, 104, 0.1)',
			'flyout_padding_top' => '13px',
			'flyout_link_padding_left' => '30px',
			'flyout_link_padding_right' => '30px',
			'flyout_link_padding_top' => '10px',
			'flyout_link_padding_bottom' => '10px',
			'flyout_link_height' => '30px',
			'flyout_background_from' => 'rgba(0, 0, 0, 0)',
			'flyout_background_to' => 'rgba(0, 0, 0, 0)',
			'flyout_background_hover_from' => 'rgb(248, 248, 248)',
			'flyout_background_hover_to' => 'rgb(248, 248, 248)',
			'flyout_link_size' => '14px',
			'flyout_link_color' => 'rgb(0, 0, 0)',
			'flyout_link_color_hover' => 'rgb(28, 129, 197)',
			'flyout_link_family' => 'Roboto',
			'shadow' => 'on',
			'shadow_vertical' => '3px',
			'shadow_color' => 'rgba(0, 0, 0, 0.3)',
			'toggle_background_from' => '#222',
			'toggle_background_to' => '#222',
			'toggle_font_color' => 'rgb(0, 0, 0)',
			'mobile_background_from' => '#222',
			'mobile_background_to' => '#222',
			'custom_css' => '/** Push menu onto new line **/
			#{$wrap} {
				clear: both;
			}
			#{$wrap} #{$menu} > li.mega-menu-item > a.mega-menu-link, #{$wrap} #{$menu} > li.mega-menu-item > a.mega-menu-link:hover, #{$wrap} #{$menu} > li.mega-menu-item > a.mega-menu-link:focus, #{$wrap} #{$menu} > li.mega-menu-item.mega-current-menu-item > a.mega-menu-link, #{$wrap} #{$menu} > li.mega-menu-item.mega-current-menu-ancestor > a.mega-menu-link,
			#{$wrap} #{$menu} > li.mega-menu-item.mega-toggle-on > a.mega-menu-link, #{$wrap} #{$menu} > li.mega-menu-item > a.mega-menu-link:hover, #{$wrap} #{$menu} > li.mega-menu-item > a.mega-menu-link:focus{
				font-weight: 500;
			}',
		);
		$themes["education_1492745921"] = array(
			'title' => 'Education Orange',
			'container_background_from' => 'rgba(0, 0, 0, 0)',
			'container_background_to' => 'rgba(0, 0, 0, 0)',
			'arrow_up' => 'disabled',
			'arrow_down' => 'disabled',
			'arrow_left' => 'disabled',
			'arrow_right' => 'disabled',
			'menu_item_align' => 'right',
			'menu_item_background_hover_from' => 'rgba(0, 0, 0, 0)',
			'menu_item_background_hover_to' => 'rgba(0, 0, 0, 0)',
			'menu_item_link_font' => 'Roboto',
			'menu_item_link_height' => '35px',
			'menu_item_link_color' => 'rgb(0, 0, 0)',
			'menu_item_link_text_transform' => 'uppercase',
			'menu_item_link_text_align' => 'center',
			'menu_item_link_color_hover' => 'rgb(237, 102, 57)',
			'menu_item_link_padding_left' => '5px',
			'menu_item_link_padding_right' => '5px',
			'menu_item_border_color' => 'rgba(255, 255, 255, 0)',
			'menu_item_border_bottom' => '2px',
			'menu_item_border_color_hover' => 'rgb(237, 102, 57)',
			'menu_item_highlight_current' => 'on',
			'panel_background_from' => 'rgb(255, 255, 255)',
			'panel_background_to' => 'rgb(255, 255, 255)',
			'panel_border_color' => 'rgb(237, 102, 57)',
			'panel_border_top' => '2px',
			'panel_header_color' => 'rgb(0, 0, 0)',
			'panel_header_font' => 'Roboto',
			'panel_header_padding_bottom' => '10px',
			'panel_header_margin_bottom' => '10px',
			'panel_header_border_color' => 'rgb(232, 232, 232)',
			'panel_header_border_bottom' => '1px',
			'panel_padding_left' => '35px',
			'panel_padding_right' => '35px',
			'panel_padding_top' => '30px',
			'panel_padding_bottom' => '20px',
			'panel_font_size' => '16px',
			'panel_font_color' => 'rgb(0, 0, 0)',
			'panel_font_family' => 'Roboto',
			'panel_second_level_font_color' => 'rgb(0, 0, 0)',
			'panel_second_level_font_color_hover' => 'rgb(237, 102, 57)',
			'panel_second_level_text_transform' => 'uppercase',
			'panel_second_level_font' => 'Roboto',
			'panel_second_level_font_size' => '15px',
			'panel_second_level_font_weight' => 'bold',
			'panel_second_level_font_weight_hover' => 'bold',
			'panel_second_level_text_decoration' => 'none',
			'panel_second_level_text_decoration_hover' => 'none',
			'panel_second_level_padding_bottom' => '5px',
			'panel_second_level_margin_bottom' => '5px',
			'panel_second_level_border_color' => '#555',
			'panel_third_level_font_color' => 'rgb(0, 0, 0)',
			'panel_third_level_font_color_hover' => 'rgb(237, 102, 57)',
			'panel_third_level_font' => 'Roboto',
			'panel_third_level_font_size' => '13px',
			'panel_third_level_padding_top' => '5px',
			'panel_third_level_padding_bottom' => '5px',
			'flyout_width' => '250px',
			'flyout_menu_background_from' => 'rgb(255, 255, 255)',
			'flyout_menu_background_to' => 'rgb(255, 255, 255)',
			'flyout_border_color' => 'rgb(237, 102, 57)',
			'flyout_border_top' => '2px',
			'flyout_menu_item_divider' => 'on',
			'flyout_menu_item_divider_color' => 'rgba(104, 104, 104, 0.1)',
			'flyout_padding_top' => '13px',
			'flyout_link_padding_left' => '30px',
			'flyout_link_padding_right' => '30px',
			'flyout_link_padding_top' => '10px',
			'flyout_link_padding_bottom' => '10px',
			'flyout_link_height' => '30px',
			'flyout_background_from' => 'rgba(0, 0, 0, 0)',
			'flyout_background_to' => 'rgba(0, 0, 0, 0)',
			'flyout_background_hover_from' => 'rgb(248, 248, 248)',
			'flyout_background_hover_to' => 'rgb(248, 248, 248)',
			'flyout_link_size' => '14px',
			'flyout_link_color' => 'rgb(0, 0, 0)',
			'flyout_link_color_hover' => 'rgb(237, 102, 57)',
			'flyout_link_family' => 'Roboto',
			'shadow' => 'on',
			'shadow_vertical' => '3px',
			'shadow_color' => 'rgba(0, 0, 0, 0.3)',
			'toggle_background_from' => '#222',
			'toggle_background_to' => '#222',
			'toggle_font_color' => 'rgb(0, 0, 0)',
			'mobile_background_from' => '#222',
			'mobile_background_to' => '#222',
			'custom_css' => '/** Push menu onto new line **/
				#{$wrap} {
					clear: both;
				}
				#{$wrap} #{$menu} > li.mega-menu-item > a.mega-menu-link, #{$wrap} #{$menu} > li.mega-menu-item > a.mega-menu-link:hover, #{$wrap} #{$menu} > li.mega-menu-item > a.mega-menu-link:focus, #{$wrap} #{$menu} > li.mega-menu-item.mega-current-menu-item > a.mega-menu-link, #{$wrap} #{$menu} > li.mega-menu-item.mega-current-menu-ancestor > a.mega-menu-link,
				#{$wrap} #{$menu} > li.mega-menu-item.mega-toggle-on > a.mega-menu-link, #{$wrap} #{$menu} > li.mega-menu-item > a.mega-menu-link:hover, #{$wrap} #{$menu} > li.mega-menu-item > a.mega-menu-link:focus{
					font-weight: 500;
				}',
		);
		return $themes;
	}
}

$NbtEducationTheme = new NbtEducationTheme();

/** Custom field */
if(function_exists("register_field_group"))
{
    register_field_group(array (
        'id' => 'acf_custom-field-course',
        'title' => esc_html__( 'Custom field course', 'education' ),
        'fields' => array (
            array (
                'key' => 'field_581d468525f8e',
                'label' => esc_html__( 'Advisor', 'education' ),
                'name' => 'advisor',
                'type' => 'wysiwyg',
                'default_value' => '',
                'toolbar' => 'full',
                'media_upload' => 'yes',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'sfwd-courses',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'no_box',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
    register_field_group(array (
        'id' => 'acf_custom-field-team',
        'title' => esc_html__( 'Page Class', 'Custom field team', 'education' ),
        'fields' => array (
            array (
                'key' => 'field_5819426dd721e',
                'label' => esc_html__( 'Page Class', 'Social login', 'education' ),
                'name' => 'social_login',
                'type' => 'wysiwyg',
                'default_value' => '',
                'toolbar' => 'full',
                'media_upload' => 'yes',
            ),
            array (
                'key' => 'field_581942dfd721f',
                'label' => esc_html__( 'Page Class', 'Description', 'education' ),
                'name' => 'description',
                'type' => 'textarea',
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => '',
                'formatting' => 'html',
            ),
            array (
                'key' => 'field_58194351d7220',
                'label' => esc_html__( 'Page Class', 'Information', 'education' ),
                'name' => 'information',
                'type' => 'wysiwyg',
                'default_value' => '',
                'toolbar' => 'full',
                'media_upload' => 'yes',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'team',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
	register_field_group(array (
		'id' => 'acf_page-class',
		'title' => esc_html__( 'Page Class', 'education' ),
		'fields' => array (
			array (
				'key' => 'field_58c0c64cd045a',
				'label' => esc_html__( 'Extra Class', 'education' ),
				'name' => 'extra_class_page',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_58f1786c8d3f5',
				'label' => esc_html__( 'Header over', 'education' ),
				'name' => 'header_over',
				'type' => 'true_false',
				'message' => 'Header overlaps with content',
				'default_value' => 0,
			),
			array (
				'key' => 'field_58ca47685630f',
				'label' => esc_html__( 'Display breadcrumb', 'education' ),
				'name' => 'display_breadcrumb',
				'type' => 'radio',
				'choices' => array (
					2 => 'No',
					1 => 'Yes',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => 1,
				'layout' => 'horizontal',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'side',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_video-trailer',
		'title' => esc_html__( 'Video Trailer', 'education' ),
		'fields' => array (
			array (
				'key' => 'field_58d3856ecffca',
				'label' => esc_html__( 'Aspect Ratio', 'education' ),
				'name' => 'aspect_ratio',
				'type' => 'select',
				'choices' => array (
					'16by9' => esc_html__( '16 x 9', 'education' ),
					'4by3' => esc_html__( '4 x 3', 'education' ),
				),
				'default_value' => '16by9',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_58d3795f26979',
				'label' => esc_html__( 'Video Type', 'education' ),
				'name' => 'video_type',
				'type' => 'select',
				'choices' => array (
					'none' => esc_html__( 'Select Video Type', 'education' ),
					'external' => esc_html__( 'External (YouTube, Vimeo, etc)', 'education' ),
					'self' => esc_html__( 'Self Hosted', 'education' ),
					'remote' => esc_html__( 'Remote Video File', 'education' ),
					'custom' => esc_html__( 'Custom Embed Code', 'education' ),
				),
				'default_value' => 'none',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_58d379cf2697a',
				'label' => esc_html__( 'External Video URL', 'education' ),
				'name' => 'external_video_url',
				'type' => 'text',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_58d3795f26979',
							'operator' => '==',
							'value' => 'external',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_58d379ef2697b',
				'label' => esc_html__( 'Self Hosted Video', 'education' ),
				'name' => 'self_hosted_video',
				'type' => 'file',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_58d3795f26979',
							'operator' => '==',
							'value' => 'self',
						),
					),
					'allorany' => 'all',
				),
				'save_format' => 'object',
				'library' => 'all',
			),
			array (
				'key' => 'field_58d37a112697c',
				'label' => esc_html__( 'MP4 File URL', 'education' ),
				'name' => 'mp4_file_url',
				'type' => 'text',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_58d3795f26979',
							'operator' => '==',
							'value' => 'remote',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_58d37a2b2697d',
				'label' => esc_html__( 'Custom Embed Code', 'education' ),
				'name' => 'custom_embed_code',
				'type' => 'textarea',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_58d3795f26979',
							'operator' => '==',
							'value' => 'custom',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => 4,
				'formatting' => 'html',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'sfwd-courses',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'side',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
include_once('advanced-custom-fields/acf.php');

function ocdi_nbt_get_import_files()
{
	$dir = plugin_dir_path( __FILE__ );
	
	return array(
		array(
			'import_file_name'             => 'Education',
			'local_import_file'            => trailingslashit( $dir ) . 'import-files/demo-content.xml',
			'local_import_widget_file'     => trailingslashit( $dir ) . 'import-files/widgets.wie',
			'import_preview_image_url'     => 'http://netbaseteam.com/wordpress/theme/import_preview_img/elearning.png',
			'import_notice'                => esc_html__( 'test demo education.', 'education' ),
		),
	);
}
add_filter( 'pt-ocdi/import_files', 'ocdi_nbt_get_import_files' );

function ocdi_nbt_after_import( $selected_import ) {
	$dir = plugin_dir_path( __FILE__ );
	
	if ( 'Education' === $selected_import['import_file_name'] ) {

		$top_menu = get_term_by('name', 'Main-menu', 'nav_menu');
		$footer_menu = get_term_by('name', 'menu-footer', 'nav_menu');
		set_theme_mod( 'nav_menu_locations' , array(
				'primary' => $top_menu->term_id,
				'footer' => $footer_menu->term_id,
			)
		);

		$page = get_page_by_title( 'Home');
		if ( isset( $page->ID ) ) {
			update_option( 'page_on_front', $page->ID );
			update_option( 'show_on_front', 'page' );
		}

		if ( class_exists( 'RevSlider' ) ) {
			$slider_array = array(
				$dir."import-files/education.zip",
				$dir."import-files/education-v2.zip",
				$dir."import-files/education-v3.zip",
				$dir."import-files/education-v4.zip",
			);

			$slider = new RevSlider();

			foreach($slider_array as $filepath){
				$slider->importSliderFromPost(true,true,$filepath);
			}

			echo 'Slider processed';
		}

		$json = $dir.'import-files/theme-options.json';
		$json_data = file_get_contents( $json );
		$result = json_decode($json_data, true);
		$redux_framework = ReduxFrameworkInstances::get_instance('education_options');
		$redux_framework->set_options($result);
	}
}

add_action( 'pt-ocdi/after_import', 'ocdi_nbt_after_import');